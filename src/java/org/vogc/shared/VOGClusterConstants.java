package org.vogc.shared;

public class VOGClusterConstants {

    // path delal cartella WebAppTemplate
    public static String XmlGeneratorString = "C:/vogcluster/Templates/WebAppTemplate/";

    // path del file FEconf.xml
    public static String CONFIG = "C:/vogcluster/config/FEConf.xml";

    /*PATH PC OAC MASSIMO*/
          //  public static String xmlgeneratorString = "C:/max/universita/progetti/DAME/SuiteSW/VOGCLUSTERS/deployment/vogcluster/Templates/WebAppTemplate/";
    // public static String config ="C:/max/universita/progetti/DAME/SuiteSW/VOGCLUSTERS/deployment/vogcluster/config/FEConf.xml";
    public static String portaMail = "8084";
    public static String portaServer = "8084";
           // public static String host = "http://193.205.103.193";
    // public static String mail = "bresciamax@gmail.com";

    /*PATH PC ETTORE*/
//    public static String xmlgeneratorString = "E:/vogcluster/Templates/WebAppTemplate/";
//    public static String config = "E:/vogcluster/config/FEConf.xml";
//    public static String portaMail = "8084";
//    public static String portaServer = "8084";
    public static String host = "http://localhost";
    public static String mail = "pellecchia.luca@gmail.com";

    /*PATH PC DAMEFRIZZ*/
//            public static String xmlgeneratorString = "/home/ettore/vogcluster/Templates/WebAppTemplate/";
//            public static String config ="/home/ettore/vogcluster/config/FEConf.xml";
//            public static String portaMail ="8080";
//            public static String portaServer ="8080";
//            public static String host = "http://143.225.85.52";
//            public static String mail = "vogclusters@gmail.com";
}
