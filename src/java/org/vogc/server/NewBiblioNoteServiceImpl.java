package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import freemarker.template.TemplateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vogc.client.NewBiblioNoteService;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.InfoForNotes;
import org.vogc.client.datatype.Note;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author Sabrina
 */
public class NewBiblioNoteServiceImpl extends RemoteServiceServlet implements NewBiblioNoteService {

    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final long serialVersionUID = 1L;
    private final XMLParser xmlp = new XMLParser();

    public InfoForNotes getBibblioNoteInfo(String objectId) {
        ErrorReport report = new ErrorReport();
        InfoForNotes info = new InfoForNotes();
        try {
            String result = webapp_uc.WAgetInfoForNotes(objectId);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                info.setReport(report);
            } else {
                info = xmlp.parseInfoForNotes(result);
            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewBiblioNoteServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return info;
    }

    public ErrorReport saveBiblioRef(BiblioReference br) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAsaveBiblioRef(br);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
                //report.setMessage(result); // parse esito salvataggio oggetto
            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    public ErrorReport saveNote(Note note) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ErrorReport saveNoteNew(BiblioReference br) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAsaveNoteRef(br);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
                //report.setMessage(result); // parse esito salvataggio oggetto
            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;


    }

    public ErrorReport saveNewAuthor(String n, String u) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAsaveAuthor(n, u);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
                //report.setMessage(result); // parse esito salvataggio oggetto
            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }
}
