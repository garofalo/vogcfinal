package org.vogc.server;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.vogc.client.datatype.AdditionalInfoAttribute;
import org.vogc.client.datatype.Author;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.Note;
import org.vogc.client.datatype.VObject;
import org.vogc.client.datatype.VObjectAttribute;
//import org.vogc.shared.VOGClusterConstants;

/**
 *
 * @author Sabrina
 */
public class XMLGenerator {
 private final static String HOME = "C:/vogcluster/";
    // path delal cartella WebAppTemplate
    public static final String XML_GENERATOR = HOME + "Templates/WebAppTemplate/";
    // path del file FEconf.xml
    public static final String CONFIG_PATH = HOME + "config/";
    public static final String CONFIG_FILE = "FEConf.xml";
    public static final String CONFIG = CONFIG_PATH+ CONFIG_FILE;

    /*PATH PC OAC MASSIMO*/
    //  public static String xmlgeneratorString = "C:/max/universita/progetti/DAME/SuiteSW/VOGCLUSTERS/deployment/vogcluster/Templates/WebAppTemplate/";
    // public static String config ="C:/max/universita/progetti/DAME/SuiteSW/VOGCLUSTERS/deployment/vogcluster/config/FEConf.xml";
    public static final String MAIL_PORT = "8080";
    public static final String SERVER_PORT = "8084";
    // public static String host = "http://193.205.103.193";
    // public static String mail = "bresciamax@gmail.com";

    /*PATH PC ETTORE*/
//    public static String xmlgeneratorString = "E:/vogcluster/Templates/WebAppTemplate/";
//    public static String config = "E:/vogcluster/config/FEConf.xml";
//    public static String portaMail = "8084";
//    public static String portaServer = "8084";
    public static final String HOST = "http://localhost";
    /**
     *
     */
    public static final String MAIL = "pellecchia.luca@gmail.com";
    private final Configuration cfg_;
    private final PrintWriter out_;
    // private final String DIRECTORY = "C:/Documents and Settings/EM/Documenti/vogcluster/Templates/WebAppTemplate/";  // DA USARE PER IL DEéLOY PER IL LABORATORIO
    //private final String DIRECTORY = "C:/Documents and Settings/EM/Documenti/VOGCLUSTERS/VOGCLUSTERS/Freemarker Template/Templates/WebAppTemplate/";
    String DIRECTORY = XML_GENERATOR;

    public XMLGenerator(PrintWriter out_) throws IOException {
        this.out_ = out_;
        cfg_ = new Configuration();
        cfg_.setDirectoryForTemplateLoading(new File(DIRECTORY));
        cfg_.setObjectWrapper(new DefaultObjectWrapper());
    }

    @SuppressWarnings("unchecked")
    public void generateGCluster(VObject obj) throws IOException, TemplateException {
        Map gcData = new HashMap(5);
        HashMap attData;
        ArrayList<HashMap> attList = new ArrayList<HashMap>(10);

        gcData.put("objId", obj.getId());
        // gcData.put("gclusterId",obj.getClusterId()); // Aggiunto da Luca
        gcData.put("clusterType", obj.getType());
        gcData.put("objName", obj.getName());
        gcData.put("sourceId", obj.getSourceId());
        for (VObjectAttribute a : obj.getAttribute()) {

            attData = new HashMap(10);
            attData.put("isNew", a.getIsNew());
            attData.put("value", a.getValue());
            attData.put("name", a.getName());


            if (a.getDescription() != null) {
                attData.put("desc", a.getDescription());
            } else {
                attData.put("desc", "");
            }
            if (a.getUcd() != null) {
                attData.put("ucd", a.getUcd());
            } else {
                attData.put("ucd", "");
            }
            if (a.getDatatype() != null) {
                attData.put("datatype", a.getDatatype());
            } else {
                attData.put("datatype", "");
            }
            if (a.getType() > 0) {
                attData.put("type", a.getType());
            } else {
                attData.put("type", "");
            }

            attList.add(attData);
        }

        gcData.put("attributes", attList);
        freemarkerDo(gcData, "gcluster.ftl");

    }

    @SuppressWarnings("unchecked")
    public void generatePulsar(VObject obj) throws IOException, TemplateException {
        Map gcData = new HashMap(5);
        Map attData;
        ArrayList attList = new ArrayList<HashMap>(10);

        gcData.put("objId", obj.getId());

        gcData.put("clusterType", obj.getType());
        gcData.put("objName", obj.getName());
        gcData.put("sourceId", obj.getSourceId());




        gcData.put("gclusterId", obj.getClusterId()); // Aggiunto da Luca
        for (VObjectAttribute a : obj.getAttribute()) {

            attData = new HashMap(10);
            attData.put("isNew", a.getIsNew());
            attData.put("value", a.getValue());
            attData.put("name", a.getName());

            if (a.getDescription() != null) {
                attData.put("desc", a.getDescription());
            } else {
                attData.put("desc", "");
            }
            if (a.getUcd() != null) {
                attData.put("ucd", a.getUcd());
            } else {
                attData.put("ucd", "");
            }
            if (a.getDatatype() != null) {
                attData.put("datatype", a.getDatatype());
            } else {
                attData.put("datatype", "");
            }
            if (a.getType() > 0) {
                attData.put("type", a.getType());
            } else {
                attData.put("type", "");
            }

            attList.add(attData);
        }

        gcData.put("attributes", attList);
        freemarkerDo(gcData, "pulsar.ftl");

    }

    @SuppressWarnings("unchecked")
    public void generateStar(VObject obj) throws IOException, TemplateException {
        Map gcData = new HashMap(5);
        Map attData;
        ArrayList attList = new ArrayList<HashMap>(10);

        gcData.put("objId", obj.getId());

        gcData.put("clusterType", obj.getType());
        gcData.put("objName", obj.getName());
        gcData.put("sourceId", obj.getSourceId());




        gcData.put("gclusterId", obj.getClusterId()); // Aggiunto da Luca
        for (VObjectAttribute a : obj.getAttribute()) {

            attData = new HashMap(10);
            attData.put("isNew", a.getIsNew());
            attData.put("value", a.getValue());
            attData.put("name", a.getName());

            if (a.getDescription() != null) {
                attData.put("desc", a.getDescription());
            } else {
                attData.put("desc", "");
            }
            if (a.getUcd() != null) {
                attData.put("ucd", a.getUcd());
            } else {
                attData.put("ucd", "");
            }
            if (a.getDatatype() != null) {
                attData.put("datatype", a.getDatatype());
            } else {
                attData.put("datatype", "");
            }
            if (a.getType() > 0) {
                attData.put("type", a.getType());
            } else {
                attData.put("type", "");
            }

            attList.add(attData);
        }

        gcData.put("attributes", attList);
        freemarkerDo(gcData, "star.ftl");

    }

    @SuppressWarnings("unchecked")
    public void generateBiblioRef(BiblioReference br) throws IOException, TemplateException {
        Map biblioInfo = new HashMap(12);
        Map authorInfo;
        ArrayList authorList = new ArrayList<HashMap>(5);

        biblioInfo.put("objectId", br.getObjectId());
        biblioInfo.put("type", br.getType());
        biblioInfo.put("userId", br.getUser().getUserId());
        biblioInfo.put("title", validateString(br.getTitle()));
        biblioInfo.put("desc", validateString(br.getDescription()));
        biblioInfo.put("year", br.getYear());
        biblioInfo.put("uri", br.getUri());
        biblioInfo.put("paperId", br.getPaper().getId());
        if (br.getPaper().getName() == null) {
            biblioInfo.put("paperName", "");
        } else {
            biblioInfo.put("paperName", validateString(br.getPaper().getName()));
        }
        if (br.getPaper().getUri() == null) {
            biblioInfo.put("paperUri", "");
        } else {
            biblioInfo.put("paperUri", validateString(br.getPaper().getUri()));
        }
        for (Author a : br.getAuthors()) {
            authorInfo = new HashMap(5);
            authorInfo.put("id", a.getId());
            if (a.getName() == null) {
                authorInfo.put("name", "");
            } else {
                authorInfo.put("name", validateString(a.getName()));
            }
            if (a.getUri() == null) {
                authorInfo.put("uri", "");
            } else {
                authorInfo.put("uri", validateString(a.getUri()));
            }
            authorList.add(authorInfo);
        }
        biblioInfo.put("authors", authorList);
        freemarkerDo(biblioInfo, "biblioRef.ftl");

    }

    public void generateNote(Note note) throws IOException, TemplateException {
    }

    public void generateAttributeUpdate(AdditionalInfoAttribute att) throws IOException, TemplateException {
    }

    public void freemarkerDo(Map datamodel, String template) throws IOException, TemplateException {
        Template tpl = cfg_.getTemplate(template);

        tpl.process(datamodel, out_);
    }

    public String validateString(String toValidate) {

        String validated = toValidate;
        if (toValidate.contains("&") == true) {
            validated = toValidate.replace("&", "&amp;");
        }
        if (toValidate.contains("<") == true) {
            validated = toValidate.replace("<", "&lt;");
        }
        if (toValidate.contains(">") == true) {
            validated = toValidate.replace(">", "&gt;");
        }
        if (toValidate.contains("\"") == true) {
            validated = toValidate.replace("\"", "&quot;");
        }
        if (toValidate.contains("'") == true) {
            validated = toValidate.replace("'", "&apos;");
        }
        return validated;
    }
}
