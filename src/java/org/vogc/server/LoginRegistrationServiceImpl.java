package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import freemarker.template.TemplateException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.vogc.client.LoginRegistrationService;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.User;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author EM
 */
/**
 *
 * @author Francesco
 */
public class LoginRegistrationServiceImpl
        extends RemoteServiceServlet implements LoginRegistrationService {
    // private static FrontendUseCase frontEnd = new FrontendUseCase();

    private static final long serialVersionUID = 1L;
    //private static String port = "8084";
    private static final String port = Config.getPortFW();
    //private static String serverName = "VONDAME";
    private static final String serverName = Config.getFWServerName();
    private static final String mail = Config.getAdminMail();
//  private static String host = "http://" + Config.getHostFW();
//  private static String port = Config.getPortFW();
//  private static String serverName = Config.getFWServerName();
//  private static String mail = Config.getAdminMail();
    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final boolean auth = Config.isAuth();
    private static final String serverMail = Config.getServerMail();
    /*
     *
     */

    /*
     *
     */
    public static void sendMail(String dest, String mitt, String oggetto, String htmlEmail) throws MessagingException {
        try {
// Creazione degli indirizzi del mittente e del destinatario
            InternetAddress fromAddress = new InternetAddress(mitt);
            InternetAddress toAddress = new InternetAddress(dest);

            /*
             *   !!! NON CANCELLARE !!!
             *
             */
            if (auth) {

// Recupero username e passowrd per autenticazione sul servermail
                String user = Config.getUser();
                String pwd = Config.getPwd();

// Recupero informazioni di sistema
                Properties props = System.getProperties();

// Setup del server mail
                props.put("mail.smtp.host", serverMail);
                props.put("mail.debug", "true");
                props.put("mail.smtp.auth", "true");

// Recupero della sessione
                Session session = Session.getDefaultInstance(props);
                session.setDebug(true);
                int scambio = Integer.valueOf(port);
                URLName urlName = new URLName("smtp", serverMail, scambio, "INBOX", user, pwd);
                PasswordAuthentication tmp = new PasswordAuthentication(user, pwd);
                session.setPasswordAuthentication(urlName, tmp);

// Creazione del messaggio da inviare
                MimeMessage message = new MimeMessage(session);
                message.setSubject(oggetto);
                message.setContent(htmlEmail, "text/html");
                message.setFrom(fromAddress);
                message.setRecipient(Message.RecipientType.TO, toAddress);

// Invio del messaggio
                Transport tr = session.getTransport("smtp");
                tr.connect(serverMail, user, pwd);
                message.saveChanges();
                tr.sendMessage(message, message.getAllRecipients());
                tr.close();
                /*
                 *** !!!! NON CANCELLARE !!! ***
                 */
            } else {
                // Creazione di una mail session
                Properties props = new Properties();

                props.put("mail.smtp.host", serverMail);
                Session session = Session.getDefaultInstance(props);
                // Creazione del messaggio da inviare
                MimeMessage message = new MimeMessage(session);
                message.setSubject(oggetto);
                message.setContent(htmlEmail, "text/html");
                // Aggiunta degli indirizzi del mittente e del destinatario

                message.setFrom(fromAddress);
                message.setRecipient(Message.RecipientType.TO, toAddress);
                // Invio del messaggio
                Transport.send(message);
            }
        } catch (NumberFormatException ex) {
            Logger.getLogger(LoginRegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//    } catch (FileNotFoundException ex) {
//      Logger.getLogger(LoginRegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(LoginRegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//    } catch (FileNotFoundException ex) {
//      Logger.getLogger(LoginRegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private final XMLParser xmlp = new XMLParser();
    //private static String host = "http://143.225.85.52";
    // private static String host = "http://localhost";
    String host = Config.getHostFW();

    public ErrorReport registration(User p) {

//    String url = host + ":" + port + "/" + serverName;
//    String link = url + "/users/newUser?userMail=" + p.getUserMail();
//    ErrorReport err = new ErrorReport();
//    String text = null;

        String url = host + ":" + port + "/" + serverName;
        String link = url + "/UpdateUserStatusServlet?userId=" + p.getUserMail();
        ErrorReport err = new ErrorReport();

        try {

            String result = webapp_uc.WANewUserValue(p.getUserMail(), p.getPassword(), p.getName(), p.getSurname(), p.getAffiliation(), p.getCountry(), 2);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                err.setCode(Messages.CONNECTION_NOT_FOUND);
                err.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                err.setAdditionalInfo("");
            } else {
                err = xmlp.parseErrorReport(result);
            }

        } catch (TemplateException ex) {
            Logger.getLogger(LoginRegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
         * l'utente e' stato registrato correttamente a questo punto mando
         * e-mail all'utente, e all'admin che procedera all'attivazione
         */
        if (err.getCode().equals("vogcSRV0000")) {

            try {
                String text = "<html><body><p>Dear " + p.getName() + " "
                        + p.getSurname() + ",<br />"
                        + "Thank you for registering to VOGCLUSTERS ALPHA VERSION .<br />"
                        + "<br /><br /><b>"
                        + "Your username for this demo is:  " + "</b> " + p.getUserMail()
                        + "<br /><b>Your password is: " + "</b> " + p.getPassword()
                        + "<br /><b><u>Your account will be activated as soon as possible<br /></b></u>"
                        + "<br/>Please visit <a href=http://dame.dsf.unina.it>dame.dsf.unina.it/</a> for "
                        + "all information about DAME services.<br /"
                        + "><br />For any request, please contact:<br />"
                        + "by e-mail: helpdame@gmail.com<br />"
                        + "by Skype: <a href='skype:helpdame?call'>"
                        + "<img src='http://download.skype.com/share/skypebuttons/buttons/call_blue_white_124x52.png' style='border: none;' width='124' height='52' alt='Skype Me™!' />"
                        + "</a><br /><br />The DAME Staff<br />"
                        + "<img src='http://dame.dsf.unina.it/images/vogc_dame_logo.png'/>"
                        + "<br/>DAME - Data Mining &amp; Exploration Program</p><body></html>";


                sendMail(p.getUserMail(), "Vogclusters@dame.it", "Registering on VOGCLUSTER ", text);

                String text2 = "<html><body><p>The user: " + p.getUserMail()
                        + " has requested a registration at VOGCLUSTERS ALPHA.</p>"
                        + "<p>Summary Of User:<br /><br />"
                        + "Name: " + p.getName() + "<br />Surname: " + p.getSurname()
                        + "<br />Country: " + p.getCountry() + "<br />Affilation: "
                        + p.getAffiliation() + "<br /><br />"
                        + "<img src='http://dame.dsf.unina.it/images/vogc_dame_logo.png'/>"
                        + "<br /><br /> <a href='" + link + "'>CLICK HERE TO ACTIVATE USER</a><br />"
                        + "DAME - Data Mining &amp; Exploration Program<br />"
                        + "</p><p><br /><br /></p></body></html>";

//invio e-mail utente di avvenuta regitrazione
                sendMail(mail, "VOGCLUSTERactivation@dame.it", "Activation Account", text2);

            } catch (MessagingException ex) {
                Logger.getLogger(LoginRegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);

            }
        }
        return err;
    }
}
