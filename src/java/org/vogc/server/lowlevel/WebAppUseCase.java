package org.vogc.server.lowlevel;

import freemarker.template.TemplateException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.FileRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.vogc.client.datatype.AdditionalInfoAttribute;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.Note;
import org.vogc.client.datatype.VObject;
import org.vogc.client.datatype.VObjectAttribute;
import org.vogc.server.XMLGenerator;
import org.vogc.server.highlevel.VogcServerUseCase;

/**
 *
 * @author Sabrina
 */
public class WebAppUseCase {

    private static final VogcServerUseCase srvUc = new VogcServerUseCase();

    public static String doGet(String urlreq) throws HttpException, IOException {
        GetMethod get = new GetMethod(urlreq);
        HttpClient client = new HttpClient();

        client.executeMethod(get);
        String result = get.getResponseBodyAsString();
        get.releaseConnection();
        if (get.getStatusCode() != 200) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public static String doPut(String urlreq) throws HttpException, IOException {

        PutMethod put = new PutMethod(urlreq);
        HttpClient client = new HttpClient();
        client.executeMethod(put);

        String result = put.getResponseBodyAsString();

        put.releaseConnection();

        if (put.getStatusCode() != 200) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public static String doPost(String urlreq, File temp) throws HttpException, IOException {
        String result = null;

        HttpClient client = new HttpClient();
        GetMethod get = new GetMethod(urlreq);
        PostMethod post = new PostMethod(urlreq);

        client.executeMethod(get);
        get.releaseConnection();

//        InputStream in = new FileInputStream(temp);
//        InputStreamRequestEntity reqEnt = new InputStreamRequestEntity(in);
        RequestEntity entity = new FileRequestEntity(temp, result);
        post.setRequestEntity(entity);
        client.executeMethod(post);

        // resubmit the original request
        result = post.getResponseBodyAsString();

        if (post.getStatusCode() != 200) {
            result = Messages.CONNECTION_NOT_FOUND;

        }
        temp.delete();
        return result;
    }

    public static String doDelete(String urlreq) throws HttpException, IOException {
        DeleteMethod delete = new DeleteMethod(urlreq);
        HttpClient client = new HttpClient();
        client.executeMethod(delete);
        String result = delete.getResponseBodyAsString();
        delete.releaseConnection();
        if (delete.getStatusCode() != 200) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAauthenticateUser(String userId, String password) {
        String urlreq = srvUc.srvAuthenticateUser(userId, password);
        String result;
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAgetAttributesList(int objType) {

        String result;
        String urlreq = srvUc.srvNewVObjectRequest(objType);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAsaveNewVObject(int objType, VObject obj, VObject obj2) throws TemplateException {
        String urlreq;
        // String urlreq2 = null;
        String result = null;
        XMLGenerator xmlg;

        if (objType == 1) {

            try {

                File temp = File.createTempFile("newObjectAtt", ".xml");
                PrintWriter out = new PrintWriter(new FileWriter(temp));
                xmlg = new XMLGenerator(out);
                xmlg.generateGCluster(obj);
                out.close();
                urlreq = srvUc.srvNewVObjectResponse(objType);
                try {
                    result = doPost(urlreq, temp);

                } catch (IOException e) {
                    result = Messages.CONNECTION_NOT_FOUND;
                }

            } catch (IOException ex) {
                Logger.getLogger(WebAppUseCase.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (objType == 2) {

            try {
                File temp = File.createTempFile("newPulsarAtt", ".xml");
                PrintWriter out = new PrintWriter(new FileWriter(temp));
                xmlg = new XMLGenerator(out);
                xmlg.generatePulsar(obj);
                out.close();
                urlreq = srvUc.srvNewVObjectResponse(objType);
                try {
                    result = doPost(urlreq, temp);

                } catch (IOException e) {
                    result = Messages.CONNECTION_NOT_FOUND;
                }

            } catch (IOException ex) {
                Logger.getLogger(WebAppUseCase.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (objType == 3) {

            try {
                File temp = File.createTempFile("newStarAtt", ".xml");
                PrintWriter out = new PrintWriter(new FileWriter(temp));
                xmlg = new XMLGenerator(out);
                xmlg.generateStar(obj);
                out.close();
                urlreq = srvUc.srvNewVObjectResponse(objType);
                try {
                    result = doPost(urlreq, temp);

                } catch (IOException e) {
                    result = Messages.CONNECTION_NOT_FOUND;
                }

            } catch (IOException ex) {
                Logger.getLogger(WebAppUseCase.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return result;
    }

    public String waGetVObject(String objectId) throws TemplateException {

        String result;

        String urlreq = srvUc.srvVObject(objectId);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAgetCatalogue(String catId) throws TemplateException {

        String result;

        String urlreq = srvUc.srvGetCatalogue(catId);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAsearch(String toSearch) throws TemplateException {

        String result;

        String urlreq = srvUc.srvSearch(toSearch);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAgetClustersList() throws TemplateException {

        String result;

        String urlreq = srvUc.srvClustersList();
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAnewAttribute(VObjectAttribute att, String userId, String objectId) throws TemplateException {

        String result;

        String urlreq = srvUc.srvNewAttribute(userId, objectId, att.getName(), att.getDescription(), att.getUcd(), att.getDatatype(), att.getValue(), att.getType());
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAnewAttributeValue(String userId, String objectId, int attId, String attValue) throws TemplateException {

        String result;

        String urlreq = srvUc.srvNewAttributeValue(userId, objectId, attId, attValue);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAgetInfoForNotes(String objectId) throws TemplateException {

        String result;

        String urlreq = srvUc.srvNewBiblioNoteRequest(objectId);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAsaveBiblioRef(BiblioReference br) throws TemplateException {
        String urlreq;
        String result = null;

        XMLGenerator xmlg;
        try {

            File temp = File.createTempFile("newBiblioRef", ".xml");
            PrintWriter out = new PrintWriter(new FileWriter(temp));
            xmlg = new XMLGenerator(out);
            xmlg.generateBiblioRef(br);
            out.close();
            urlreq = srvUc.srvNewBiblioNoteResponse(2); // 2 for biblio ref
            try {
                result = doPost(urlreq, temp);

            } catch (IOException e) {
                result = Messages.CONNECTION_NOT_FOUND;
            }

        } catch (IOException ex) {
            Logger.getLogger(WebAppUseCase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String WAsaveNoteRef(BiblioReference br) throws TemplateException {

        String result = null;

        XMLGenerator xmlg;
        try {

            File temp = File.createTempFile("newBiblioRef", ".xml");
            PrintWriter out = new PrintWriter(new FileWriter(temp));
            xmlg = new XMLGenerator(out);
            xmlg.generateBiblioRef(br);
            out.close();
            String urlreq = srvUc.srvNewBiblioNoteResponse(1);
            try {
                result = doPost(urlreq, temp);

            } catch (IOException e) {
                result = Messages.CONNECTION_NOT_FOUND;
            }

        } catch (IOException ex) {
            Logger.getLogger(WebAppUseCase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String WAgetUserInfo(String userId) throws TemplateException {

        String result;

        String urlreq = srvUc.srvUserInfo(userId);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAgetUsersList() throws TemplateException {

        String result;

        String urlreq = srvUc.srvUsersList();
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAgetAuthorsList() throws TemplateException {

        String result;

        String urlreq = srvUc.srvAuthorList();
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAupdateAttribute(AdditionalInfoAttribute att) throws TemplateException {
        String urlreq;
        String result = null;
        XMLGenerator xmlg;

        try {

            File temp = File.createTempFile("newBiblioRef", ".xml");
            PrintWriter out = new PrintWriter(new FileWriter(temp));
            xmlg = new XMLGenerator(out);
            xmlg.generateAttributeUpdate(att);
            out.close();
            urlreq = srvUc.srvUpdateAttribute();
            try {
                result = doPost(urlreq, temp);

            } catch (IOException e) {
                result = Messages.CONNECTION_NOT_FOUND;
            }

        } catch (IOException ex) {
            Logger.getLogger(WebAppUseCase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    // String userId, int objectType, int objectHasAttId, String value

    public String WAupdateAttributeValue(String userId, int objectType, int objectHasAttId, String value, String name, String attribute) throws TemplateException {

        String result;

        String urlreq = srvUc.srvUpdateAttributeValue(userId, objectType, objectHasAttId, value, name, attribute);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAupdateBiblioRef(BiblioReference br) throws TemplateException {
        String urlreq;
        String result = null;

        XMLGenerator xmlg;
        try {

            File temp = File.createTempFile("updBiblioRef", ".xml");
            PrintWriter out = new PrintWriter(new FileWriter(temp));
            xmlg = new XMLGenerator(out);
            xmlg.generateBiblioRef(br);
            out.close();
            urlreq = srvUc.srvUpdateBiblioNote(2); // 2 for biblio ref
            try {
                result = doPost(urlreq, temp);

            } catch (IOException e) {
                result = Messages.CONNECTION_NOT_FOUND;
            }

        } catch (IOException ex) {
            Logger.getLogger(WebAppUseCase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String WAupdateNote(Note note) throws TemplateException {
        String urlreq;
        String result = null;

        XMLGenerator xmlg;
        try {

            File temp = File.createTempFile("updNote", ".xml");
            PrintWriter out = new PrintWriter(new FileWriter(temp));
            xmlg = new XMLGenerator(out);
            xmlg.generateNote(note);
            out.close();
            urlreq = srvUc.srvUpdateBiblioNote(1); // 2 for biblio ref
            try {
                result = doPost(urlreq, temp);

            } catch (IOException e) {
                result = Messages.CONNECTION_NOT_FOUND;
            }

        } catch (IOException ex) {
            Logger.getLogger(WebAppUseCase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String WAdeleteAttribute(String userId, String objectId, String attName) throws TemplateException {

        String result;

        String urlreq = srvUc.srvDeleteAttribute(userId, objectId, attName);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAdeleteAttributeValue(String userId, String objectId, int vobjHasAttId) throws TemplateException {

        String result;

        String urlreq = srvUc.srvDeleteAttributeValue(userId, objectId, vobjHasAttId);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAdeleteBiblioNote(String userId, int noteId) throws TemplateException {

        String result;

        String urlreq = srvUc.srvDeleteBiblioNote(userId, noteId);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAdeleteObject(String userId, String type, String objectId) throws TemplateException {

        String result;

        String urlreq = srvUc.srvDeleteObject(userId, type, objectId);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WANewUserValue(String userId, String password, String name, String surname, String motivation, String country, int active) throws TemplateException {

        String result;

        String urlreq = srvUc.srvNewUserValue(userId, password, name, surname, motivation, country, 2);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAgetParameter(String param, String value, String operator) throws TemplateException {

        String result;

        String urlreq = srvUc.srvParameterSearch(param, value, operator);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAgetPartial(String value) throws TemplateException {

        String result;

        String urlreq = srvUc.srvSearch(value);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAHistoryparam(String attName, String vobjectId) throws TemplateException {

        String result;

        String urlreq = srvUc.srvParameterHistory(attName, vobjectId);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }

    public String WAsaveAuthor(String name, String uri) throws TemplateException {

        String result;

        String urlreq = srvUc.srvNewBiblioAuthor(name, uri);
        try {
            result = doGet(urlreq);
        } catch (IOException e) {
            result = Messages.CONNECTION_NOT_FOUND;
        }
        return result;
    }
}
