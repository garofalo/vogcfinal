package org.vogc.server.lowlevel;

import java.util.HashMap;
import java.util.Map;
import org.dame.vogc.error.Error;

/**
 *
 * @author Sabrina
 */
public class Messages {

    public final static String CONNECTION_NOT_FOUND = "CONNECTION_NOT_FOUND";
    private final static String CONNECTION_NOT_FOUND_CODE = "vogcWEBAPP0001";
    private static final Map<String, Error> errorMap_;

    static {
        errorMap_ = new HashMap<String, Error>(2);
        errorMap_.put(CONNECTION_NOT_FOUND, new Error(CONNECTION_NOT_FOUND_CODE, "Connection Not Found"));

    }

    public static String getErrorCode(String error) {
        Error e = errorMap_.get(error);
        return e.getCode();
    }

    public static String getErrorString(String error) {
        Error e = errorMap_.get(error);
        return e.getCode() + "- " + e.getMessage();
    }

    public static Error getError(String error) {
        return errorMap_.get(error);
    }
}
