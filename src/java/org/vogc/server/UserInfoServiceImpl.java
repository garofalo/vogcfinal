package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import freemarker.template.TemplateException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vogc.client.UserInfoService;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.User;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author Sabrina
 */
public class UserInfoServiceImpl extends RemoteServiceServlet implements UserInfoService {

    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final long serialVersionUID = 1L;
    private final XMLParser xmlp = new XMLParser();

    @Override
    public User getUserInfo(String userId) {
        ErrorReport report = new ErrorReport();
        User usr = new User();
        try {
            String result = webapp_uc.WAgetUserInfo(userId);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                usr.setReport(report);
            } else {
                usr = xmlp.parseUserInfo(result);
            }
        } catch (TemplateException ex) {
            Logger.getLogger(UserInfoServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usr;
    }

    @Override
    public ArrayList<User> getUsersList() {
        ErrorReport report = new ErrorReport();
        User usr = new User();
        ArrayList<User> users = new ArrayList<User>(100);
        try {
            String result = webapp_uc.WAgetUsersList();

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                usr.setReport(report);
                users.add(usr);
            } else {
                users = xmlp.parseUsersList(result);
            }
        } catch (TemplateException ex) {
            Logger.getLogger(UserInfoServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }
}
