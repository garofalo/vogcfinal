package org.vogc.server;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vogc.shared.VOGClusterConstants;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * @author Alfonso Nocella
 * @version %I% %G%
 */
public class Config {

    private static String hostFW;
    private static String portFW;
    private static String serverFWName;
    private static String adminMail;
    private static String serverMail;
    private static String user;
    private static String pwd;
    private static boolean auth;

    static {
//        try {
//            XMLReader reader;
//            ConfigHandler handler = new ConfigHandler();
//            reader = XMLReaderFactory.createXMLReader();
//            reader.setContentHandler(handler);
//            reader.setErrorHandler(handler);
//
//            FileInputStream fis = new FileInputStream(VOGClusterConstants.CONFIG);
//
//            BufferedInputStream bis = new BufferedInputStream(fis);
//
//            InputSource ins = new InputSource(bis);
//
//            reader.parse(ins);
//
////            hostFW = handler.getHostFW();
////            portFW = handler.getPortFW();
////            serverFWName = handler.getFWServerName();
////            adminMail = handler.getAdminMail();
////            serverMail = handler.getServerMail();
////            user = handler.getUser();
////            pwd = handler.getPwd();
////            auth = handler.isAuth();
//hostFW = handler.getHostFW();
//            portFW = handler.getPortFW();
//            serverFWName = handler.getFWServerName();
//            adminMail = handler.getAdminMail();
//            serverMail = handler.getServerMail();
//            user = handler.getUser();
//            pwd = handler.getPwd();
//            auth = handler.isAuth();
//        } catch (SAXException ex) {
//            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
//        }

    }

    public static boolean isAuth() {
//        return auth;
        return true;
    }

    public static String getHostFW() {
//        return hostFW;
        return "http://localhost";
    }

    public static String getPortFW() {
//        return portFW;
        return "8084";
    }

    public static String getFWServerName() {
//        return serverFWName;
        return "VOGCSERVER";
    }

    public static String getServerMail() {
//        return serverMail;
        return "posta.na.astro.it";
    }

    public static String getAdminMail() {
//        return adminMail;
        return "bresciamax@gmail.com";

    }

    public static String getUser() {
        return "dame";
//        return user;
    }

    public static String getPwd() {
//        return pwd;
        return "V0neural";
    }

}

class ConfigHandler extends DefaultHandler {

    private static String hostFW;
    private static String portFW;
    private static String serverFWName;
    private static String adminMail;
    private static String serverMail;
    private static String user;
    private static String pwd;
    private static boolean auth;
    private final String name = "name";
    private final String value = "value";

    ConfigHandler() {
        super();
    }

    @Override
    public void startElement(String uri, String name,
            String qName, Attributes atts) {

        if (name.equals("INFO")) {
//            if (atts.getValue(name) != null) {
//                ValueName valueName = ValueName.valueOf(atts.getValue(name));
//
//                switch (valueName) {
//                    case ValueName.valueOf("H"):
//                        hostFW = atts.getValue(value);
//                        break;
//                    case portFW:
//                        portFW = atts.getValue(value);
//                        break;
//                    case FWserverName:
//                        serverFWName = atts.getValue(value);
//                        break;
//                    case adminMail:
//                        adminMail = atts.getValue(value);
//                        break;
//                    case serverMail:
//                        serverMail = atts.getValue(value);
//                        break;
//                    case user:
//                        user = atts.getValue(value);
//                        break;
//                    case pwd:
//                        pwd = atts.getValue(value);
//                        break;
//                    case auth:
//                        auth = atts.getValue(value).matches("true");
//                        break;
//                }
//            }
//            String attName = atts.getValue(name);
//            if (atts.getValue(name).equalsIgnoreCase("")) {
//            } else 
//            if (atts.getValue(name).equalsIgnoreCase("hostFW")) {
//                hostFW = atts.getValue(value);
//            } else if (atts.getValue(name).equalsIgnoreCase("portFW")) {
//                portFW = atts.getValue(value);
//            } else if (atts.getValue(name).equalsIgnoreCase("FWserverName")) {
//
//                serverFWName = atts.getValue(value);
//            } else if (atts.getValue(name).equalsIgnoreCase("adminMail")) {
//                adminMail = atts.getValue(value);
//            } else if (atts.getValue(name).equalsIgnoreCase("serverMail")) {
//                serverMail = atts.getValue(value);
//            } else if (atts.getValue(name).equalsIgnoreCase("user")) {
//                user = atts.getValue(value);
//            } else if (atts.getValue(name).equalsIgnoreCase("pwd")) {
//                pwd = atts.getValue(value);
//            } else if (atts.getValue(name).equalsIgnoreCase("auth")) {
//                auth = atts.getValue(value).matches("true");
//            }

        }
    }

    public boolean isAuth() {
        return auth;
    }

    public String getHostFW() {
        return hostFW;
    }

    public String getPortFW() {
        return portFW;
    }

    public String getFWServerName() {
        return serverFWName;
    }

    public String getAdminMail() {
        return adminMail;
    }

    public String getServerMail() {
        return serverMail;
    }

    public String getUser() {

        return user;

    }

    public String getPwd() {

        return pwd;

    }

    private enum ValueName {

        hostFW, portFW, FWserverName, adminMail, serverMail, user, pwd, auth
    }
}
