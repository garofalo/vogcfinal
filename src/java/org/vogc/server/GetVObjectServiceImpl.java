package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import freemarker.template.TemplateException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vogc.client.GetVObjectService;
import org.vogc.client.datatype.AdditionalInfoAttribute;
import org.vogc.client.datatype.Catalogue;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.GClusterInfo;
import org.vogc.client.datatype.PulsarInfo;
import org.vogc.client.datatype.StarInfo;
import org.vogc.client.datatype.VObject;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author Sabrina
 */
public class GetVObjectServiceImpl extends RemoteServiceServlet implements GetVObjectService {

    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final long serialVersionUID = 1L;
    private final XMLParser xmlp = new XMLParser();

    public GClusterInfo getGcluster(String objectId) {  // per recuperare i dati relativi ad un ammasso
        ErrorReport report = new ErrorReport();
        GClusterInfo gcluster = new GClusterInfo();
        try {
            String result = webapp_uc.waGetVObject(objectId.replaceAll(" ", ""));

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                gcluster.setReport(report);
            } else {
                gcluster = xmlp.parseGcluster(result);
            }



        } catch (TemplateException ex) {
            Logger.getLogger(GetVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return gcluster;  // return xmlp.parseAttribute(result);
    }

    public ArrayList<VObject> getClustersList() {
        ErrorReport report = new ErrorReport();
        VObject cluster = new VObject();
        ArrayList<VObject> clustersList = new ArrayList<VObject>(100);
        try {
            String result = webapp_uc.WAgetClustersList();

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                cluster.setReport(report);
                clustersList = new ArrayList<VObject>(1);
                clustersList.add(cluster);
            } else {
                clustersList = xmlp.parseClustersList(result);
            }

        } catch (TemplateException ex) {
            Logger.getLogger(GetVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clustersList;

    }

    public Catalogue getCatalogInfo(String catId) {

        ErrorReport report = new ErrorReport();
        Catalogue catalog = new Catalogue();
        try {
            String result = webapp_uc.WAgetCatalogue(catId);
            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                catalog.setReport(report);
            } else {
                catalog = xmlp.parseCatalogue(result);
            }
        } catch (TemplateException ex) {
            Logger.getLogger(GetVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return catalog;
    }

    public PulsarInfo getPulsar(String objectId) {
        ErrorReport report = new ErrorReport();
        PulsarInfo pulsar = new PulsarInfo();
        try {
            String result = webapp_uc.waGetVObject(objectId);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                pulsar.setReport(report);
            } else {
                pulsar = xmlp.parsePulsar(result);
            }



        } catch (TemplateException ex) {
            Logger.getLogger(GetVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pulsar;
    }

    public StarInfo getStar(String objectId) {
        ErrorReport report = new ErrorReport();
        StarInfo star = new StarInfo();
        try {
            String result = webapp_uc.waGetVObject(objectId);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                star.setReport(report);
            } else {
                star = xmlp.parseStar(result);
            }



        } catch (TemplateException ex) {
            Logger.getLogger(GetVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return star;
    }

    public ArrayList<AdditionalInfoAttribute> getParameterHistory(String attName, String vobjectId) {
        ErrorReport report = new ErrorReport();
        AdditionalInfoAttribute att = new AdditionalInfoAttribute();
        ArrayList<AdditionalInfoAttribute> valueList;
        try {
            //String result = webapp_uc.WAgetClustersList();
            String result = webapp_uc.WAHistoryparam(attName, vobjectId);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                att.setReport(report);
                valueList = new ArrayList<AdditionalInfoAttribute>(1);
                valueList.add(att);

            } else {
                valueList = xmlp.parseValuesList(result);
            }

        } catch (TemplateException ex) {
            valueList = null;
            Logger.getLogger(GetVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return valueList;
    }

    public ArrayList<VObject> getGclusterByParameters(String parameter, String Value, String operator) {
        ErrorReport report = new ErrorReport();
        VObject cluster = new VObject();
        ArrayList<VObject> clustersList;
        try {
            String result = webapp_uc.WAgetParameter(parameter, Value, operator);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                cluster.setReport(report);
                clustersList = new ArrayList<VObject>(1);
                clustersList.add(cluster);
            } else {
                clustersList = xmlp.parseClustersList(result);
            }

        } catch (TemplateException ex) {
            clustersList = null;
            Logger.getLogger(GetVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clustersList;

    }

    public ArrayList<VObject> getVobjectPartial(String Value) {
        ErrorReport report = new ErrorReport();
        VObject cluster = new VObject();
        ArrayList<VObject> clustersList;
        try {
            String result = webapp_uc.WAgetPartial(Value);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                cluster.setReport(report);
                clustersList = new ArrayList<VObject>(1);
                clustersList.add(cluster);
            } else {
                clustersList = xmlp.parseClustersList(result);
            }

        } catch (TemplateException ex) {
            clustersList = null;
            Logger.getLogger(GetVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clustersList;

    }

    public GClusterInfo getBiblioGcluster(String objectId) {  // per recuperare i dati relativi ad un ammasso
        ErrorReport report = new ErrorReport();
        GClusterInfo gcluster = new GClusterInfo();
        try {
            
            String result = webapp_uc.waGetVObject(objectId.replaceAll(" ", ""));

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                gcluster.setReport(report);
            } else {
                gcluster = xmlp.parseBiblioGcluster(result);
            }



        } catch (TemplateException ex) {
            Logger.getLogger(GetVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return gcluster;  // return xmlp.parseAttribute(result);
    }
}
