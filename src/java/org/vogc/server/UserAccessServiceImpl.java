package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.vogc.client.UserAccessService;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author Sabrina
 */
public class UserAccessServiceImpl extends RemoteServiceServlet implements UserAccessService {

    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final long serialVersionUID = 1L;
    private final XMLParser xmlp = new XMLParser();

    @Override
    public ErrorReport authenticateUser(String userId, String password) {
        String result = webapp_uc.WAauthenticateUser(userId, password);
        ErrorReport report = new ErrorReport();
        if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
            report.setCode(Messages.CONNECTION_NOT_FOUND);
            report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
            report.setAdditionalInfo("");

        } else {
            report = xmlp.parseErrorReport(result);
        }

        return report;

    }

}
