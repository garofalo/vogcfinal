package org.vogc.server.highlevel;

import org.vogc.server.Config;

/**
 *
 * @author Sabrina
 */
public class VogcServerUseCase {

    private final String host = Config.getHostFW();
    private final String port = Config.getPortFW();
    private final String serverName = Config.getFWServerName();
    private final String url = host + ":" + port + "/" + serverName;

    public String srvAuthenticateUser(String userId, String password) {
        return url + "/authenticate?userId=" + userId + "&password=" + password;
    }

    public String srvClustersList() {
        return url + "/clusters";
    }

    public String srvDeleteAttribute(String userId, String objectId, String attName) {
        return url + "/deleteAttribute?userId=" + userId + "&objectId=" + objectId + "&attName=" + attName;
    }

    public String srvDeleteObject(String userId, String type, String objectId) {
        //return url+"/deleteVObject?objectId="+objectId;
        return url + "/deleteVObject?userId=" + userId + "&attObjectType=" + type + "&objectHasAttId=" + type + "&name=" + objectId;
        //deleteVObject?userId=ector1984@gmail.com&attObjectType=3&objectHasAttId=3&name=teststar
    }

// /CatalogueInfoServlet
    public String srvDeleteBiblioNote(String userId, int noteId) {
        return url + "/DeleteBiblioNoteServlet?userId=" + userId + "&noteId=" + noteId;
    }

    public String srvGetCatalogue(String catalogId) {
        return url + "/CatalogueInfoServlet?catalogId=" + catalogId;
    }

    public String srvDeleteAttributeValue(String userId, String objectId, int vobjHasAttId) {
        return url + "/deleteValue?userId=" + userId + "&objectId=" + objectId + "&vobjHasAttId=" + vobjHasAttId;
    }

    public String srvDeleteVObject(String objectId) {
        return url + "/deleteVObject?objectId=" + objectId;
    }

    public String srvEnableUser(String userId, String name, String surname, String motivations) {
        return url + "/enableUser?userId=" + userId + "&name=" + name + "&surname=" + surname + "&motivations=" + motivations;
    }

    public String srvNewBiblioNoteRequest(String objectId) {
        objectId = objectId.replace(' ', '+');
        return url + "/newNoteRequest?objectId=" + objectId;
    }

    public String srvNewBiblioNoteResponse(int type) {
        return url + "/newNoteResponse?type=" + type;
    }

    public String srvNewBiblioAuthor(String name, String uri) {
        return url + "/InsertAuthor?Author=" + name + "&URI" + uri;
    }

    public String srvNewVObjectRequest(int type) {
        return url + "/newVObjectRequest?vobjectType=" + type;
    }

    public String srvNewVObjectResponse(int type) {
        return url + "/NewVObjectResponse?vobjectType=" + type;
    }

    public String srvParameterHistory(String attName, String vobjectId) {
        return url + "/paramHistory?attName=" + attName + "&vobjectId=" + vobjectId;
    }

    public String srvSearch(String toSearch) {
        toSearch = toSearch.replace(' ', '+');
        return url + "/PartialSearchServlet?value=" + toSearch;
    }

    public String srvUpdateAttribute() {
        return url + "/updateAttribute";
    }

    public String srvUpdateAttributeValue(String userId, int objectType, int objectHasAttId, String value, String name, String attribute) {
        //return url+"/updateValue?userId=" + userId + "&attObjectType=" + objectType + "&objectHasAttId=" + objectHasAttId + "&value=" + value;
        //&value=3.5&name=NGC+288&attribute=ra
        name = name.replace(' ', '+');
        return url + "/updateValue?userId=" + userId + "&attObjectType=" + objectType + "&objectHasAttId=" + objectHasAttId + "&value=" + value + "&name=" + name + "&attribute=" + attribute;
    }

    public String srvUpdateBiblioNote(int type) {
        return url + "/updateValue?type=" + type;
    }

    public String srvUserInfo(String userId) {
        return url + "/user?userId=" + userId;
    }

    public String srvUsersList() {
        return url + "/users";
    }

    public String srvAuthorList() {
        return url + "/ShowAuthors";
    }

    public String srvVObject(String objectId) {
        objectId = objectId.replace(' ', '+');
        return url + "/VObjectServlet?objectId=" + objectId;
    }

    public String srvNewAttribute(String userId, String objectId, String attName, String attDesc, String attUcd, String attDatatype, String attValue, int type) {

        objectId = objectId.replace(' ', '+');
        attName = attName.replace(' ', '+');
        attDesc = attDesc.replace(' ', '+');
        attUcd = attUcd.replace(' ', '+');

        return url + "/newAttribute?userId=" + userId + "&objectId=" + objectId + "&attName=" + attName + "&attDesc=" + attDesc + "&attUcd=" + attUcd + "&attDatatype=" + attDatatype + "&attValue=" + attValue + "&type=" + type;
    }

    public String srvNewAttributeValue(String userId, String objectId, int attId, String attValue) {
        objectId = objectId.replace(' ', '+');
        return url + "/NewAttributeValueServlet?userId=" + userId + "&objectId=" + objectId + "&attId=" + attId + "&attValue=" + attValue;
    }

    public String srvNewUserValue(String userId, String password, String name, String surname, String affiliation, String country, int active) {
        name = name.replace(' ', '+');
        surname = surname.replace(' ', '+');
        affiliation = affiliation.replace(' ', '+');
        country = country.replace(' ', '+');
        password = password.replace(' ', '+');
        return url + "/InsertUserServlet?userId=" + userId + "&password=" + password + "&name=" + name + "&surname=" + surname + "&affiliation=" + affiliation + "&country=" + country;
    }

    public String srvParameterSearch(String param, String value, String operator) {
        return url + "/ParameterServlet?toSearch=" + param + "&value=" + value + "&operator=" + operator;
    }
}
