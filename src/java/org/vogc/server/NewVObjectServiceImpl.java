package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import freemarker.template.TemplateException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vogc.client.NewVObjectService;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.VObject;
import org.vogc.client.datatype.VObjectAttribute;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author Luca
 */
public class NewVObjectServiceImpl extends RemoteServiceServlet implements NewVObjectService {

    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final long serialVersionUID = 1L;
    private final XMLParser xmlp = new XMLParser();

    public ArrayList<VObjectAttribute> getAttributesList(int objType) {
        ArrayList<VObjectAttribute> ret;
        String result = webapp_uc.WAgetAttributesList(objType);
        if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
            ErrorReport report = new ErrorReport();
            VObjectAttribute att = new VObjectAttribute();
            report.setCode(Messages.CONNECTION_NOT_FOUND);
            report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
            report.setAdditionalInfo("");
            att.setReport(report);
            ArrayList<VObjectAttribute> attList = new ArrayList<VObjectAttribute>(10);
            attList.add(att);
            ret = attList;
        } else {
            ret = xmlp.parseAttribute(result);
        }
        return ret;
    }

    public ErrorReport saveNewVObject(VObject obj, VObject obj2) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAsaveNewVObject(obj.getObjType(), obj, obj2);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
                //report.setMessage(result); // parse esito salvataggio oggetto
            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    public ErrorReport saveNewPulsar(VObject obj, VObject obj2) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAsaveNewVObject(obj.getObjType(), obj, obj2);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
                //report.setMessage(result); // parse esito salvataggio oggetto
            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    public ErrorReport nexImage(VObject obj, VObject obj2) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAsaveNewVObject(obj.getObjType(), obj, obj2);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
                //report.setMessage(result); // parse esito salvataggio oggetto
            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }
}
