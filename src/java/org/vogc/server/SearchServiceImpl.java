package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import freemarker.template.TemplateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vogc.client.SearchService;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.SearchResult;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author Sabrina Checola
 */
public class SearchServiceImpl extends RemoteServiceServlet implements SearchService {

    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final long serialVersionUID = 1L;
    private final XMLParser xmlp = new XMLParser();

    public SearchResult search(String toSearch) {
        SearchResult src = new SearchResult();
        try {
            ErrorReport report = new ErrorReport();
            String result = webapp_uc.WAsearch(toSearch);
            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
                src.setReport(report);
            } else {
                src = xmlp.parseSearchResult(result);
            }
        } catch (TemplateException ex) {
            Logger.getLogger(SearchServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return src;
    }
}
