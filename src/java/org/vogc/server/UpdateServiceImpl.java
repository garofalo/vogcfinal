package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import freemarker.template.TemplateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vogc.client.UpdateService;
import org.vogc.client.datatype.AdditionalInfoAttribute;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.Note;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author Sabrina
 */
public class UpdateServiceImpl extends RemoteServiceServlet implements UpdateService {

    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final long serialVersionUID = 1L;
    private final XMLParser xmlp = new XMLParser();

    public ErrorReport updateAttribute(AdditionalInfoAttribute attValues) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAupdateAttribute(attValues);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
            }
        } catch (TemplateException ex) {
            Logger.getLogger(UpdateServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    @Override
    public ErrorReport updateAttributeValue(String userId, int objectType, int objectHasAttId, String value, String name, String attribute) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAupdateAttributeValue(userId, objectType, objectHasAttId, value, name, attribute);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
            }
        } catch (TemplateException ex) {
            Logger.getLogger(UpdateServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    public ErrorReport updateBiblioRef(BiblioReference bibRef) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAupdateBiblioRef(bibRef);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);

            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    public ErrorReport updateNote(Note note) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAupdateNote(note);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);

            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewVObjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }
}
