package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import freemarker.template.TemplateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vogc.client.DeleteService;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author Sabrina
 */
public class DeleteServiceImpl extends RemoteServiceServlet implements DeleteService {

    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final long serialVersionUID = 1L;
    private final XMLParser xmlp = new XMLParser();

    public ErrorReport deleteAttribute(String userId, String objectId, String attName) {

        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAdeleteAttribute(userId, objectId, attName);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
            }

        } catch (TemplateException ex) {
            Logger.getLogger(DeleteServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    public ErrorReport deleteAttributeValue(String userId, String objectId, int vobjHasAttId) {
        ErrorReport report = new ErrorReport();
        try {

            String result = webapp_uc.WAdeleteAttributeValue(userId, objectId, vobjHasAttId);
            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
            }

        } catch (TemplateException ex) {
            Logger.getLogger(DeleteServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    public ErrorReport deleteBiblioNote(String userId, int noteId) {
        ErrorReport report = new ErrorReport();
        try {

            String result = webapp_uc.WAdeleteBiblioNote(userId, noteId);
            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
            }

        } catch (TemplateException ex) {
            Logger.getLogger(DeleteServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    public ErrorReport deleteObject(String userId, String type, String objectId) {
        ErrorReport report = new ErrorReport();
        try {

            String result = webapp_uc.WAdeleteObject(userId, type, objectId);
            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
            }

        } catch (TemplateException ex) {
            Logger.getLogger(DeleteServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }
}
