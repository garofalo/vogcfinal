package org.vogc.server;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.vogc.client.datatype.AdditionalInfoAttribute;
import org.vogc.client.datatype.Author;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.Catalogue;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.GClusterInfo;
import org.vogc.client.datatype.Image;
import org.vogc.client.datatype.InfoForNotes;
import org.vogc.client.datatype.Paper;
import org.vogc.client.datatype.PulsarInfo;
import org.vogc.client.datatype.SearchResult;
import org.vogc.client.datatype.StarInfo;
import org.vogc.client.datatype.User;
import org.vogc.client.datatype.VObject;
import org.vogc.client.datatype.VObjectAttribute;

/**
 *
 * @author Sabrina
 */
public class XMLParser {

    SAXBuilder parser = new SAXBuilder();
    final String INFO = "INFO";
    final String NAME = "name";
    final String VALUE = "value";
    final String PARAM = "PARAM";
    final String DESCRIPTION = "DESCRIPTION";
    final String RESOURCE = "RESOURCE";
    final String DATA = "DATA";
    final String TABLE = "TABLE";
    final String TABLEDATA = "TABLEDATA";
    final String ERROR_REPORT = "Error Report";

    public ErrorReport parseErrorReport(String errReport) {
        ErrorReport report = new ErrorReport();
        try {
            Document doc = parser.build(new StringReader(errReport));

            Namespace ns = Namespace.getNamespace("xsi", "http://www.ivoa.net/xml/VOTable/v1.1");
            Element rootVOTABLE = doc.getRootElement();
            List childrenINFO = rootVOTABLE.getChildren(INFO, ns);
            Iterator i = childrenINFO.iterator();

            while (i.hasNext()) {
                Element element = (Element) i.next();
                if (element.getAttributeValue(NAME).equals("errorCode")) {
                    report.setCode(element.getAttributeValue(VALUE));
                } else if (element.getAttributeValue(NAME).equals("errorMessage")) {
                    report.setMessage(element.getAttributeValue(VALUE));
                } else if (element.getAttributeValue(NAME).equals("additionalInfo")) {
                    report.setAdditionalInfo(element.getAttributeValue(VALUE));
                }
            }

        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    public ArrayList<VObjectAttribute> parseAttribute(String attributes) {
        ArrayList<VObjectAttribute> attList = new ArrayList<VObjectAttribute>(10);
        int k;
        try {

            Document doc = parser.build(new StringReader(attributes));
            Namespace ns = Namespace.getNamespace("xsi", "http://www.ivoa.net/xml/VOTable/v1.1");
            Element rootVOTABLE = doc.getRootElement();
            Element resource = rootVOTABLE.getChild(RESOURCE, ns);
            Element table = resource.getChild(TABLE, ns);
            Element data = table.getChild(DATA, ns);
            Element tabledata = data.getChild(TABLEDATA, ns);

            List elementTR = tabledata.getChildren("TR", ns);
            Iterator i = elementTR.iterator();
            while (i.hasNext()) {
                VObjectAttribute att = new VObjectAttribute();
                Element eleTR = (Element) i.next();
                List elementTD = eleTR.getChildren("TD", ns);
                Iterator j = elementTD.iterator();
                k = 0;
                while (j.hasNext()) {
                    Element eleTD = (Element) j.next();
                    if (k == 0) {
                        att.setName(eleTD.getTextTrim());
                        k++;
                    } else {
                        att.setDatatype(eleTD.getTextTrim());
                    }
                }
                attList.add(att);
            }
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        return attList;
    }

    public GClusterInfo parseGcluster(String gcluster) {
        GClusterInfo gc = new GClusterInfo();
//        ArrayList<VOAttribute> attDeclaration = new ArrayList<VOAttribute>();
//        ArrayList<AttributeValueInfo> values = new ArrayList<AttributeValueInfo>();
        ArrayList<AdditionalInfoAttribute> gcList = new ArrayList<AdditionalInfoAttribute>(10);

        try {

            //FileWriter wrt = new FileWriter(new File("C:/Documents and Settings/EM/Documenti/VOGCLUSTERS/VOGCLUSTERS/Freemarker Template/Templates/WebAppTemplate/ccc.txt"));
            Document doc = parser.build(new StringReader(gcluster));
            //      Namespace ns = Namespace.getNamespace("xsi", "http://www.ivoa.net/xml/VOTable/v1.1");
            Element rootVOTABLE = doc.getRootElement();

            List elementTR = rootVOTABLE.getChildren(PARAM/*
             * , ns
             */);
            Iterator i = elementTR.iterator();
            while (i.hasNext()) {
                Element elePARAM = (Element) i.next();
                if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("Id")) {
                    gc.setId(elePARAM.getAttributeValue(VALUE));
                } else if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("Name")) {
                    gc.setName(elePARAM.getAttributeValue(VALUE));
                } else if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("type")) {
                    gc.setType(elePARAM.getAttributeValue(VALUE));
                }

            }

            Element eleRESOURCE = rootVOTABLE.getChild(RESOURCE/*
             * , ns
             */);
            if (eleRESOURCE != null) {
                List listTABLE = eleRESOURCE.getChildren(TABLE/*
                 * , ns
                 */);
                Iterator t = listTABLE.iterator();
                while (t.hasNext()) {

                    Element eleTABLE = (Element) t.next();

                    if (eleTABLE.getAttributeValue("ID"/*
                     * , ns
                     */).equals("gcAtt")) {

                        Element eleDATA = eleTABLE.getChild(DATA/*
                         * , ns
                         */);
                        Element eleTABLEDATA = eleDATA.getChild(TABLEDATA/*
                         * , ns
                         */);
                        List listTR = eleTABLEDATA.getChildren("TR"/*
                         * , ns
                         */);

                        Iterator tr = listTR.iterator();
                        while (tr.hasNext()) {

                            Element eleTR = (Element) tr.next();
                            List listTD = eleTR.getChildren("TD"/*
                             * , ns
                             */);
//                        VOAttribute attributeDecl = new VOAttribute();
//                        AttributeValueInfo aValue = new AttributeValueInfo();

                            VObjectAttribute voAtt = new VObjectAttribute();
                            AdditionalInfoAttribute gcAtt = new AdditionalInfoAttribute();

                            Element id = (Element) listTD.get(0);
                            Element name = (Element) listTD.get(1);
                            Element desc = (Element) listTD.get(2);
                            Element dataType = (Element) listTD.get(3);
                            Element ucd = (Element) listTD.get(4);
                            Element type = (Element) listTD.get(5);
                            Element sourceId = (Element) listTD.get(6);
                            Element sourceType = (Element) listTD.get(7);
                            Element userName = (Element) listTD.get(8);
                            Element value = (Element) listTD.get(9);
                            Element date = (Element) listTD.get(10);
                            Element isFirst = (Element) listTD.get(11);
                            Element isPrimary = (Element) listTD.get(12);
                            Element gcHasAttId = (Element) listTD.get(13);
//------------------------------------------------------
                        /*
                             * attributeDecl.setId(Integer.parseInt(id.getValue()));
                             * attributeDecl.setName(name.getValue());
                             * attributeDecl.setDescription(desc.getValue());
                             * attributeDecl.setUcd(ucd.getValue());
                             * attributeDecl.setDatatype(dataType.getValue());
                             * attributeDecl.setType(Integer.parseInt(type.getValue()));
                             * attributeDecl.setIsPrimary(Integer.parseInt(isPrimary.getValue()));
                             *
                             * aValue.setId(Integer.parseInt(gcHasAttId.getValue()));
                             * aValue.setValue(value.getValue());
                             * aValue.setIsFirst(Integer.parseInt(isFirst.getValue()));
                             * aValue.setDate(date.getValue());
                             * aValue.setSourceId(sourceId.getValue());
                             * aValue.setSourceType(sourceType.getValue());
                             * aValue.setUserName(userName.getValue());
                             */
//--------------------------------------------------------
                            voAtt.setId(Integer.parseInt(id.getValue()));
                            voAtt.setName(name.getValue());
                            voAtt.setDescription(desc.getValue());
                            voAtt.setUcd(ucd.getValue());
                            voAtt.setDatatype(dataType.getValue());
                            voAtt.setType(Integer.parseInt(type.getValue()));
                            voAtt.setIsPrimary(Integer.parseInt(isPrimary.getValue()));
                            voAtt.setValue(value.getValue());

                            gcAtt.setGcHasAttId(gcHasAttId.getValue());
                            gcAtt.setSourceId(sourceId.getValue());
                            gcAtt.setSourceType(sourceType.getValue());
                            gcAtt.setUserName(userName.getValue());
                            gcAtt.setDate(date.getValue());
                            gcAtt.setIsFirst(isFirst.getValue());

                            gcAtt.setAttPrimaryInfo(voAtt);
                            gcList.add(gcAtt);

                        }
                        gc.setGcAtt(gcList);
                    }
                }
            }

            //wrt.close();
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        return gc;
    }

    public PulsarInfo parsePulsar(String pulsarInfo) {
        PulsarInfo pulsar = new PulsarInfo();
        ArrayList<AdditionalInfoAttribute> gcList = new ArrayList<AdditionalInfoAttribute>(10);
        try {

            //FileWriter wrt = new FileWriter(new File("C:/Documents and Settings/EM/Documenti/VOGCLUSTERS/VOGCLUSTERS/Freemarker Template/Templates/WebAppTemplate/ccc.txt"));
            Document doc = parser.build(new StringReader(pulsarInfo));
            //      Namespace ns = Namespace.getNamespace("xsi", "http://www.ivoa.net/xml/VOTable/v1.1");
            Element rootVOTABLE = doc.getRootElement();

            List elementTR = rootVOTABLE.getChildren(PARAM/*
             * , ns
             */);
            Iterator i = elementTR.iterator();
            while (i.hasNext()) {
                Element elePARAM = (Element) i.next();
                if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("Id")) {
                    pulsar.setId(elePARAM.getAttributeValue(VALUE));
                } else if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("gClusterId")) {
                    pulsar.setName(elePARAM.getAttributeValue(VALUE));
                } else if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("type")) {
                    pulsar.setType(elePARAM.getAttributeValue(VALUE));
                }

            }

            Element eleRESOURCE = rootVOTABLE.getChild(RESOURCE/*
             * , ns
             */);
            List listTABLE = eleRESOURCE.getChildren(TABLE/*
             * , ns
             */);
            Iterator t = listTABLE.iterator();
            while (t.hasNext()) {

                Element eleTABLE = (Element) t.next();

                if (eleTABLE.getAttributeValue("ID"/*
                 * , ns
                 */).equals("plsAtt")) {

                    Element eleDATA = eleTABLE.getChild(DATA/*
                     * , ns
                     */);
                    Element eleTABLEDATA = eleDATA.getChild(TABLEDATA/*
                     * , ns
                     */);
                    List listTR = eleTABLEDATA.getChildren("TR"/*
                     * , ns
                     */);

                    Iterator tr = listTR.iterator();
                    while (tr.hasNext()) {

                        Element eleTR = (Element) tr.next();
                        List listTD = eleTR.getChildren("TD"/*
                         * , ns
                         */);
//                        VOAttribute attributeDecl = new VOAttribute();
//                        AttributeValueInfo aValue = new AttributeValueInfo();

                        VObjectAttribute voAtt = new VObjectAttribute();
                        AdditionalInfoAttribute gcAtt = new AdditionalInfoAttribute();

                        Element id = (Element) listTD.get(0);
                        Element name = (Element) listTD.get(1);
                        Element dataType = (Element) listTD.get(2);
                        Element ucd = (Element) listTD.get(3);
                        Element desc = (Element) listTD.get(4);
                        Element value = (Element) listTD.get(5);
                        Element sourceType = (Element) listTD.get(6);
                        Element userName = (Element) listTD.get(7);
                        Element date = (Element) listTD.get(8);
                        Element sourceId = (Element) listTD.get(9);
                        Element isFirst = (Element) listTD.get(10);
                        Element isPrimary = (Element) listTD.get(11);
                        Element gcHasAttId = (Element) listTD.get(12);

//------------------------------------------------------
                        /*
                         * attributeDecl.setId(Integer.parseInt(id.getValue()));
                         * attributeDecl.setName(name.getValue());
                         * attributeDecl.setDescription(desc.getValue());
                         * attributeDecl.setUcd(ucd.getValue());
                         * attributeDecl.setDatatype(dataType.getValue());
                         * attributeDecl.setType(Integer.parseInt(type.getValue()));
                         * attributeDecl.setIsPrimary(Integer.parseInt(isPrimary.getValue()));
                         *
                         * aValue.setId(Integer.parseInt(gcHasAttId.getValue()));
                         * aValue.setValue(value.getValue());
                         * aValue.setIsFirst(Integer.parseInt(isFirst.getValue()));
                         * aValue.setDate(date.getValue());
                         * aValue.setSourceId(sourceId.getValue());
                         * aValue.setSourceType(sourceType.getValue());
                         * aValue.setUserName(userName.getValue());
                         */
//--------------------------------------------------------
                        voAtt.setId(Integer.parseInt(id.getValue()));
                        voAtt.setName(name.getValue());
                        voAtt.setDatatype(dataType.getValue());
                        voAtt.setUcd(ucd.getValue());
                        voAtt.setDescription(desc.getValue());
                        voAtt.setValue(value.getValue());
                        gcAtt.setSourceType(sourceType.getValue());
                        gcAtt.setUserName(userName.getValue());
                        gcAtt.setDate(date.getValue());
                        gcAtt.setSourceId(sourceId.getValue());
                        gcAtt.setIsFirst(isFirst.getValue());
                        voAtt.setIsPrimary(Integer.parseInt(isPrimary.getValue()));
                        //voAtt.setType(Integer.parseInt(type.getValue()));

                        gcAtt.setGcHasAttId(gcHasAttId.getValue());

                        gcAtt.setAttPrimaryInfo(voAtt);
                        gcList.add(gcAtt);

                    }
                    pulsar.setPlsAtt(gcList);
                }
            }

            //wrt.close();
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pulsar;
    }

    public StarInfo parseStar(String starInfo) {
        StarInfo star = new StarInfo();

        ArrayList<AdditionalInfoAttribute> gcList = new ArrayList<AdditionalInfoAttribute>(10);
        try {

            //FileWriter wrt = new FileWriter(new File("C:/Documents and Settings/EM/Documenti/VOGCLUSTERS/VOGCLUSTERS/Freemarker Template/Templates/WebAppTemplate/ccc.txt"));
            Document doc = parser.build(new StringReader(starInfo));
            //      Namespace ns = Namespace.getNamespace("xsi", "http://www.ivoa.net/xml/VOTable/v1.1");
            Element rootVOTABLE = doc.getRootElement();

            List elementTR = rootVOTABLE.getChildren(PARAM/*
             * , ns
             */);
            Iterator i = elementTR.iterator();
            while (i.hasNext()) {
                Element elePARAM = (Element) i.next();
                if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("Id")) {
                    star.setId(elePARAM.getAttributeValue(VALUE));
                } else if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("gClusterId")) {
                    star.setName(elePARAM.getAttributeValue(VALUE));
                } else if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("type")) {
                    star.setType(elePARAM.getAttributeValue(VALUE));
                }

            }

            Element eleRESOURCE = rootVOTABLE.getChild(RESOURCE/*
             * , ns
             */);
            List listTABLE = eleRESOURCE.getChildren(TABLE/*
             * , ns
             */);
            Iterator t = listTABLE.iterator();
            while (t.hasNext()) {

                Element eleTABLE = (Element) t.next();

                if (eleTABLE.getAttributeValue("ID"/*
                 * , ns
                 */).equals("starAtt")) {

                    Element eleDATA = eleTABLE.getChild(DATA/*
                     * , ns
                     */);
                    Element eleTABLEDATA = eleDATA.getChild(TABLEDATA/*
                     * , ns
                     */);
                    List listTR = eleTABLEDATA.getChildren("TR"/*
                     * , ns
                     */);

                    Iterator tr = listTR.iterator();
                    while (tr.hasNext()) {

                        Element eleTR = (Element) tr.next();
                        List listTD = eleTR.getChildren("TD"/*
                         * , ns
                         */);
//                        VOAttribute attributeDecl = new VOAttribute();
//                        AttributeValueInfo aValue = new AttributeValueInfo();

                        VObjectAttribute voAtt = new VObjectAttribute();
                        AdditionalInfoAttribute gcAtt = new AdditionalInfoAttribute();

                        Element id = (Element) listTD.get(0);
                        Element name = (Element) listTD.get(1);
                        Element dataType = (Element) listTD.get(2);
                        Element ucd = (Element) listTD.get(3);
                        Element desc = (Element) listTD.get(4);
                        Element value = (Element) listTD.get(5);
                        Element sourceType = (Element) listTD.get(6);
                        Element userName = (Element) listTD.get(7);
                        Element date = (Element) listTD.get(8);
                        Element sourceId = (Element) listTD.get(9);
                        Element isFirst = (Element) listTD.get(10);
                        Element isPrimary = (Element) listTD.get(11);
                        Element gcHasAttId = (Element) listTD.get(12);

//------------------------------------------------------
                        /*
                         * attributeDecl.setId(Integer.parseInt(id.getValue()));
                         * attributeDecl.setName(name.getValue());
                         * attributeDecl.setDescription(desc.getValue());
                         * attributeDecl.setUcd(ucd.getValue());
                         * attributeDecl.setDatatype(dataType.getValue());
                         * attributeDecl.setType(Integer.parseInt(type.getValue()));
                         * attributeDecl.setIsPrimary(Integer.parseInt(isPrimary.getValue()));
                         *
                         * aValue.setId(Integer.parseInt(gcHasAttId.getValue()));
                         * aValue.setValue(value.getValue());
                         * aValue.setIsFirst(Integer.parseInt(isFirst.getValue()));
                         * aValue.setDate(date.getValue());
                         * aValue.setSourceId(sourceId.getValue());
                         * aValue.setSourceType(sourceType.getValue());
                         * aValue.setUserName(userName.getValue());
                         */
//--------------------------------------------------------
                        voAtt.setId(Integer.parseInt(id.getValue()));
                        voAtt.setName(name.getValue());
                        voAtt.setDatatype(dataType.getValue());
                        voAtt.setUcd(ucd.getValue());
                        voAtt.setDescription(desc.getValue());
                        voAtt.setValue(value.getValue());
                        gcAtt.setSourceType(sourceType.getValue());
                        gcAtt.setUserName(userName.getValue());
                        gcAtt.setDate(date.getValue());
                        gcAtt.setSourceId(sourceId.getValue());
                        gcAtt.setIsFirst(isFirst.getValue());
                        voAtt.setIsPrimary(Integer.parseInt(isPrimary.getValue()));
                        //voAtt.setType(Integer.parseInt(type.getValue()));

                        gcAtt.setGcHasAttId(gcHasAttId.getValue());

                        gcAtt.setAttPrimaryInfo(voAtt);
                        gcList.add(gcAtt);

                    }
                    star.setStarAtt(gcList);
                }
            }

            //wrt.close();
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        return star;
    }

    public SearchResult parseSearchResult(String toSearch) {
//        FileWriter wrt = null;

        SearchResult src = new SearchResult();
        ArrayList<String> tagList = new ArrayList<String>(5);
        ArrayList<Image> imgList = new ArrayList<Image>(5);
        ArrayList<VObject> objList = new ArrayList<VObject>(10);
        try {
            //wrt = new FileWriter(new File("C:/Users/Sabrina/Desktop/Templates/WebAppTemplate/ccc.txt"));

            try {

                Document doc = parser.build(new StringReader(toSearch));
                Element rootVOTABLE = doc.getRootElement();
                //      wrt.write(rootVOTABLE.getChildText(DESCRIPTION));
                //      wrt.close();
                String desc = rootVOTABLE.getChildText(DESCRIPTION);

                if (desc.compareTo(ERROR_REPORT) == 0) {
                    ErrorReport report = new ErrorReport();
                    List listTD = rootVOTABLE.getChildren(INFO);
                    Element code = (Element) listTD.get(0);
                    Element msg = (Element) listTD.get(1);
                    Element addInfo = (Element) listTD.get(2);
                    report.setCode(code.getAttributeValue(VALUE));
                    report.setMessage(msg.getAttributeValue(VALUE));
                    report.setAdditionalInfo(addInfo.getAttributeValue(VALUE));
                    src.setReport(report);

                } else {
                    String searched = rootVOTABLE.getChild(PARAM).getAttributeValue(VALUE);

                    src.setSearched(searched);
                    List tableElement = rootVOTABLE.getChild(RESOURCE).getChildren(TABLE);
                    Iterator t = tableElement.iterator();
                    while (t.hasNext()) {
                        Element table = (Element) t.next();
                        if (table.getAttributeValue("ID").equals("tags")) {
                            Element eleDATA = table.getChild(DATA);
                            Element eleTABLEDATA = eleDATA.getChild(TABLEDATA);
                            List listTR = eleTABLEDATA.getChildren("TR");
                            Iterator tr = listTR.iterator();
                            while (tr.hasNext()) {
                                Element eleTR = (Element) tr.next();
                                List listTD = eleTR.getChildren("TD");
                                Element tagName = (Element) listTD.get(0);
                                tagList.add(tagName.getValue());
                            }
                            src.setTags(tagList);
                        }
                        if (table.getAttributeValue("ID").equals("images")) {
                            Element eleDATA = table.getChild(DATA);
                            Element eleTABLEDATA = eleDATA.getChild(TABLEDATA);
                            List listTR = eleTABLEDATA.getChildren("TR");
                            Iterator tr = listTR.iterator();
                            while (tr.hasNext()) {
                                Element eleTR = (Element) tr.next();
                                List listTD = eleTR.getChildren("TD");
                                Element imgId = (Element) listTD.get(0);
                                Element imgName = (Element) listTD.get(1);
                                Element imgUri = (Element) listTD.get(2);
                                Element objId = (Element) listTD.get(3);
                                Image img = new Image();
                                img.setId(Integer.parseInt(imgId.getValue()));
                                img.setName(imgName.getValue());
                                img.setUri(imgUri.getValue());
                                img.setObjectId(objId.getValue());
                                imgList.add(img);
                            }
                            src.setImages(imgList);
                        } else if (table.getAttributeValue("ID").equals("vobjects")) {
                            Element eleDATA = table.getChild(DATA);
                            Element eleTABLEDATA = eleDATA.getChild(TABLEDATA);
                            List listTR = eleTABLEDATA.getChildren("TR");
                            Iterator tr = listTR.iterator();
                            while (tr.hasNext()) {
                                Element eleTR = (Element) tr.next();
                                List listTD = eleTR.getChildren("TD");
                                Element objId = (Element) listTD.get(0);
                                Element type = (Element) listTD.get(1);
                                VObject obj = new VObject();
                                obj.setId(objId.getValue());
                                obj.setType(Integer.parseInt(type.getValue()));
                                objList.add(obj);
                            }
                            src.setObjects(objList);
                        }
                    }
                }
            } catch (JDOMException ex) {
                Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return src;
    }

    public ArrayList<VObject> parseClustersList(String result) {

//        FileWriter wrt = null;
        ArrayList<VObject> clustersList = new ArrayList<VObject>(50);
        VObject cluster = new VObject();
//        try {
        //  wrt = new FileWriter(new File("C:/Users/Sabrina/Desktop/Templates/WebAppTemplate/ccc.txt"));
        //   wrt.write(result);
        //   wrt.close();
        try {
            Document doc = parser.build(new StringReader(result));
            Element rootVOTABLE = doc.getRootElement();
            String desc = rootVOTABLE.getChildText(DESCRIPTION);
            if (desc.compareTo(ERROR_REPORT) == 0) {
                ErrorReport report = new ErrorReport();
                List listTD = rootVOTABLE.getChildren(INFO);
                Element code = (Element) listTD.get(0);
                Element msg = (Element) listTD.get(1);
                Element addInfo = (Element) listTD.get(2);
                report.setCode(code.getAttributeValue(VALUE));
                report.setMessage(msg.getAttributeValue(VALUE));
                report.setAdditionalInfo(addInfo.getAttributeValue(VALUE));
                cluster.setReport(report);
            } else {
                List tableElement = rootVOTABLE.getChild(RESOURCE).getChildren(TABLE);
                Iterator t = tableElement.iterator();
                while (t.hasNext()) {
                    Element table = (Element) t.next();
                    if (table.getAttributeValue("ID").equals("galCluster")) {
                        Element eleDATA = table.getChild(DATA);
                        Element eleTABLEDATA = eleDATA.getChild(TABLEDATA);
                        List listTR = eleTABLEDATA.getChildren("TR");
                        Iterator tr = listTR.iterator();
                        while (tr.hasNext()) {
                            VObject obj = new VObject();
                            obj.setType(1); // ammasso galattico
                            Element eleTR = (Element) tr.next();
                            List listTD = eleTR.getChildren("TD");
                            Element objId = (Element) listTD.get(0);
                            obj.setId(objId.getValue());
                            clustersList.add(obj);
                        }
                    } else if (table.getAttributeValue("ID").equals("exgalCluster")) {
                        Element eleDATA = table.getChild(DATA);
                        Element eleTABLEDATA = eleDATA.getChild(TABLEDATA);
                        List listTR = eleTABLEDATA.getChildren("TR");
                        Iterator tr = listTR.iterator();
                        while (tr.hasNext()) {
                            VObject obj = new VObject();
                            obj.setType(2); // ammasso extragalattico
                            Element eleTR = (Element) tr.next();
                            List listTD = eleTR.getChildren("TD");
                            Element objId = (Element) listTD.get(0);
                            obj.setId(objId.getValue());
                            clustersList.add(obj);
                        }
                    }
                }
            }
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }

//        } catch (IOException ex) {
//           // Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return clustersList;
    }

    public InfoForNotes parseInfoForNotes(String result) {
        InfoForNotes info = new InfoForNotes();
        FileWriter wrt = null;

        ArrayList<AdditionalInfoAttribute> attList = new ArrayList<AdditionalInfoAttribute>(5);
        ArrayList<Author> authList = new ArrayList<Author>(5);
        ArrayList<Paper> paperList = new ArrayList<Paper>(5);
        ArrayList<Image> imageList = new ArrayList<Image>(5);
        try {
            Document doc = parser.build(new StringReader(result));
            Element rootVOTABLE = doc.getRootElement();
//            wrt = new FileWriter(new File("C:/Users/Sabrina/Desktop/Templates/WebAppTemplate/ccc.txt"));
            String desc = rootVOTABLE.getChildText(DESCRIPTION);
            if (desc.compareTo(ERROR_REPORT) == 0) {
                ErrorReport report = new ErrorReport();
                List listTD = rootVOTABLE.getChildren(INFO);
                Element code = (Element) listTD.get(0);
                Element msg = (Element) listTD.get(1);
                Element addInfo = (Element) listTD.get(2);
                report.setCode(code.getAttributeValue(VALUE));
                report.setMessage(msg.getAttributeValue(VALUE));
                report.setAdditionalInfo(addInfo.getAttributeValue(VALUE));
                info.setReport(report);
            } else {
                VObject obj = new VObject();
                List listParam = rootVOTABLE.getChildren(PARAM);
                Element objId = (Element) listParam.get(0);
                Element objName = (Element) listParam.get(1);
                Element objType = (Element) listParam.get(2);

                obj.setId(objId.getAttributeValue(VALUE));
                obj.setName(objName.getAttributeValue(VALUE));
                obj.setType(Integer.parseInt(objType.getAttributeValue(VALUE)));
                info.setObj(obj);

                List tableElement = rootVOTABLE.getChild(RESOURCE).getChildren(TABLE);
                Iterator t = tableElement.iterator();
                while (t.hasNext()) {
                    Element table = (Element) t.next();
                    if (table.getAttributeValue("ID").equals("objAtt")) {
                        Element eleDATA = table.getChild(DATA);
                        Element eleTABLEDATA = eleDATA.getChild(TABLEDATA);
                        List listTR = eleTABLEDATA.getChildren("TR");
                        Iterator tr = listTR.iterator();
                        while (tr.hasNext()) {

                            Element eleTR = (Element) tr.next();
                            List listTD = eleTR.getChildren("TD");

                            Element attValueId = (Element) listTD.get(0);
                            Element attName = (Element) listTD.get(1);
                            Element attDesc = (Element) listTD.get(2);
                            Element sourceId = (Element) listTD.get(3);
                            Element sourceType = (Element) listTD.get(4);
                            Element userName = (Element) listTD.get(5);
                            Element value = (Element) listTD.get(6);
                            Element date = (Element) listTD.get(7);

                            VObjectAttribute voAtt = new VObjectAttribute();
                            AdditionalInfoAttribute addInfo = new AdditionalInfoAttribute();
                            addInfo.setGcHasAttId(attValueId.getValue());
                            addInfo.setSourceId(sourceId.getValue());
                            addInfo.setSourceType(sourceType.getValue());
                            addInfo.setUserName(userName.getValue());
                            addInfo.setDate(date.getValue());
                            voAtt.setName(attName.getValue());
                            voAtt.setDescription(attDesc.getValue());
                            voAtt.setValue(value.getValue());
                            addInfo.setAttPrimaryInfo(voAtt);
                            attList.add(addInfo);
                        }

                        info.setAtt(attList);
                    }
                    if (table.getAttributeValue("ID").equals("authList")) {
                        Element eleDATA = table.getChild(DATA);
                        Element eleTABLEDATA = eleDATA.getChild(TABLEDATA);
                        List listTR = eleTABLEDATA.getChildren("TR");
                        Iterator tr = listTR.iterator();
                        while (tr.hasNext()) {
                            Element eleTR = (Element) tr.next();
                            List listTD = eleTR.getChildren("TD");

                            Element authorId = (Element) listTD.get(0);
                            Element authorName = (Element) listTD.get(1);

                            wrt.write(" <" + authorId.getValue() + "> ");

                            Author auth = new Author();
                            auth.setId(Integer.parseInt(authorId.getValue()));
                            auth.setName(authorName.getValue());
                            authList.add(auth);
                        }
                        info.setAuthors(authList);

                    } else if (table.getAttributeValue("ID").equals("papList")) {
                        Element eleDATA = table.getChild(DATA);
                        Element eleTABLEDATA = eleDATA.getChild(TABLEDATA);
                        List listTR = eleTABLEDATA.getChildren("TR");
                        Iterator tr = listTR.iterator();
                        while (tr.hasNext()) {
                            Element eleTR = (Element) tr.next();
                            List listTD = eleTR.getChildren("TD");

                            Element paperId = (Element) listTD.get(0);
                            Element paperName = (Element) listTD.get(1);
                            Paper p = new Paper();
                            p.setId(Integer.parseInt(paperId.getValue()));
                            p.setName(paperName.getValue());
                            paperList.add(p);
                        }
                        info.setPapers(paperList);
                    } else if (table.getAttributeValue("ID").equals("imgList")) {
                        Element eleDATA = table.getChild(DATA);
                        Element eleTABLEDATA = eleDATA.getChild(TABLEDATA);
                        List listTR = eleTABLEDATA.getChildren("TR");
                        Iterator tr = listTR.iterator();
                        while (tr.hasNext()) {
                            Element eleTR = (Element) tr.next();
                            List listTD = eleTR.getChildren("TD");

                            Element imgId = (Element) listTD.get(0);
                            Element userId = (Element) listTD.get(1);
                            Element imgName = (Element) listTD.get(2);
                            Element imgDesc = (Element) listTD.get(3);
                            Element imgScale = (Element) listTD.get(4);
                            Element imgFormat = (Element) listTD.get(5);
                            Element imgDim = (Element) listTD.get(6);
                            Element imgUri = (Element) listTD.get(7);
                            Element imgDate = (Element) listTD.get(8);
                            Element imgType = (Element) listTD.get(9);

                            Image img = new Image();
                            img.setId(Integer.parseInt(imgId.getValue()));
                            User u = new User();
                            u.setUserId(userId.getValue());
                            img.setUser(u);
                            img.setName(imgName.getValue());
                            img.setDesc(imgDesc.getValue());
                            img.setScale(imgScale.getValue());
                            img.setFormat(imgFormat.getValue());
                            img.setDimension(imgDim.getValue());
                            img.setUri(imgUri.getValue());
                            img.setDate(imgDate.getValue());
                            img.setType(Integer.parseInt(imgType.getValue()));
                            imageList.add(img);

                        }
                        info.setImages(imageList);
                    }
                }
            }
            wrt.close();
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        return info;
    }

    public User parseUserInfo(String result) {
        User usr = new User();
        //FileWriter wrt = null;
        try {
            // wrt = new FileWriter(new File("C:/Users/Sabrina/Desktop/Templates/WebAppTemplate/ccc.txt"));
            Document doc = parser.build(new StringReader(result));
            Element rootVOTABLE = doc.getRootElement();
            //  wrt.write(rootVOTABLE.toString());
            //  wrt.close();
            String desc = rootVOTABLE.getChildText(DESCRIPTION);

            if (desc.compareTo(ERROR_REPORT) == 0) {
                ErrorReport report = new ErrorReport();
                List listTD = rootVOTABLE.getChildren(INFO);
                Element code = (Element) listTD.get(0);
                Element msg = (Element) listTD.get(1);
                Element addInfo = (Element) listTD.get(2);
                report.setCode(code.getAttributeValue(VALUE));
                report.setMessage(msg.getAttributeValue(VALUE));
                report.setAdditionalInfo(addInfo.getAttributeValue(VALUE));
                usr.setReport(report);
            } else {
                List listParam = rootVOTABLE.getChildren(PARAM);
                Element userId = (Element) listParam.get(0);
                Element name = (Element) listParam.get(1);
                Element surname = (Element) listParam.get(2);
                Element motiv = (Element) listParam.get(3);
                usr.setUserId(userId.getAttributeValue(VALUE));
                usr.setName(name.getAttributeValue(VALUE));
                usr.setSurname(surname.getAttributeValue(VALUE));
                usr.setMotivations(motiv.getAttributeValue(VALUE));
            }

        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usr;
    }

    public ArrayList<User> parseUsersList(String result) {
        ArrayList<User> users = new ArrayList<User>(100);
//        FileWriter wrt = null;
        try {
            //  wrt = new FileWriter(new File("C:/Users/Sabrina/Desktop/Templates/WebAppTemplate/ccc.txt"));
            //wrt.write("ciao");
            // wrt.close();
            Document doc = parser.build(new StringReader(result));
            Element rootVOTABLE = doc.getRootElement();

            String desc = rootVOTABLE.getChildText(DESCRIPTION);

            if (desc.compareTo(ERROR_REPORT) == 0) {
                ErrorReport report = new ErrorReport();
                List listTD = rootVOTABLE.getChildren(INFO);
                Element code = (Element) listTD.get(0);
                Element msg = (Element) listTD.get(1);
                Element addInfo = (Element) listTD.get(2);
                report.setCode(code.getAttributeValue(VALUE));
                report.setMessage(msg.getAttributeValue(VALUE));
                report.setAdditionalInfo(addInfo.getAttributeValue(VALUE));
                User usr = new User();
                usr.setReport(report);
                users.add(usr);
            } else {
                Element eleTABLEDATA = rootVOTABLE.getChild(RESOURCE).getChild(TABLE).getChild(DATA).getChild(TABLEDATA);

                List listTR = eleTABLEDATA.getChildren("TR");
                Iterator tr = listTR.iterator();
                while (tr.hasNext()) {
                    User usr = new User();
                    Element eleTR = (Element) tr.next();
                    List listTD = eleTR.getChildren("TD");
                    Element userId = (Element) listTD.get(0);
                    Element name = (Element) listTD.get(1);
                    Element surname = (Element) listTD.get(2);
                    Element motiv = (Element) listTD.get(3);
                    usr.setUserId(userId.getValue());
                    usr.setName(name.getValue());
                    usr.setSurname(surname.getValue());
                    usr.setMotivations(motiv.getValue());
                    users.add(usr);
                }
            }
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }

    public ArrayList<Author> parseAuthorList(String result) {
        ArrayList<Author> authorsOut = new ArrayList<Author>(5);
//        FileWriter wrt = null;
        try {
            //  wrt = new FileWriter(new File("C:/Users/Sabrina/Desktop/Templates/WebAppTemplate/ccc.txt"));
            //wrt.write("ciao");
            // wrt.close();
            Document doc = parser.build(new StringReader(result));
            Element rootVOTABLE = doc.getRootElement();

            String desc = rootVOTABLE.getChildText(DESCRIPTION);

            if (desc.compareTo(ERROR_REPORT) == 0) {
                ErrorReport report = new ErrorReport();
                List listTD = rootVOTABLE.getChildren(INFO);
                Element code = (Element) listTD.get(0);
                Element msg = (Element) listTD.get(1);
                Element addInfo = (Element) listTD.get(2);
                report.setCode(code.getAttributeValue(VALUE));
                report.setMessage(msg.getAttributeValue(VALUE));
                report.setAdditionalInfo(addInfo.getAttributeValue(VALUE));

            } else {
                Element eleTABLEDATA = rootVOTABLE.getChild(RESOURCE).getChild(TABLE).getChild(DATA).getChild(TABLEDATA);

                List listTR = eleTABLEDATA.getChildren("TR");
                Iterator tr = listTR.iterator();
                while (tr.hasNext()) {
                    Author usr = new Author();
                    Element eleTR = (Element) tr.next();
                    List listTD = eleTR.getChildren("TD");
                    Element userId = (Element) listTD.get(0);
                    Element name = (Element) listTD.get(1);

                    usr.setId(Integer.parseInt(userId.getValue()));
                    usr.setName(name.getValue());

                    authorsOut.add(usr);
                }
            }
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return authorsOut;
    }

    Catalogue parseCatalogue(String result) {
        Catalogue cat = new Catalogue();
        try {

            Document doc = parser.build(new StringReader(result));
            Element rootVOTABLE = doc.getRootElement();
            String desc = rootVOTABLE.getChildText(DESCRIPTION);
            if (desc.compareTo(ERROR_REPORT) == 0) {
                ErrorReport report = new ErrorReport();
                List listTD = rootVOTABLE.getChildren(INFO);
                Element code = (Element) listTD.get(0);
                Element msg = (Element) listTD.get(1);
                Element addInfo = (Element) listTD.get(2);
                report.setCode(code.getAttributeValue(VALUE));
                report.setMessage(msg.getAttributeValue(VALUE));
                report.setAdditionalInfo(addInfo.getAttributeValue(VALUE));
                cat.setReport(report);
            }
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cat;
    }

    public ArrayList<AdditionalInfoAttribute> parseValuesList(String result) {

        // o pezzotto ci sta !!!!! by ETTORE
        AdditionalInfoAttribute value = new AdditionalInfoAttribute();
        ArrayList<AdditionalInfoAttribute> values = new ArrayList<AdditionalInfoAttribute>(10);
        try {
            //  wrt = new FileWriter(new File("C:/Users/Sabrina/Desktop/Templates/WebAppTemplate/ccc.txt"));
            //wrt.write("ciao");
            // wrt.close();
            Document doc = parser.build(new StringReader(result));
            Element rootVOTABLE = doc.getRootElement();

            String desc = rootVOTABLE.getChildText(DESCRIPTION);

            if (desc.compareTo(ERROR_REPORT) == 0) {
                ErrorReport report = new ErrorReport();
                List listTD = rootVOTABLE.getChildren(INFO);
                Element code = (Element) listTD.get(0);
                Element msg = (Element) listTD.get(1);
                Element addInfo = (Element) listTD.get(2);
                report.setCode(code.getAttributeValue(VALUE));
                report.setMessage(msg.getAttributeValue(VALUE));
                report.setAdditionalInfo(addInfo.getAttributeValue(VALUE));
                value.setReport(report);
                values.add(value);
            } else {
                Element eleTABLEDATA = rootVOTABLE.getChild(RESOURCE).getChild(TABLE).getChild(DATA).getChild(TABLEDATA);

                List listTR = eleTABLEDATA.getChildren("TR");
                Iterator tr = listTR.iterator();
                while (tr.hasNext()) {

                    Element eleTR = (Element) tr.next();
                    List listTD = eleTR.getChildren("TD");
                    Element valueH = (Element) listTD.get(0);
//                    Element first = (Element) listTD.get(1);
                    Element date = (Element) listTD.get(2);
                    Element sourceId = (Element) listTD.get(3);
                    Element sourceType = (Element) listTD.get(2);
                    Element userName = (Element) listTD.get(3);
                    VObjectAttribute voAtt = new VObjectAttribute();
                    AdditionalInfoAttribute addInfo = new AdditionalInfoAttribute();
                    voAtt.setValue(valueH.getValue());
                    addInfo.setIsFirst(valueH.getValue()); /// questo contiene il valore è un pezzotto dovrebbe contenere is first
                    addInfo.setDate(date.getValue());
                    addInfo.setSourceId(sourceId.getValue());
                    addInfo.setSourceType(sourceType.getValue());
                    addInfo.setUserName(userName.getValue());

                    addInfo.setAttPrimaryInfo(voAtt);
                    values.add(addInfo);

                }
            }
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return values;

    }

    public GClusterInfo parseBiblioGcluster(String gcluster) {
        GClusterInfo gc = new GClusterInfo();
//        ArrayList<VOAttribute> attDeclaration = new ArrayList<VOAttribute>();
//        ArrayList<AttributeValueInfo> values = new ArrayList<AttributeValueInfo>();
        ArrayList<BiblioReference> gcList = new ArrayList<BiblioReference>(5);

        try {

            //FileWriter wrt = new FileWriter(new File("C:/Documents and Settings/EM/Documenti/VOGCLUSTERS/VOGCLUSTERS/Freemarker Template/Templates/WebAppTemplate/ccc.txt"));
            Document doc = parser.build(new StringReader(gcluster));
            //      Namespace ns = Namespace.getNamespace("xsi", "http://www.ivoa.net/xml/VOTable/v1.1");
            Element rootVOTABLE = doc.getRootElement();

            List elementTR = rootVOTABLE.getChildren(PARAM/*
             * , ns
             */);
            Iterator i = elementTR.iterator();
            while (i.hasNext()) {
                Element elePARAM = (Element) i.next();
                if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("Id")) {
                    gc.setId(elePARAM.getAttributeValue(VALUE));
                } else if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("Name")) {
                    gc.setName(elePARAM.getAttributeValue(VALUE));
                } else if (elePARAM.getAttributeValue(NAME/*
                 * , ns
                 */).equals("type")) {
                    gc.setType(elePARAM.getAttributeValue(VALUE));
                }

            }

            Element eleRESOURCE = rootVOTABLE.getChild(RESOURCE/*
             * , ns
             */);
            List listTABLE = eleRESOURCE.getChildren(TABLE/*
             * , ns
             */);
            Iterator t = listTABLE.iterator();
            while (t.hasNext()) {

                Element eleTABLE = (Element) t.next();

                if (eleTABLE.getAttributeValue("ID"/*
                 * , ns
                 */).equals("BiblioRefCluster")) {

                    Element eleDATA = eleTABLE.getChild(DATA/*
                     * , ns
                     */);
                    Element eleTABLEDATA = eleDATA.getChild(TABLEDATA/*
                     * , ns
                     */);
                    List listTR = eleTABLEDATA.getChildren("TR"/*
                     * , ns
                     */);

                    Iterator tr = listTR.iterator();
                    while (tr.hasNext()) {

                        Element eleTR = (Element) tr.next();
                        List listTD = eleTR.getChildren("TD"/*
                         * , ns
                         */);
//                        VOAttribute attributeDecl = new VOAttribute();
//                        AttributeValueInfo aValue = new AttributeValueInfo();
//
//                        VObjectAttribute voAtt = new VObjectAttribute();
//                        AdditionalInfoAttribute gcAtt = new AdditionalInfoAttribute();
                        BiblioReference biblio = new BiblioReference();

                        Element biblioRefId = (Element) listTD.get(0);
                        Element paperId = (Element) listTD.get(1);
                        Element paperName = (Element) listTD.get(2);
                        Element paperUri = (Element) listTD.get(3);
                        Element userId = (Element) listTD.get(4);
                        Element userName = (Element) listTD.get(5);
                        Element title = (Element) listTD.get(6);
                        Element desc = (Element) listTD.get(7);
                        Element uri = (Element) listTD.get(8);
                        Element year = (Element) listTD.get(9);
                        Element date = (Element) listTD.get(10);

                        biblio.setId(Integer.parseInt(biblioRefId.getValue()));
                        Paper pap = new Paper();
                        pap.setName(paperName.getValue());
                        pap.setId(Integer.parseInt(paperId.getValue()));
                        pap.setUri(paperUri.getValue());
                        biblio.setPaper(pap);
                        User usr = new User();
                        usr.setName(userName.getValue());
                        usr.setUserId(userId.getValue());
                        biblio.setUser(usr);
                        biblio.setTitle(title.getValue());
                        biblio.setDescription(desc.getValue());
                        biblio.setUri(uri.getValue());
                        biblio.setYear(year.getValue());
                        biblio.setDate(date.getValue());

                        gcList.add(biblio);

                    }
                    gc.setRefAboutAttribute(gcList);
                }
            }

            //wrt.close();
        } catch (JDOMException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        return gc;
    }
}
