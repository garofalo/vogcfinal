package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import freemarker.template.TemplateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vogc.client.NewAttributeService;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.VObjectAttribute;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author Sabrina
 */
public class NewAttributeServiceImpl extends RemoteServiceServlet implements NewAttributeService {

    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final long serialVersionUID = 1L;
    private final XMLParser xmlp = new XMLParser();

    @Override
    public ErrorReport newAttribute(VObjectAttribute att, String userId, String objectId) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAnewAttribute(att, userId, objectId);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewAttributeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }

    @Override
    public ErrorReport newAttributeValue(String userId, String objectId, int attId, String attValue) {
        ErrorReport report = new ErrorReport();
        try {
            String result = webapp_uc.WAnewAttributeValue(userId, objectId, attId, attValue);

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");
            } else {
                report = xmlp.parseErrorReport(result);
            }

        } catch (TemplateException ex) {
            Logger.getLogger(NewAttributeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return report;
    }
}
