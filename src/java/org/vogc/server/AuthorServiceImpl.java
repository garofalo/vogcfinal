package org.vogc.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import freemarker.template.TemplateException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vogc.client.AuthorService;
import org.vogc.client.datatype.Author;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.server.lowlevel.Messages;
import org.vogc.server.lowlevel.WebAppUseCase;

/**
 *
 * @author Ettore
 */
public class AuthorServiceImpl extends RemoteServiceServlet implements AuthorService {

    private static final WebAppUseCase webapp_uc = new WebAppUseCase();
    private static final long serialVersionUID = 1L;
    private final XMLParser xmlp = new XMLParser();

    public ArrayList<Author> ShowlistAuthors() {
        ErrorReport report = new ErrorReport();
        Author auth = new Author();
        ArrayList<Author> authorList = new ArrayList<Author>(5);

        try {
            String result = webapp_uc.WAgetAuthorsList();

            if (result.equals(Messages.CONNECTION_NOT_FOUND)) {
                report.setCode(Messages.CONNECTION_NOT_FOUND);
                report.setMessage(Messages.getErrorString(Messages.CONNECTION_NOT_FOUND));
                report.setAdditionalInfo("");

                authorList.add(auth);
            } else {
                authorList = xmlp.parseAuthorList(result);
            }
        } catch (TemplateException ex) {
            Logger.getLogger(UserInfoServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return authorList;
    }
}
