package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.User;

/**
 *
 * @author Francesco
 */
@RemoteServiceRelativePath("loginregistrationservice")
public interface LoginRegistrationService extends RemoteService {

    public ErrorReport registration(User person);
}
