package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.User;

/**
 *
 * @author Francesco
 */
public interface LoginRegistrationServiceAsync {

    public void registration(User person, AsyncCallback<ErrorReport> asyncCallback);
}
