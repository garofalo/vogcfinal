package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import org.vogc.client.datatype.ErrorReport;

/**
 *
 * @author Sabrina
 */
public interface DeleteServiceAsync {

    public void deleteAttribute(String userId, String objectId, String attName, AsyncCallback<ErrorReport> asyncCallback);

    public void deleteAttributeValue(String userId, String objectId, int vobjHasAttId, AsyncCallback<ErrorReport> asyncCallback);

    public void deleteBiblioNote(String userId, int noteId, AsyncCallback<ErrorReport> asyncCallback);

    public void deleteObject(String userId, String type, String objectId, AsyncCallback<ErrorReport> asyncCallback);
}
