package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.InfoForNotes;
import org.vogc.client.datatype.Note;

/**
 *
 * @author Sabrina
 */
@RemoteServiceRelativePath("newbiblionoteservice")
public interface NewBiblioNoteService extends RemoteService {

    public InfoForNotes getBibblioNoteInfo(String s);

    public ErrorReport saveBiblioRef(BiblioReference br);

    public ErrorReport saveNote(Note note);

    public ErrorReport saveNoteNew(BiblioReference br);

    public ErrorReport saveNewAuthor(String n, String u);
}
