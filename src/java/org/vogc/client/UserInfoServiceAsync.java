package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import org.vogc.client.datatype.User;

/**
 *
 * @author Sabrina
 */
public interface UserInfoServiceAsync {

    public void getUserInfo(String userId, AsyncCallback<User> asyncCallback);

    public void getUsersList(AsyncCallback<ArrayList<User>> asyncCallback);
}
