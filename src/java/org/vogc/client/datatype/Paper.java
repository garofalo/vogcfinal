package org.vogc.client.datatype;

import java.io.Serializable;
//import java.util.ArrayList;

/**
 *
 * @author Luca
 */
public class Paper implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private String name;
    private String uri;
//    private ArrayList<Author> authorsList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
