package org.vogc.client.datatype;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Luca
 */
public class InfoForNotes implements Serializable {

    private static final long serialVersionUID = 1L;
    private VObject obj;
    private ArrayList<AdditionalInfoAttribute> att;
    private ArrayList<Author> authors;
    private ArrayList<Paper> papers;
    private ArrayList<Image> images;
    private ErrorReport report;

    public ArrayList<AdditionalInfoAttribute> getAtt() {
        return att;
    }

    public void setAtt(ArrayList<AdditionalInfoAttribute> att) {
        this.att = att;
    }

    public ArrayList<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<Author> authors) {
        this.authors = authors;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public VObject getObj() {
        return obj;
    }

    public void setObj(VObject obj) {
        this.obj = obj;
    }

    public ArrayList<Paper> getPapers() {
        return papers;
    }

    public void setPapers(ArrayList<Paper> papers) {
        this.papers = papers;
    }

    public ErrorReport getReport() {
        return report;
    }

    public void setReport(ErrorReport report) {
        this.report = report;
    }
}
