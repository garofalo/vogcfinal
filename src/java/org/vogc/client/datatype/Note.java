package org.vogc.client.datatype;

import java.io.Serializable;

/**
 *
 * @author Luca
 */
public class Note implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private String title;
    private String description;
    private String type;
    private String date;
    private int attValueId;
    private String vobjectId;
    private int imageId;
    private User user;
    private String objectId;
    private int objectType;

    public int getAttValueId() {
        return attValueId;
    }

    public void setAttValueId(int attValueId) {
        this.attValueId = attValueId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getVobjectId() {
        return vobjectId;
    }

    public void setVobjectId(String vobjectId) {
        this.vobjectId = vobjectId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public int getObjectType() {
        return objectType;
    }

    public void setObjectType(int objectType) {
        this.objectType = objectType;
    }
}
