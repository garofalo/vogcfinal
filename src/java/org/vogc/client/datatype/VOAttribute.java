package org.vogc.client.datatype;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Luca
 */
public class VOAttribute implements Serializable {

    private static final long serialVersionUID = 1L;
    /* ELIMINA */
    private int id;
    private String name;
    private String description;
    private String ucd;
    private String datatype;
    private int type;
    private int isPrimary;
    private ArrayList<AttributeValueInfo> values;
    private ErrorReport report;

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(int isPrimary) {
        this.isPrimary = isPrimary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ErrorReport getReport() {
        return report;
    }

    public void setReport(ErrorReport report) {
        this.report = report;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUcd() {
        return ucd;
    }

    public void setUcd(String ucd) {
        this.ucd = ucd;
    }

    public ArrayList<AttributeValueInfo> getValues() {
        return values;
    }

    public void setValues(ArrayList<AttributeValueInfo> values) {
        this.values = values;
    }
}
