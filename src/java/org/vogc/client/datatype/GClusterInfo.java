package org.vogc.client.datatype;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Luca
 */
public class GClusterInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    private ErrorReport report;
    private String id;
    private String name;
    private String type;
    private ArrayList<AdditionalInfoAttribute> gcAtt;    // ELIMINA
    private ArrayList<Note> noteAboutCluster;
    private ArrayList<Note> noteAboutOneAttribute;
    private ArrayList<BiblioReference> refAboutCluster;
    private ArrayList<BiblioReference> refAboutAttribute;
    private ArrayList<Image> imageList;
    private ArrayList<String> tags;
    private ArrayList<VOAttribute> attributes;
    private ArrayList<VObject> pulsarsList;
    private ArrayList<VObject> starsList;

    public ArrayList<AdditionalInfoAttribute> getGcAtt() {
        return gcAtt;
    }

    
    public void setGcAtt(ArrayList<AdditionalInfoAttribute> gcAtt) {
        this.gcAtt = gcAtt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Image> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<Image> imageList) {
        this.imageList = imageList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Note> getNoteAboutCluster() {
        return noteAboutCluster;
    }

    public void setNoteAboutCluster(ArrayList<Note> noteAboutCluster) {
        this.noteAboutCluster = noteAboutCluster;
    }

    public ArrayList<Note> getNoteAboutOneAttribute() {
        return noteAboutOneAttribute;
    }

    public void setNoteAboutOneAttribute(ArrayList<Note> noteAboutOneAttribute) {
        this.noteAboutOneAttribute = noteAboutOneAttribute;
    }

    public ArrayList<VObject> getPulsarsList() {
        return pulsarsList;
    }

    public void setPulsarsList(ArrayList<VObject> pulsarsList) {
        this.pulsarsList = pulsarsList;
    }

    public ArrayList<BiblioReference> getRefAboutAttribute() {
        return refAboutAttribute;
    }

    public void setRefAboutAttribute(ArrayList<BiblioReference> refAboutAttribute) {
        this.refAboutAttribute = refAboutAttribute;
    }

    public ArrayList<BiblioReference> getRefAboutCluster() {
        return refAboutCluster;
    }

    public void setRefAboutCluster(ArrayList<BiblioReference> refAboutCluster) {
        this.refAboutCluster = refAboutCluster;
    }

    public ErrorReport getReport() {
        return report;
    }

    public void setReport(ErrorReport report) {
        this.report = report;
    }

    public ArrayList<VObject> getStarsList() {
        return starsList;
    }

    public void setStarsList(ArrayList<VObject> starsList) {
        this.starsList = starsList;
    }

   public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<VOAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<VOAttribute> attributes) {
        this.attributes = attributes;
    }
}
