package org.vogc.client.datatype;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Luca
 */
// utilizzato per passare GLI ATTRIBUTI dal CLIENT al SERVER
// bisogna aggiungere note, bibref, immg e tag
public class VObject implements Serializable {

    private static final long serialVersionUID = 1L;
    private int objType;       // this specify if the object is CLUSTER, PULSAR or STAR
    private String clusterId;  // only for PULSAR and STAR
    private String id;         // the object id
    private int type;       // only for cluster, specify if it is galactic or extragactic
    private String name;       // only for cluster, specify the cluster name
    private String sourceId;
    private ArrayList<VObjectAttribute> Attribute;
    private ErrorReport report;

    public VObject() {
    }

    public ArrayList<VObjectAttribute> getAttribute() {
        return Attribute;
    }

    public void setAttribute(ArrayList<VObjectAttribute> Attribute) {
        this.Attribute = Attribute;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ErrorReport getReport() {
        return report;
    }

    public void setReport(ErrorReport report) {
        this.report = report;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getClusterId() {
        return clusterId;
    }

    public void setClusterId(String clusterId) {
        this.clusterId = clusterId;
    }

    public int getObjType() {
        return objType;
    }

    public void setObjType(int objType) {
        this.objType = objType;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }
}
