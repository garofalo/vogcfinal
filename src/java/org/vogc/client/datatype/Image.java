package org.vogc.client.datatype;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Luca
 */
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private String objectId;
    private String name;
    private String desc;
    private String scale;
    private String format;
    private String dimension;
    private String uri;
    private String date;
    private int type;
    private ArrayList<Note> noteAboutImage;
    private ArrayList<BiblioReference> refAboutImage;
    private User user;
    private ArrayList<String> tags;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Note> getNoteAboutImage() {
        return noteAboutImage;
    }

    public void setNoteAboutImage(ArrayList<Note> noteAboutImage) {
        this.noteAboutImage = noteAboutImage;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public ArrayList<BiblioReference> getRefAboutImage() {
        return refAboutImage;
    }

    public void setRefAboutImage(ArrayList<BiblioReference> refAboutImage) {
        this.refAboutImage = refAboutImage;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

   public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
