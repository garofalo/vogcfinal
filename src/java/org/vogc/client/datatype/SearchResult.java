package org.vogc.client.datatype;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Luca
 */
public class SearchResult implements Serializable {

    private static final long serialVersionUID = 1L;
    private ErrorReport report;
    private String searched;
    private ArrayList<Image> images;
    private ArrayList<VObject> objects;
    private ArrayList<String> tags;

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public ArrayList<VObject> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<VObject> objects) {
        this.objects = objects;
    }

    public String getSearched() {
        return searched;
    }

    public void setSearched(String searched) {
        this.searched = searched;
    }

    public ErrorReport getReport() {
        return report;
    }

    public void setReport(ErrorReport report) {
        this.report = report;
    }

   public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }
}
