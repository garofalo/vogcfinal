package org.vogc.client.datatype;

import java.io.Serializable;

/**
 *
 * @author Luca
 */
public class Author implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private String name;
    private String uri; // sito web autore

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
