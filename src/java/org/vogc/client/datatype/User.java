package org.vogc.client.datatype;

import java.io.Serializable;

/**
 *
 * @author Luca
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    private String userId;
    private String name;
    private String surname;
    private String nameSurname;
    private String motivations;
    private String userMail;
    private String country;
    private String affiliation;
    private String password;
    private ErrorReport report;

    public String getMotivations() {
        return motivations;
    }

    public void setMotivations(String motivations) {
        this.motivations = motivations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ErrorReport getReport() {
        return report;
    }

    public void setReport(ErrorReport report) {
        this.report = report;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUserMail() {
        return userMail;
    }

    public String getCountry() {
        return country;
    }
}