package org.vogc.client.datatype;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Luca
 */
public class StarInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    private ErrorReport report;
    private String id;
    private String name;
    private String type;
    private ArrayList<AdditionalInfoAttribute> starAtt;    // ELIMINA
    private ArrayList<Note> noteAboutCluster;
    private ArrayList<Note> noteAboutOneAttribute;
    private ArrayList<BiblioReference> refAboutCluster;
    private ArrayList<BiblioReference> refAboutAttribute;
    private ArrayList<Image> imageList;
    private ArrayList<String> tags;
    private ArrayList<VOAttribute> attributes;

    public ArrayList<VOAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<VOAttribute> attributes) {
        this.attributes = attributes;
    }

    public ArrayList<AdditionalInfoAttribute> getGcAtt() {
        return starAtt;
    }

    public void setGcAtt(ArrayList<AdditionalInfoAttribute> gcAtt) {
        this.starAtt = gcAtt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Image> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<Image> imageList) {
        this.imageList = imageList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStarAtt(ArrayList<AdditionalInfoAttribute> starAtt) {
        this.starAtt = starAtt;
    }

    public ArrayList<Note> getNoteAboutCluster() {
        return noteAboutCluster;
    }

    public void setNoteAboutCluster(ArrayList<Note> noteAboutCluster) {
        this.noteAboutCluster = noteAboutCluster;
    }

    public ArrayList<Note> getNoteAboutOneAttribute() {
        return noteAboutOneAttribute;
    }

    public void setNoteAboutOneAttribute(ArrayList<Note> noteAboutOneAttribute) {
        this.noteAboutOneAttribute = noteAboutOneAttribute;
    }

    public ArrayList<BiblioReference> getRefAboutAttribute() {
        return refAboutAttribute;
    }

    public void setRefAboutAttribute(ArrayList<BiblioReference> refAboutAttribute) {
        this.refAboutAttribute = refAboutAttribute;
    }

    public ArrayList<BiblioReference> getRefAboutCluster() {
        return refAboutCluster;
    }

    public void setRefAboutCluster(ArrayList<BiblioReference> refAboutCluster) {
        this.refAboutCluster = refAboutCluster;
    }

    public ErrorReport getReport() {
        return report;
    }

    public void setReport(ErrorReport report) {
        this.report = report;
    }

   public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
