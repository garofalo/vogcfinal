package org.vogc.client.datatype;

/**
 *
 * @author Luca
 */
public class BiblioNote {

    private int type;
    private BiblioReference biblioRef;
    private Note note;
    private ErrorReport report;

    public BiblioReference getBiblioRef() {
        return biblioRef;
    }

    public void setBiblioRef(BiblioReference biblioRef) {
        this.biblioRef = biblioRef;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public ErrorReport getReport() {
        return report;
    }

    public void setReport(ErrorReport report) {
        this.report = report;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
