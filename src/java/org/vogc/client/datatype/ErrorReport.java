package org.vogc.client.datatype;

import java.io.Serializable;

/**
 *
 * @author Luca
 */
public class ErrorReport implements Serializable {

    private static final long serialVersionUID = 1L;
    private String code_;
    private String message_;
    private String additionalInfo_;

    public void setCode(String code) {
        code_ = code;
    }

    public void setMessage(String message) {
        message_ = message;
    }

    public void setAdditionalInfo(String info) {
        additionalInfo_ = info;
    }

    public String getCode() {
        return code_;
    }

    public String getMessage() {
        return message_;
    }

    public String getAdditionalInfo() {
        return additionalInfo_;
    }

    @Override
    public String toString() {
        return "codice:" + code_ + "\n messaggio:" + message_ + "\n additional:" + additionalInfo_;
    }
}
