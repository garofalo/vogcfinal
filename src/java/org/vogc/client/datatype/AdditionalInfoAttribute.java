package org.vogc.client.datatype;

import java.io.Serializable;

/**
 *
 * @author Luca
 */
public class AdditionalInfoAttribute implements Serializable {

    private static final long serialVersionUID = 1L;
    private VObjectAttribute attPrimaryInfo;
    private String sourceId;
    private String sourceType;
    private String userName;
    private String date;
    private String isFirst;
    private String gcHasAttId;
    ErrorReport report;

    public VObjectAttribute getAttPrimaryInfo() {
        return attPrimaryInfo;
    }

    public void setAttPrimaryInfo(VObjectAttribute attPrimaryInfo) {
        this.attPrimaryInfo = attPrimaryInfo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGcHasAttId() {
        return gcHasAttId;
    }

    public void setGcHasAttId(String gcHasAttId) {
        this.gcHasAttId = gcHasAttId;
    }

    public String getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(String isFirst) {
        this.isFirst = isFirst;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ErrorReport getReport() {
        return report;
    }

    public void setReport(ErrorReport report) {
        this.report = report;
    }
}
