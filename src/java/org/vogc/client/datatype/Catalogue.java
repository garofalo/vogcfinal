package org.vogc.client.datatype;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Luca
 */
public class Catalogue implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private String title;
    private String desc;
    private String year;
    private String uri;
    private String date;
    private ArrayList<Author> authors;
    private ErrorReport error;

    public ArrayList<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<Author> authors) {
        this.authors = authors;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public ErrorReport getReport() {
        return error;
    }

    public void setReport(ErrorReport error) {
        this.error = error;
    }
}
