package org.vogc.client.gui;

/**
 *
 * @author EM
 */
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.IMenuButton;
import com.smartgwt.client.widgets.menu.Menu;
import org.vogc.client.gui.data.HeaderMenuRecord;

public class MenuForm extends VLayout {

    Menu menu;
    IMenuButton button;

    public MenuForm(int i) {

        setAlign(Alignment.LEFT);
        menu = new Menu();
        menu.setItems(HeaderMenuRecord.getMenuRecords(i));
        button = new IMenuButton(HeaderMenuRecord.getMenuTitle(i), menu);
        addMember(button);
    }
}
