package org.vogc.client.gui.data;

/**
 *
 * @author EM
 */
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceFloatField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class FormFieldAdvancedSearch extends DataSource {

    private static FormFieldAdvancedSearch instance = null;

    public static FormFieldAdvancedSearch getInstance() {
        if (instance == null) {
            instance = new FormFieldAdvancedSearch();
        }
        return instance;
    }

    private FormFieldAdvancedSearch() {

        setID("worldDS");
        setRecordXPath("/List/country");
        DataSourceIntegerField pkField = new DataSourceIntegerField("pk");
        pkField.setHidden(true);
        pkField.setPrimaryKey(true);

        DataSourceTextField countryCodeField = new DataSourceTextField("countryCode", "NAME");
        DataSourceFloatField countryNameField = new DataSourceFloatField("countryName", "Gal. Longitude");
        DataSourceFloatField capitalField = new DataSourceFloatField("capital", "Gal. Latitude");
        DataSourceFloatField governmentField = new DataSourceFloatField("government", "R_sun");
        DataSourceFloatField continentField = new DataSourceFloatField("continent", "R_gc");
        DataSourceFloatField independenceField = new DataSourceFloatField("independence", "X");
        DataSourceFloatField areaField = new DataSourceFloatField("area", "Y");
        DataSourceFloatField populationField = new DataSourceFloatField("population", "Z");


        setFields(pkField, countryCodeField, countryNameField, capitalField, governmentField,
                continentField, independenceField, areaField, populationField);

        setDataURL("ds/test_data/world.data.xml");
        setClientOnly(true);
    }
}
