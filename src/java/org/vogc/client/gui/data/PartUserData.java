package org.vogc.client.gui.data;

/**
 *
 * @author Ettore
 */
import java.util.ArrayList;
import org.vogc.client.datatype.User;

public class PartUserData {

    private static PartUserData istanza = null;

    public static PartUserData getInstance() {
        if (istanza == null) {
            istanza = new PartUserData();
        }
        return istanza;
    }

    private PartUserData() {
    }

    

//    public PartUserRecord[] emptyRec() {
//        records = null;
//        return records;
//    }

    /**
     *
     * @param gcAtt
     * @return
     */
    public PartUserRecord[] getRecords(ArrayList<User> gcAtt) {
       
        return getNewRecordsTags(gcAtt);
    }

    private PartUserRecord[] getNewRecordsTags(ArrayList<User> userAtt) {
        PartUserRecord[] ritorno = new PartUserRecord[userAtt.size()];

        int k = 0;
        for (User usr : userAtt) {
            ritorno[k] = new PartUserRecord(usr.getUserId(), usr.getName(), usr.getSurname());
            k++;
        }
        return ritorno;
    }
}
