package org.vogc.client.gui.data;

/**
 *
 * @author EM
 */
import com.smartgwt.client.widgets.grid.ListGridRecord;

public final class PartClusterRecord extends ListGridRecord {

    public PartClusterRecord(String partName) {
        setPartName(partName);
    }

    public PartClusterRecord(String partName, String partSrc) {
        setPartName(partName);
        setPartSrc(partSrc);
    }

    public void setPartName(String partName) {
        setAttribute("partClusterName", partName);
    }

    public void setPartSrc(String partSrc) {
        setAttribute("partClusterSrc", partSrc);
    }
}