package org.vogc.client.gui.data;

import java.util.ArrayList;
import org.vogc.client.datatype.AdditionalInfoAttribute;

/**
 *
 * @author EM
 */
public class PartPulsarData {

    private static PartPulsarData istanza = null;
    public static final ArrayList<String> listaRicerca = new ArrayList<String>(10);
    public static PartClusterRecord[] records;

    public static PartPulsarData getInstance() {
        if (istanza == null) {
            istanza = new PartPulsarData();
        }
        return istanza;
    }

    private PartPulsarData() {
    }

//    public PartClusterRecord[] getRecords(int i, ArrayList<String> listaRicerca, ArrayList<String> listona) {
//
//        if (records == null) {
//            if (i == 0) {
//                records = getNewRecordsFromSearch(listaRicerca);
//            } else if (i == 1) {
//                records = getNewRecordsValue(listaRicerca, listona);
//            }
//        }
//        return records;
//
//    }
    public PartClusterRecord[] getRec() {
        return records;
    }

    public PartClusterRecord[] emptyRec() {
        records = null;
        return records;
    }

    public PartClusterRecord[] getNewRecordsFromSearch(ArrayList<String> listaRicerca) {
        PartClusterRecord[] ritorno = new PartClusterRecord[listaRicerca.size()];

        int i = 0;

        for (String string : listaRicerca) {
            ritorno[i] = new PartClusterRecord(string);
            i++;
        }
        return ritorno;
    }

    public PartClusterRecord[] getNewRecordsValue(ArrayList<String> listaRicerca, ArrayList<String> listona) {

        PartClusterRecord[] ritorno = new PartClusterRecord[listaRicerca.size()];


        int i = 0;
        for (String nome : listaRicerca) {
            for (String value : listona) {
                ritorno[i] = new PartClusterRecord(nome, value);
                i++;
            }
        }

        return ritorno;

    }

    public PartClusterRecord[] getRecords(ArrayList<AdditionalInfoAttribute> gcAtt) {

        return getNewRecordsTags(gcAtt);
    }

    private PartClusterRecord[] getNewRecordsTags(ArrayList<AdditionalInfoAttribute> gcAtt) {
        PartClusterRecord[] ritorno = new PartClusterRecord[gcAtt.size()];
        int k = 0;
        for (AdditionalInfoAttribute att : gcAtt) {
            ritorno[k] = new PartClusterRecord(att.getAttPrimaryInfo().getName(), att.getAttPrimaryInfo().getValue());
            k++;
        }
        return ritorno;
    }
}