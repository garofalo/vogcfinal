package org.vogc.client.gui.data;

import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Ettore
 */
final class PartAuthorRecord extends ListGridRecord {

   

    PartAuthorRecord(int partIDName, String partNameAuthor) {
        setPartIDName(partIDName);
        setPartNameAuthor(partNameAuthor);

    }

    public void setPartIDName(int partIDName) {
        setAttribute("partUserNameA", partIDName);
    }

    public void setPartNameAuthor(String partNameAuthor) {
        setAttribute("partNameUserA", partNameAuthor);
    }
}
