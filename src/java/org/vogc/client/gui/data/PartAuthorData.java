package org.vogc.client.gui.data;

/**
 *
 * @author Ettore
 */
import java.util.ArrayList;
import org.vogc.client.datatype.Author;

public class PartAuthorData {

    private static PartAuthorData istanza = null;

    public static PartAuthorData getInstance() {
        if (istanza == null) {
            istanza = new PartAuthorData();
        }
        return istanza;
    }

    private PartAuthorData() {
    }

  

    public PartAuthorRecord[] getRecords(ArrayList<Author> gcAtt) {
      
        return getNewRecordsTags(gcAtt);
    }

    private PartAuthorRecord[] getNewRecordsTags(ArrayList<Author> AuthorAtt) {
        PartAuthorRecord[] ritorno = new PartAuthorRecord[AuthorAtt.size()];
        int k = 0;
        for (Author usr : AuthorAtt) {
            ritorno[k] = new PartAuthorRecord(usr.getId(), usr.getName());
            k++;
        }
        return ritorno;
    }
}
