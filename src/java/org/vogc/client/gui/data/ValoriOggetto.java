package org.vogc.client.gui.data;

/**
 *
 * @author EM
 */
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class ValoriOggetto extends DataSource {

    private static ValoriOggetto instance = null;

    public static ValoriOggetto getInstance() {
        if (instance == null) {
            instance = new ValoriOggetto("supplyItemLocalDS");
        }
        return instance;
    }

    private ValoriOggetto(String id) {

        setID(id);
        DataSourceIntegerField pkField = new DataSourceIntegerField("itemID");
        pkField.setHidden(true);
        pkField.setPrimaryKey(true);

        DataSourceTextField itemNameField = new DataSourceTextField("att", "ATTRIBUTE", 128, true);
        DataSourceTextField skuField = new DataSourceTextField("val", "VALUE", 10, true);


        setFields(pkField, itemNameField, skuField);

        setClientOnly(true);

    }
}