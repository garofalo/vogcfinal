package org.vogc.client.gui.data;

import java.util.ArrayList;

/**
 *
 * @author EM
 */
public class PartData {

    private static PartData istanza = null;
    public static final ArrayList<String> listaRicerca = new ArrayList<String>(10);
    public static PartRecord[] records;

    public static PartData getInstance() {
        if (istanza == null) {
            istanza = new PartData();
        }
        return istanza;
    }

    private PartData() {
    }

    public PartRecord[] getRecords(ArrayList<String> listaRicerca, ArrayList<String> listona) {
        return getNewRecordsValue(listaRicerca, listona);
    }

    public PartRecord[] getRecords(ArrayList<String> listaRicerca) {
        return getNewRecordsTags(listaRicerca);

    }


    public PartRecord[] getNewRecordsTags(ArrayList<String> listaRicerca) {
        PartRecord[] ritorno = new PartRecord[listaRicerca.size()];

        int i = 0;

        for (String string : listaRicerca) {
            ritorno[i] = new PartRecord(string);
            i++;
        }
        return ritorno;
    }

    public PartRecord[] getNewRecordsValue(ArrayList<String> listaRicerca, ArrayList<String> listona) {
        int t = listaRicerca.size();


        if (t < listona.size()) {
            t = listona.size();
        }

        PartRecord[] ritorno = new PartRecord[t];
        int i = 0;
        for (String nome : listaRicerca) {
            for (String value : listona) {
                ritorno[i] = new PartRecord(nome, value);
                i++;
            }
        }

        return ritorno;

    }
}
