package org.vogc.client.gui.data;

/**
 *
 * @author Ettore
 */
import com.smartgwt.client.widgets.grid.ListGridRecord;

public final class PartClusterRecordBiblio extends ListGridRecord {

 

    public PartClusterRecordBiblio(String partNameBiblio, String partSrcBiblio) {
        setPartNameBiblio(partNameBiblio);
        setPartSrcBiblio(partSrcBiblio);
    }

    public void setPartNameBiblio(String partNameBiblio) {
        setAttribute("partClusterNameBiblio", partNameBiblio);
    }

    public void setPartSrcBiblio(String partSrcBiblio) {
        setAttribute("partClusterSrcBiblio", partSrcBiblio);
    }
}
