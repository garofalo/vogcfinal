package org.vogc.client.gui.data;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

/**
 *
 * @author EM
 */
public class HeaderMenuRecord {

    final static String[] TITLE = new String[]{
        "Manuals",
        "External Resources",
        "Dame Services",
        "Science Cases",
        "Documents",
        "Info"
    };
    final static String LINK = "http://dame.dsf.unina.it/vogclusters.html#manuals";
    final static String LINK2 = "http://dame.dsf.unina.it/vogclusters.html";
    final static String LINK3 = "http://dame.dsf.unina.it/vogclusters.html";
    final static String LINK4 = "http://localhost:8080/MyDameFE/data/help/appHelp";
    private static final String[] SUB1 = new String[]{
        //  "DAME Book;pdf;" + link,
        //  "DAME Book;pdf;" + link,
        "User Manual;url;" + LINK,
        "Web page;url;" + LINK, //"Extend DAME;pdf;" + link
    };
    private static final String[] SUB2 = new String[]{
        "Harris;url;http://www.physics.mcmaster.ca/Globular.html", "WEBDA;url;http://www.univie.ac.at/webda/", "Variable Stars;url;http://www.astro.utoronto.ca/~cclement/read.html", "GCblog;url;http://globularclusters.wordpress.com/"};
    private static final String[] SUB3 = new String[]{
        "DAME WARE;url;" + "http://dame.dsf.unina.it/beta_info.html",
        "SDSS;url;" + "http://dames.scope.unina.it/",
        "WFXT time calc;url;" + "http://dame.dsf.unina.it/dame_wfxt.html",
        "STraDiWA web app;url;" + "http://dame.dsf.unina.it/dame_td.html"
    };
    private static final String[] SUB4 = new String[]{
        "Photometric Redshifts;url;" + "http://dame.dsf.unina.it/dame_photoz.html",
        "Photometric Quasars;url;" + "http://dame.dsf.unina.it/dame_qso.html",
        "Globular Clusters;url;" + "http://dame.dsf.unina.it/dame_gcs.html",
        "Image Segmentation;url;" + "http://dame.dsf.unina.it/next.html"
    };
    private static final String[] SUB5 = new String[]{
        "Docs & lectures;url;" + "http://dame.dsf.unina.it/documents.html",
        "Science Production;url;" + "http://dame.dsf.unina.it/science_papers.html",
        "Newsletters;url;" + "http://dame.dsf.unina.it/newsletters.html",
        "Release Notes;url;" + "http://dame.dsf.unina.it/vogclusters.html#notes",};
    private static final String[] SUB6 = new String[]{
        "Offical Website;url;" + "http://dame.dsf.unina.it/index.html",
        "Write us;email;" + "mailto:vogclusters@gmail.com",
        "About Us;url;" + "http://dame.dsf.unina.it/project_members.html",
        "Faq;url;" + "http://dame.dsf.unina.it/vogclusters.html#faq"
    };
    private static final String[][] subList = new String[][]{SUB1, SUB2, SUB3, SUB4, SUB5, SUB6};

    public static int lenght() {

        return subList.length;
    }

    public static String getMenuTitle(int i) {
        return TITLE[i];
    }

    public static MenuItem[] getMenuRecords(int i) {
        String[] subMenu = subList[i];
        MenuItem[] result = new MenuItem[subMenu.length];

        for (int recordIndex = 0; recordIndex < subMenu.length; ++recordIndex) {
            final String[] fieldValues = subMenu[recordIndex].split(";");
            result[recordIndex] = new MenuItem();
            result[recordIndex].setTitle(fieldValues[0]);
            result[recordIndex].setIcon("woo/icon_" + fieldValues[1] + ".png");

            result[recordIndex].addClickHandler(new ClickHandler() {
                public void onClick(MenuItemClickEvent event) {
                    if (fieldValues[1].equalsIgnoreCase("url") || fieldValues[1].equalsIgnoreCase("email")) {
                        Window.open(fieldValues[2], "_blank", "");
                    } else if (fieldValues[1].equalsIgnoreCase("pdf")) {
                        String baseURL = GWT.getHostPageBaseURL();
                        String url = baseURL + "Download?ssid="
                                // + DataSession.getInstance().getSsid() + "&userMail="
                                // + DataSession.getInstance().getEmail() + "&fileUri="
                                + fieldValues[2];
                        Window.open(url, "", "menubar=no,location=no,resizable=no,"
                                + "scrollbars=no,status=no,toolbar=no,width=0,height=0");
                    }
                }
            });
        }
        return result;
    }
}
