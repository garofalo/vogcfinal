package org.vogc.client.gui.data;

/**
 *
 * @author EM
 */
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class FormFieldType extends DataSource {

    private static FormFieldType instance = null;

    public static FormFieldType getInstance() {
        if (instance == null) {
            instance = new FormFieldType();
        }
        return instance;
    }

    private FormFieldType() {
        setID("animalDS");
        DataSourceTextField TypeField = new DataSourceTextField("Type", "type");
        TypeField.setValueMap("Cluster", " Pulsar", "Star");
        setFields(TypeField);
        setClientOnly(true);
    }
}