package org.vogc.client.gui.data;

import com.smartgwt.client.data.Record;

/**
 *
 * @author EM
 *
 *
 */
public class TempSearchObject {

    public Record[] rec;

    public TempSearchObject() {
    }

    public Record[] getRec() {
        return rec;
    }

    public void setRec(Record[] rec) {
        this.rec = rec;
    }
}
