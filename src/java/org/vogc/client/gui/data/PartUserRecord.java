package org.vogc.client.gui.data;

import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Ettore
 */
final class PartUserRecord extends ListGridRecord {

  
    PartUserRecord(String partIDName, String partNameUser, String partSurname) {
        setPartIDName(partIDName);
        setPartNameUser(partNameUser);
        setPartSurname(partSurname);
    }

    public void setPartIDName(String partIDName) {
        setAttribute("partUserName", partIDName);
    }

    public void setPartNameUser(String partNameUser) {
        setAttribute("partNameUser", partNameUser);
    }

    public void setPartSurname(String partSurname) {
        setAttribute("partSurnameUser", partSurname);
    }
}
