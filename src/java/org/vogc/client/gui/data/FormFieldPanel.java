package org.vogc.client.gui.data;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;

public class FormFieldPanel extends DataSource {

    private static FormFieldPanel instance = null;

    public static FormFieldPanel getInstance() {
        if (instance == null) {
            instance = new FormFieldPanel();
        }
        return instance;
    }

    private FormFieldPanel() {

        DataSource dataSource = new DataSource();
        dataSource.setID("regularExpression");

        RegExpValidator regExpValidator = new RegExpValidator();
        regExpValidator.setExpression("^([a-zA-Z0-9_.\\-+])+@(([a-zA-Z0-9\\-])+\\.)+[a-zA-Z0-9]{2,4}$");



        DataSourceTextField dsTextField = new DataSourceTextField("USERNAME");
        dsTextField.setTitle("USERNAME");
        dsTextField.setValidators(regExpValidator);

        dataSource.setFields(dsTextField);

        final DynamicForm form = new DynamicForm();
        form.setWidth(300);
        form.setDataSource(dataSource);



        IButton validateButton = new IButton();
        validateButton.setTitle("  SEND  ");

        validateButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                form.validate();
            }
        });

        setFields(dsTextField);

        setClientOnly(true);

    }
}
