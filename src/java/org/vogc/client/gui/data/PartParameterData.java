package org.vogc.client.gui.data;

import java.util.ArrayList;
import org.vogc.client.datatype.VObject;

/**
 *
 * @author EM
 */
public class PartParameterData {

    private static PartParameterData istanza = null;
    public static final ArrayList<String> listaRicerca = new ArrayList<String>(10);
    public static PartParameterRecord[] records;

    public static PartParameterData getInstance() {
        if (istanza == null) {
            istanza = new PartParameterData();
        }
        return istanza;
    }

    private PartParameterData() {
    }

    public PartParameterRecord[] getRecords(ArrayList<String> listaRicerca) {

        return getNewRecordsTags(listaRicerca);

    }

    public PartParameterRecord[] getRecords(ArrayList<String> listaRicerca, ArrayList<String> listona) {

        return getNewRecordsValue(listaRicerca, listona);

    }

    public PartParameterRecord[] getNewRecordsTags(ArrayList<String> listaRicerca) {
        PartParameterRecord[] ritorno = new PartParameterRecord[listaRicerca.size()];

        int i = 0;

        for (String string : listaRicerca) {
            ritorno[i] = new PartParameterRecord(string);
            i++;
        }
        return ritorno;
    }

    public PartParameterRecord[] getNewRecordsValue(ArrayList<String> listaRicerca, ArrayList<String> listona) {
        int t = listaRicerca.size();

        if (t < listona.size()) {
            t = listona.size();
        }
        PartParameterRecord[] ritorno = new PartParameterRecord[t];
        int i = 0;
        for (String nome : listaRicerca) {
            for (String value : listona) {
                ritorno[i] = new PartParameterRecord(nome, value);
                i++;
            }
        }

        return ritorno;

    }

    public PartParameterRecord[] getRecordsObj(ArrayList<VObject> result) {

        return getNewRecordsTagsObj(result);
    }

    private PartParameterRecord[] getNewRecordsTagsObj(ArrayList<VObject> result) {
        PartParameterRecord[] ritorno = new PartParameterRecord[result.size()];

        int k = 0;
        for (VObject obj : result) {
            ritorno[k] = new PartParameterRecord(obj.getId());
            k++;
        }
        return ritorno;
    }
}
