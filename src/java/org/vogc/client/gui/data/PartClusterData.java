package org.vogc.client.gui.data;

import java.util.ArrayList;
import org.vogc.client.datatype.AdditionalInfoAttribute;

/**
 *
 * @author EM
 */
public class PartClusterData {

    private static PartClusterData istanza = null;
    public static final ArrayList<String> listaRicerca = new ArrayList<String>(10);
    public static PartClusterRecord[] records;

    public static PartClusterData getInstance() {
        if (istanza == null) {
            istanza = new PartClusterData();
        }
        return istanza;
    }

    private PartClusterData() {
    }

    public PartClusterRecord[] getRec() {
        return records;
    }

   

    public PartClusterRecord[] getRecords( ArrayList<AdditionalInfoAttribute> gcAtt) {
       
        return getNewRecordsTags(gcAtt);
    }

    private PartClusterRecord[] getNewRecordsTags( ArrayList<AdditionalInfoAttribute> gcAtt) {
        PartClusterRecord[] ritorno = new PartClusterRecord[gcAtt.size()];
        int k = 0;
        for (AdditionalInfoAttribute att : gcAtt) {
            ritorno[k] = new PartClusterRecord(att.getAttPrimaryInfo().getDescription(), att.getAttPrimaryInfo().getValue());
            k++;
        }
        return ritorno;
    }
}