package org.vogc.client.gui.data;

/**
 *
 * @author EM
 */
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class FormFieldSearch extends DataSource {

    private static FormFieldSearch instance = null;

    public static FormFieldSearch getInstance() {
        if (instance == null) {
            instance = new FormFieldSearch();
        }
        return instance;
    }

    private FormFieldSearch() {

        setID("animalDS");

        DataSourceTextField statusField = new DataSourceTextField("Parameter", "Parameter");
        statusField.setValueMap("Gal. Longitude", "Gal. Latitude", "R_sun", "R_gc", "X", "Y", "Z", "Reddening", "HV B mag", "Visual distance modulus", "[FE/H", "eliocetric vel");

        DataSourceTextField measure = new DataSourceTextField("Measure", "");

        measure.setValueMap("< less than ", "=  equals ", "> more than");

        DataSourceTextField parameterField = new DataSourceTextField("Category", "Category");
        parameterField.setValueMap("Photometric", " Positional", "Structural");

        setFields(statusField, measure, parameterField);

        setClientOnly(true);
    }
}


