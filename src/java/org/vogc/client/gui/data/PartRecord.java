package org.vogc.client.gui.data;

/**
 *
 * @author EM
 */
import com.smartgwt.client.widgets.grid.ListGridRecord;

public final class PartRecord extends ListGridRecord {

    public PartRecord(String partName) {
        setPartName(partName);
    }

    public PartRecord(String partName, String partValue) {
        setPartName(partName);
        setPartValue(partValue);
    }

   

    public void setPartName(String partName) {
        setAttribute("partName", partName);
    }

    public void setPartValue(String partValue) {
        setAttribute("partValue", partValue);
    }
}
