package org.vogc.client.gui.data;

import java.util.ArrayList;
import org.vogc.client.datatype.BiblioReference;

/**
 *
 * @author Ettore
 */
public class PartClusterDataBiblio {

    private static PartClusterDataBiblio istanza2 = null;
    public static PartClusterRecordBiblio[] recordsBiblio;

    public static PartClusterDataBiblio getInstance() {
        if (istanza2 == null) {
            istanza2 = new PartClusterDataBiblio();
        }
        return istanza2;
    }

    private PartClusterDataBiblio() {
    }

    public PartClusterRecordBiblio[] getRecords(ArrayList<BiblioReference> biblioAtt) {
       
        return getNewRecordsTags( biblioAtt);
    }

    private PartClusterRecordBiblio[] getNewRecordsTags( ArrayList<BiblioReference> biblioAtt) {
        PartClusterRecordBiblio[] ritorno2 = new PartClusterRecordBiblio[biblioAtt.size()];

        int k = 0;
        for (BiblioReference att : biblioAtt) {
            ritorno2[k] = new PartClusterRecordBiblio(att.getDescription(), att.getTitle());
            k++;
        }
        return ritorno2;
    }

    public PartClusterRecordBiblio[] emptyRec() {
        recordsBiblio = null;
        return recordsBiblio;
    }

    public PartClusterRecordBiblio[] getRec() {
        return recordsBiblio;
    }
}
