package org.vogc.client.gui;

/**
 *
 * @author Ettore
 */
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;

public class PartsUserListGrid extends ListGrid {

    public PartsUserListGrid() {
        setWidth100();
        setCellHeight(24);
        setImageSize(16);
        setShowEdges(true);
        setBorder("0px");
        setBodyStyleName("normal");
        setShowHeader(false);
        setLeaveScrollbarGap(false);
        setEmptyMessage("<br><br>Drag &amp; drop parts here");

//        ListGridField partSrcField = new ListGridField("partSrc", 24);
//        partSrcField.setType(ArrayListGridFieldType.IMAGE);
//        partSrcField.setImgDir("pieces/16/");

        ListGridField partUserField = new ListGridField("partUserName");
        ListGridField partNameField = new ListGridField("partNameUser");
        ListGridField partSurnameField = new ListGridField("partSurnameUser");
        // setFields(partNameField);
        setFields(partUserField, partNameField, partSurnameField);

        // setTrackerImage(new ImgProperties("pieces/24/cubes_all.png", 24, 24));
    }
}
