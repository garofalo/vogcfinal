package org.vogc.client.gui;

import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.ItemChangedEvent;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import org.vogc.client.gui.data.FormFieldSearch;

/**
 *
 * @author EM
 */
public class SearchAdvancedPanel extends VLayout {

    //final TabPanel p = new TabPanel();
    public SearchAdvancedPanel(final TabPanel p) {


        setBackgroundImage("background.jpg");

        //setWidth("95%"); // grandezza massima
        //setHeight100();

        VLayout principale = new VLayout();
        // principale.setHeight("5%");


        //VLayout Semplice = new VLayout();

        // HLayout Avanzato = new HLayout();
        // Avanzato.setBorder("2 px solid pink");


        // Complessa per categoria
        DynamicForm categoryForm = new DynamicForm();
        categoryForm.setIsGroup(true);
        categoryForm.setGroupTitle("Category Search");
        categoryForm.setWidth("45%");
        categoryForm.setDataSource(FormFieldSearch.getInstance());
        //categoryForm.setAutoFocus(false);

        //TextItem CategoryField = new TextItem("Value");//campo dove inserire il valore

        SelectItem statusItem2 = new SelectItem("Category");// tipi di categorie
        statusItem2.setTooltip("Select Category");
        statusItem2.setOperator(OperatorId.EQUALS);
        statusItem2.setAllowEmptyValue(true);

        ButtonItem buttonCategory = new ButtonItem();//bottone
        buttonCategory.setTitle("Search");
        buttonCategory.setRowSpan(0);
        buttonCategory.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                // grid.fetchData(form.getValuesAsCriteria());
                Tab tTab = new Tab("Category search");
                tTab.setCanClose(true);
                p.TabCategorySearch(tTab, p);
                SC.say("Sorry ! Feature available in next release");
            }
        });
        categoryForm.setFields(statusItem2, buttonCategory);

        categoryForm.addItemChangedHandler(new ItemChangedHandler() {
            @Override
            public void onItemChanged(ItemChangedEvent event) {
                //  tileGrid.fetchData(Categoryform.getValuesAsCriteria());
            }
        });


        // pulsanti per ricerca avanzata e per mostrare tutto il DB
        DynamicForm advancedForm = new DynamicForm();
        advancedForm.setWidth("50%");
        advancedForm.setIsGroup(true);
        advancedForm.setGroupTitle("Advanced Search");
        advancedForm.setNumCols(6);
        advancedForm.setDataSource(FormFieldSearch.getInstance());
        advancedForm.setAutoFocus(false);



        ButtonItem buttonAdvanced = new ButtonItem();//bottone avanzato

        buttonAdvanced.setTitle("Advanced");
        buttonAdvanced.setTooltip("click here for advanced search");
        buttonAdvanced.setRowSpan(0);
        buttonAdvanced.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String title = "Advanced";
                Tab tTab = new Tab(title, " hello ");
                tTab.setCanClose(true);
                p.TabAdvancedSearch(tTab, p);
                SC.say("Sorry ! Feature available in next release");





                // grid.fetchData(form.getValuesAsCriteria());
            }
        });

        ButtonItem buttonAllDatabase = new ButtonItem();// bottone per visualizzare tutto il database
        buttonAllDatabase.setTitle("Complete");
        buttonAllDatabase.setTooltip("click here show all Database");
        buttonAllDatabase.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                String title = "Complete";
                Tab tTab = new Tab(title, " hello ");
                tTab.setCanClose(true);
                p.TabCompleteSearch(tTab, p);
            }
        });
        advancedForm.setFields(buttonAdvanced, buttonAllDatabase);


//        IButton hideButton = new IButton();
//        hideButton.setID("SHOW");
//        hideButton.setTitle("Advanced");
//        hideButton.addClickHandler(new ClickHandler() {
//
//            public void onClick(ClickEvent event) {
//
//
//               String title = "Addvanced";
//               Tab tTab = new Tab(title, " hello ");
//               tTab.setCanClose(true);
//                p.addTabTEST(tTab);
//                //mylabel.addTab(mytab);
//                //addMember(mylabel);
//            }
//        });



        principale.addMember(categoryForm);
        principale.addMember(advancedForm);

        addMember(principale);

//        show();
    }
}
