package org.vogc.client.gui;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
//import org.dame.client.gui.RegistrationForm;
//import org.dame.client.gui.RegistrationWindow;

/*
 *
 *
 */
public class AboutPanel extends HLayout {

//    private DynamicForm form;
    private final Button logButton;
    private final Button regButton;
    private boolean first = true;
    String username;
    String password;

    public AboutPanel() {
        setBackgroundImage("background.jpg");
        /*
         * Fullscreen setting
         */
        setWidth100();
        setHeight100();
        setBackgroundColor("white");
        /*
         * Margine sinistro
         */
        VLayout leftLay = new VLayout();
        leftLay.setWidth("20%");
        leftLay.setHeight100();
        //aggiungo al layer principale
        addMember(leftLay);

        /*
         * Header
         */
        Img head = new Img("vogc_dame_logo.png");
        head.setAlign(Alignment.LEFT);
        head.setSize("503px", "154px");

        HLayout topLayout = new HLayout(5);
        topLayout.setWidth("20%");
        topLayout.setHeight("170px");
        topLayout.setAlign(Alignment.LEFT);
        topLayout.setAlign(VerticalAlignment.CENTER);
        topLayout.addMember(head);

        VLayout middleLay = new VLayout();
        middleLay.setWidth("800px");
        middleLay.setHeight100();
        middleLay.addMember(topLayout);




        /*
         * Aggiungere testo PRE registrazione
         */
        final String centerText =
                "<span style='font-size:large; font-weight: bold;'><p align=justify>"
                + "VOGCLUSTERS is a web application designed  "
                + "for the data and text mining on astronomical archives related to "
                + "globular cluster objects.</p></span>"
                + "<span style='font-size:medium; font-weight: bold;'><p align=justify>"
                + "<br />For news and documentation  "
                + "please visit <a href='http://dame.dsf.unina.it' target='_blank'>http://dame.dsf.unina.it</a> "
                               + "<br /></p>"
                + "<br /></p>"
                + "Copyright © 2011 VOGCLUSTERS - DAME PROGRAM, Naples, Italy"
                + "<br /></p>"
                + " All rights about the material of this application (including text, computer code, artwork, pictures, information) are reserved to DAME Board"
                + "<p align=center>"
                + "Technical Support<br />"
                + "<a href='mailto:vogclusters@gmail.com' target='_blank'>vogclusters AT gmail.com</a><br />"
                + "<br />Skype helpdesk<br />"
                + "<!--Skype 'Skype Me™!' button http://www.skype.com/go/skypebuttons-->"
                + "<script type='text/javascript' src='http://download.skype.com/share/skypebuttons/js/skypeCheck.js'></script> "
                + "<a href='skype:helpdame?call'>"
                + "<img src='http://download.skype.com/share/skypebuttons/buttons/call_blue_white_124x52.png' style='border: none;' width='124' height='52' alt='Skype Me™!' /></a>"
                + "</p></span>";

        HTMLFlow centerFlow = new HTMLFlow(centerText);
        centerFlow.setWidth("600px");
        centerFlow.setMargin(5);
        centerFlow.setAlign(Alignment.CENTER);

        VLayout sx = new VLayout();
        sx.addMember(centerFlow);
        HLayout sx_dx = new HLayout();
        sx_dx.setWidth("790px");
        sx_dx.addMember(sx);


        /*
         *
         */
//        String loginText =
//                "<span style='color:#C6CCD3; font-size:large; font-weight: bold;'>"
//                + "<b>Sign in</b></span>";
//        HTMLFlow loginFlow = new HTMLFlow(loginText);


        VLayout dx = new VLayout();
        dx.setAlign(VerticalAlignment.TOP);
        dx.setBackgroundColor("#708090");
        dx.setWidth("220px");
        //dx.addMember(loginFlow);

        /*
         *
         */
//        RegExpValidator mailRegExp = new RegExpValidator();
////    mailRegExp.setExpression("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
//        mailRegExp.setExpression("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)");


//        TextItem userName = new TextItem("usernamevalue", "<span style='color:#C6CCD3;'>User Mail</span>");
//        userName.setRequired(Boolean.TRUE);
//        // userName.setValidators(mailRegExp);

//    final PasswordItem password = new PasswordItem("Password", "<span style='color:#C6CCD3;'>Password</span>");
//    password.setRequired(Boolean.TRUE);

//        final PasswordItem Password = new PasswordItem("passwordvalue", "<span style='color:#C6CCD3;'>Password</span>");
//        Password.getAttributeAsString("passwordvalue");
//
//        form = new DynamicForm();
//
//        form.setFields(userName, Password);

        //  dx.addMember(form);

        /*
         *
         */
        logButton = new Button("<b>LogIn</b>");
        logButton.setID("AboutButton1");
        logButton.setWidth("140px");
        logButton.setHeight("40px");
        logButton.setMargin(5);

        logButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
//                if (form.validate(true)) {
//                    MainWindow f = new MainWindow();
//                    f.setID("MainWindow");
//                    hide();
//                    f.draw();
//                }
//        if (form.validate(true)) {
//
//                    username = form.getValueAsString("usernamevalue");
//                    password =form.getValueAsString("passwordvalue");
//
//                 //   getService().authenticateUser(username, callback);
//                  if (username.equals("guest") && password.equals("v0gclusters")){
////                    MainWindow f = new MainWindow();
////                    f.setID("MainWindow");
////                    hide();
////                    f.draw();
//                  }
//
//                  else{
//                       SC.warn("ERROR IN USER NAME");
//                  }
//                    // HomePanel h = new HomePanel();
//                    // hide();
//                    // h.draw();
//                }
//                else {
//
//                    SC.warn("ERROR IN USER NAME");
//                    //((IButton) layout.getMember("ok")).disable();
//                }
            }
        });

        // dx.addMember(logButton);

        /*
         *
         */
//        String regText = "<br />"
//                + "<span style='color:#C6CCD3; font-size:large;'>"
//                + "<b>New on VOGCLUSTERS?</b>"
//                + "</span>";
//        HTMLFlow regFlow = new HTMLFlow(regText);
        //  dx.addMember(regFlow);

        /*
         *
         */

        regButton = new Button("<b>Register Now</b>");
        regButton.setID("AboutButton2");
        regButton.setWidth("140px");
        regButton.setHeight("40px");
        regButton.setMargin(5);
        regButton.setAlign(Alignment.CENTER);
        regButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                if (isFirst()) {
                    final RegistrationWindow regWin = RegistrationWindow.getInstance();
                    regWin.addCloseClickHandler(new CloseClickHandler() {
                        public void onCloseClick(CloseClickEvent event) {
                            ((DynamicForm) RegistrationForm.getInstance().getMember("_RegistrationForm")).clearValues();
                            regWin.hide();
                        }
                    });

                    regWin.show();
                    setFirst();
                } else {
                    RegistrationWindow.getInstance().show();
                }

            }
        });

        //dx.addMember(regButton);

        /*
         *
         */
//        final String moreText =
//                "<span style='color:#C6CCD3; font-weight: bold;'><p>"
//                + "You can obtain the access by following "
//                + "a simple registration procedure:</p><ol>"
//                + "<li>Compile the registration form (click <i>Register Now</i> button);</li>"
//                + "<li>Immediately after you will receive by e-mail a welcome message;</li>"
//                + "<li>Check for an e-mail message with your account confirmation;</li>"
//                + "<li>Go back at this page and sign in;</li> </ol></span>";
//        HTMLFlow sxFlow = new HTMLFlow(moreText);
        //  dx.addMember(sxFlow);



        // sx_dx.addMember(dx);

        VLayout centerLayout = new VLayout();
        centerLayout.setAlign(Alignment.CENTER);
        centerLayout.setAlign(VerticalAlignment.CENTER);
        centerLayout.addMember(sx_dx);

        middleLay.addMember(centerLayout);

        /*
         *
         */
        Img footer = new Img("footer.gif");
        footer.setSize("776px", "74px");

        HLayout bottomLayout = new HLayout(5);
        bottomLayout.setWidth100();
        bottomLayout.setHeight("90px");
        bottomLayout.setAlign(Alignment.CENTER);
        bottomLayout.setAlign(VerticalAlignment.CENTER);
        bottomLayout.addMember(footer);

        middleLay.addMember(bottomLayout);

        addMember(middleLay);

        /*
         *
         */
        VLayout rightLay = new VLayout();
        rightLay.setWidth("10%");
        rightLay.setHeight100();

        addMember(rightLay);
    }
    //    final AsyncCallback<ErrorReport> callback = new AsyncCallback<ErrorReport>() {

    private void setFirst() {
        this.first = false;
    }

    private boolean isFirst() {
        return first;
    }
}