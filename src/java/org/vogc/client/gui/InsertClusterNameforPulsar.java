package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.GetVObjectService;
import org.vogc.client.GetVObjectServiceAsync;
import org.vogc.client.datatype.GClusterInfo;

/**
 *
 * @author Ettore
 */
public class InsertClusterNameforPulsar extends VLayout {
    // Listen for the button clicks

    public static GetVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(GetVObjectService.class);
    }
    public String s;

    public InsertClusterNameforPulsar() {

        final TextItem nameItem = new TextItem();

        nameItem.setName("CLUSTERNAME");
        nameItem.setTitle("Cluster");
        s = nameItem.getAttributeAsString("CLUSTERNAME");


        //nameItem.disable();


        final ButtonItem buttonItem = new ButtonItem();
        buttonItem.setName("proceed");
        buttonItem.setTitle("Proceed");



//        RadioGroupItem radioGroupItem = new RadioGroupItem("sharing");
//        radioGroupItem.setVertical(false);
//        radioGroupItem.setValueMap("Cluster exist", "cluster not exist");
//        radioGroupItem.setWidth(100);
//        radioGroupItem.addChangedHandler(new ChangedHandler() {
//
//            public void onChanged(ChangedEvent event) {
//                String val = ((String) event.getValue());
//
//
//                if (val == null ? "Cluster exist" == null : val.equals("Cluster exist")) {
//
//                    nameItem.show();
//                    buttonItem.setDisabled(false);
//
//                    //nameItem.enable();
//                } else {
//
//                    nameItem.hide();
//                    buttonItem.setDisabled(false);
//                }
//            }
//        });
        final AsyncCallback<GClusterInfo> callback = new AsyncCallback<GClusterInfo>() {
            public void onSuccess(GClusterInfo result) {
//                Iterator nrow = result.getGcAtt().iterator();
//                int row = 0, i = 0;
//                while (nrow.hasNext()) {
//                    AdditionalInfoAttribute att = (AdditionalInfoAttribute) nrow.next();
//
//
//
//
//                    row++;
//                    // rec [row] = att.getAttPrimaryInfo().getDescription();
//                }

                s = result.getId();
                InsertPulsar pro = new InsertPulsar(s);
                addMember(pro);




            }

            public void onFailure(Throwable caught) {
                SC.warn("CLUSTER NOT INTO DB PLEASE CONTINUE TO CREATE NEW");
                InsertClusterStandAlone cluster = new InsertClusterStandAlone();
                addMember(cluster);



            }
        };

        final DynamicForm controls = new DynamicForm();
        controls.setIsGroup(true);
        controls.setGroupTitle("enter the name of the reference CLUSTER ");
        controls.setNumCols(2);
        controls.setFields(nameItem, buttonItem);

        buttonItem.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {


                buttonItem.setDisabled(true);
                getService().getGcluster(controls.getValueAsString("CLUSTERNAME"), callback);



            }
        });




        addMember(controls);




    }
}
