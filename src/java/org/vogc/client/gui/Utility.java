package org.vogc.client.gui;

/**
 *
 * @author luca
 */
public class Utility {

    private static String userId;
    private static String password;
    private static String name;
    private static String surname;
    private static String motivation;
    private static String country;
    private static boolean active;
    /**
     * Constructs a user object with a specific userId, password, name, surname,
     * motivation and active flag
     *
     *
     */
    public static Utility instance = null;

    /**
     * Gets the value of the active flag
     *
     * @return - a boolean value: true if the user is active, false otherwise
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Gets the user's motivations
     *
     * @return - a string containing the user's motivation relative to the
     * abilitation request
     */
    public String getAffiliation() {
        return motivation;
    }

    /**
     * Gets the user's country
     *
     * @return - a string containing the user's country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Gets the user's name
     *
     * @return - a string containing the user's name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the user's password
     *
     * @return - a string contaning the user's password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets the user's surname
     *
     * @return - a string containing the user's surname
     */
    public String getSurname() {
        return surname;
    }

    public String getUserId() {
        return userId;
    }

    /*
     @param userId unique string that identify a user object
     */
    public void setUserId(String userId) {
        Utility.userId = userId;
    }

    /**
     * Sets the active flag
     *
     * @param active a boolean containing true if the user is active, false
     * otherwise
     */
    public void setActive(boolean active) {
        Utility.active = active;
    }

    /**
     * Sets the user's motivations
     *
     * @param motivation a string containing the user's motivation relative to
     * the abilitation request
     */
    public void setMotivation(String motivation) {
        Utility.motivation = motivation;
    }

    /**
     * Sets the user's country
     *
     * @param country a string containing the user's country
     */
    public void setCountry(String country) {
        Utility.country = country;
    }

    /**
     * Sets the user's name
     *
     * @param name a string containing the user's name
     */
    public void setName(String name) {
        Utility.name = name;
    }

    /**
     * Sets the user's password
     *
     * @param password a string containing the user's password
     */
    public void setPassword(String password) {
        Utility.password = password;
    }

    /**
     * Sets the user's surname
     *
     * @param surname a string containing the user's surname
     */
    public void setSurname(String surname) {
        Utility.surname = surname;
    }
}
