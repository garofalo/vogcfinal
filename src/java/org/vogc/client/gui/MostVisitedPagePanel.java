package org.vogc.client.gui;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author EM
 */
public class MostVisitedPagePanel extends VLayout {

    public MostVisitedPagePanel(int membersMargin) {
        super(membersMargin);
    }

    public MostVisitedPagePanel(JavaScriptObject jsObj) {
        super(jsObj);
    }

    public MostVisitedPagePanel() {


//        Label label = new Label();
//        label.setContents("<marquee direction='up' scrollAmount=1 height='100%' width='100%'>Most visited </marquee>");
//       // setBorder("2px solid pink");
//
//        addMember(label);

        show();
    }
}
