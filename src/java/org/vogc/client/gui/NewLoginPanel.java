package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.UserAccessService;
import org.vogc.client.UserAccessServiceAsync;
import org.vogc.client.datatype.ErrorReport;

//import org.dame.client.gui.RegistrationForm;
//import org.dame.client.gui.RegistrationWindow;

/*
 *
 *
 */
public class NewLoginPanel extends HLayout {

    public static UserAccessServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(UserAccessService.class);
    }
    private DynamicForm form;
    private final Button logButton;
    private final Button regButton;
    private boolean first = true;
    String username;
    String password;

    public NewLoginPanel() {

        /*
         * Fullscreen setting
         */
        setWidth100();
        setHeight100();
        setBackgroundColor("white");
        /*
         * Margine sinistro
         */
        VLayout leftLay = new VLayout();
        leftLay.setWidth("10%");
        leftLay.setHeight100();
        //aggiungo al layer principale
        addMember(leftLay);

        /*
         * Header
         */
        Img head = new Img("vogc_dame_logo.png");
        head.setAlign(Alignment.LEFT);
        head.setSize("503px", "154px");

        HLayout topLayout = new HLayout(5);
        topLayout.setWidth100();
        topLayout.setHeight("170px");
        topLayout.setAlign(Alignment.LEFT);
        topLayout.setAlign(VerticalAlignment.CENTER);
        topLayout.addMember(head);

        VLayout middleLay = new VLayout();
        middleLay.setWidth("800px");
        middleLay.setHeight100();
        middleLay.addMember(topLayout);




        /*
         * Aggiungere testo PRE registrazione
         */
        final String centerText =
                "<span style='font-size:large; font-weight: bold;'><p align=justify>"
                + "VOGCLUSTERS is a web application designed  "
                + "for the data and text mining on astronomical archives related to "
                + "globular cluster objects.</p></span>"
                + "<span style='font-size:medium; font-weight: bold;'><p align=justify>"
                + "<br />For news, documentation and FAQ information "
                + "please click <a href='http://dame.dsf.unina.it/vogclusters.html' target='_blank'>HERE</a>"
                + "<br /></p>"
                + "<p align=center>"
                + "Technical Support<br />"
                + "<a href='mailto:helpdame@gmail.com' target='_blank'>helpdame AT gmail.com</a><br />"
                + "<br />Skype helpdesk<br />"
                + "<!--Skype 'Skype Me™!' button http://www.skype.com/go/skypebuttons-->"
                + "<script type='text/javascript' src='http://download.skype.com/share/skypebuttons/js/skypeCheck.js'></script> "
                + "<a href='skype:helpdame?call'>"
                + "<img src='http://download.skype.com/share/skypebuttons/buttons/call_blue_white_124x52.png' style='border: none;' width='124' height='52' alt='Skype Me™!' /></a>"
                + "</p></span>"
                + "<br /></p>"
                + "<br /></p>"
                + "Copyright © 2015 VOGCLUSTERS - DAME PROGRAM, Naples, Italy"
                + "<br /></p>";

        HTMLFlow centerFlow = new HTMLFlow(centerText);
        centerFlow.setWidth("500px");
        centerFlow.setMargin(5);
        centerFlow.setAlign(Alignment.LEFT);

        VLayout sx = new VLayout();
        sx.addMember(centerFlow);
        HLayout sx_dx = new HLayout();
        sx_dx.setWidth("790px");
        sx_dx.addMember(sx);


        /*
         *
         */
        String loginText =
                "<span style='color:#C6CCD3; font-size:large; font-weight: bold;'>"
                + "<b>Sign in</b></span>";
        HTMLFlow loginFlow = new HTMLFlow(loginText);


        VLayout dx = new VLayout();
        dx.setAlign(VerticalAlignment.TOP);
        dx.setBackgroundColor("#708090");
        dx.setWidth("220px");
        dx.addMember(loginFlow);

        /*
         *
         */
        RegExpValidator mailRegExp = new RegExpValidator();
        mailRegExp.setExpression("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
        //mailRegExp.setExpression("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)");


        TextItem userName = new TextItem("usernamevalue", "<span style='color:#C6CCD3;'>User Mail</span>");
        userName.setRequired(Boolean.TRUE);
        // userName.setValidators(mailRegExp);
        // userName.setDefaultValue("ector1984@gmail.com");
//    final PasswordItem password = new PasswordItem("Password", "<span style='color:#C6CCD3;'>Password</span>");
//    password.setRequired(Boolean.TRUE);

        final PasswordItem Password = new PasswordItem("passwordvalue", "<span style='color:#C6CCD3;'>Password</span>");
        Password.getAttributeAsString("passwordvalue");
        Password.setRequired(Boolean.TRUE);
        // Password.setDefaultValue("1234567890");

        form = new DynamicForm();

        form.setFields(userName, Password);

        dx.addMember(form);

        final AsyncCallback<ErrorReport> callback = new AsyncCallback<ErrorReport>() {
            @Override
            public void onSuccess(ErrorReport result) {

                if (result.getCode().equals("vogcSRV0001") || result.getCode().equals("vogcSRV0002")) {
                    new MainWindow(form.getValueAsString("usernamevalue")).show();

                    //SC.say(" welcome " + username);
                } else if (result.getCode().equals("vogcSRV0006")) {
                    SC.warn("Error in password");
                } else if (result.getCode().equals("12345")) {
                    SC.warn("User not exist");
                } else if (result.getCode().equals("vogcSRV0003")) {
                    SC.warn("You aren't enable");
                } else {
                    SC.warn(result.getMessage());
                }
            }

            @Override
            public void onFailure(Throwable caught) {
                SC.warn("Communication failed");
            }
        };
        /*
         *
         */
        logButton = new Button("<b>LogIn</b>");
        logButton.setID("LogButton");
        logButton.setWidth("140px");
        logButton.setHeight("40px");
        logButton.setMargin(5);
        logButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
////              
                if (form.validate(true)) {

                    username = form.getValueAsString("usernamevalue");
                    username = username.replace(' ', '_');
                    password = form.getValueAsString("passwordvalue");
                    password = password.replace(' ', '_');
                    Utility a = new Utility();
                    a.setUserId(username);

                    getService().authenticateUser(username, password, callback);

                } else {

                    SC.warn("ERROR IN USER NAME OR PASSWORD ");
                }
            }
        });

        dx.addMember(logButton);

        /*
         *
         */
        String regText = "<br />"
                + "<span style='color:#C6CCD3; font-size:large;'>"
                + "<b>New on VOGCLUSTERS?</b>"
                + "</span>";
        HTMLFlow regFlow = new HTMLFlow(regText);
        dx.addMember(regFlow);

        /*
         *
         */

        regButton = new Button("<b>Register Now</b>");
        regButton.setID("RegButton");
        regButton.setWidth("140px");
        regButton.setHeight("40px");
        regButton.setMargin(5);
        regButton.setAlign(Alignment.CENTER);
        regButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                if (isFirst()) {
                    final RegistrationWindow regWin = RegistrationWindow.getInstance();
                    regWin.addCloseClickHandler(new CloseClickHandler() {
                        public void onCloseClick(CloseClickEvent event) {
                            ((DynamicForm) RegistrationForm.getInstance().getMember("_RegistrationForm")).clearValues();
                            regWin.hide();
                        }
                    });
                    regWin.show();
                    setFirst();
                } else {
                    RegistrationWindow.getInstance().show();
                }

            }
        });

        dx.addMember(regButton);

        /*
         *
         */
        final String moreText =
                "<span style='color:#C6CCD3; font-weight: bold;'><p>"
                + "You can obtain the access by following "
                + "a simple registration procedure:</p><ol>"
                + "<li>Compile the registration form (click <i>Register Now</i> button);</li>"
                + "<li>Immediately after you will receive by e-mail a welcome message;</li>"
                + "<li>Check for an e-mail message with your account confirmation;</li>"
                + "<li>Go back at this page and sign in;</li> </ol></span>";
        HTMLFlow sxFlow = new HTMLFlow(moreText);
        dx.addMember(sxFlow);



        sx_dx.addMember(dx);

        VLayout centerLayout = new VLayout();
        centerLayout.setAlign(Alignment.CENTER);
        centerLayout.setAlign(VerticalAlignment.CENTER);
        centerLayout.addMember(sx_dx);

        middleLay.addMember(centerLayout);

        /*
         *
         */
        Img footer = new Img("footer.gif");
        footer.setSize("776px", "74px");

        HLayout bottomLayout = new HLayout(5);
        bottomLayout.setWidth100();
        bottomLayout.setHeight("90px");
        bottomLayout.setAlign(Alignment.CENTER);
        bottomLayout.setAlign(VerticalAlignment.CENTER);
        bottomLayout.addMember(footer);

        middleLay.addMember(bottomLayout);

        addMember(middleLay);

        /*
         *
         */
        VLayout rightLay = new VLayout();
        rightLay.setWidth("10%");
        rightLay.setHeight100();

        addMember(rightLay);

    }

    private void setFirst() {
        this.first = false;
    }

    private boolean isFirst() {
        return first;
    }
}
