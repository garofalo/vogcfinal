package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import java.util.ArrayList;
import org.vogc.client.GetVObjectService;
import org.vogc.client.GetVObjectServiceAsync;
import org.vogc.client.datatype.GClusterInfo;
import org.vogc.client.gui.data.PartClusterData;

/**
 *
 * @author EM
 */
public class InfoClusterPanel extends HLayout {

    public static ArrayList<String> listaRicerca = new ArrayList<String>(10);

    public static GetVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(GetVObjectService.class);



    }
    public String s = null;
    public TabPanel Pan;

    public InfoClusterPanel(final TabPanel p, String s) {

        Pan = p;
        // toptabset.animateHide(AnimationEffect.SLIDE);
        final AsyncCallback<GClusterInfo> callback = new AsyncCallback<GClusterInfo>() {
            @Override
            public void onSuccess(final GClusterInfo result) {

                if (result.getReport() == null) {

                    Record[] data = PartClusterData.getInstance().getRecords(result.getGcAtt());


                    // PartClusterDataBiblio.getInstance().getRecords(0, result.getRefAboutCluster());
//                    Record[] data2 = PartClusterDataBiblio.getInstance().getRecords(result.getRefAboutCluster());

                    final PartsClusterListGrid myList1 = new PartsClusterListGrid();

                    myList1.setCanReorderRecords(true);
//                ListGridField nameField = new ListGridField("partClusterName", "NAME");
//                ListGridField valueField = new ListGridField("partClusterSrc", "VALUE");
//                myList1.setCanDragRecordsOut(true);
//                myList1.setCanReorderFields(true);
//                myList1.setDragDataAction(DragDataAction.MOVE);
                    myList1.setShowAllRecords(true);

                    //myList1.setFields(nameField,valueField);
                    myList1.setData(data);
                    myList1.setTooltip("double click for more info");


//                    final PartsClusterListGridBiblio myList2 = new PartsClusterListGridBiblio();
//                    myList2.setShowAllRecords(true);
//                    myList2.setCanResizeFields(true);
//                    //myList1.setFields(nameField,valueField);
//                    myList2.setData(data2);


                    Label labelNewObject = new Label();
                    labelNewObject.setHeight(10);
                    labelNewObject.setShowEdges(true);
                    labelNewObject.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>" + result.getId().toUpperCase() + "</FONT></p></font>");

                    IButton buttonGallery = new IButton("Image Gallery"); // creato bottone
                    // buttonGallery.setWidth("100%");
                    buttonGallery.setID("BUTTONGALLERY"); // id del bottone
                    buttonGallery.setTooltip("Show Gallery"); // tooltip del bottone
                    buttonGallery.addClickHandler(new ClickHandler() {
                        @Override
                        public void onClick(ClickEvent event) { // funzione click
                            Tab tTabgallery = new Tab(" Image Gallery of " + result.getId().toUpperCase()); // creazione della tab
                            tTabgallery.setCanClose(true); // la tab si pu chiudere
                            Pan.TabGallery(tTabgallery, Pan); // richiama la tab
                            SC.say("Sorry ! Feature available in next release");

                        }
                    });
                    
                    IButton buttonBiblio = new IButton("Biblio Notes"); // creato bottone
                    // buttonGallery.setWidth("100%");
                    buttonBiblio.setID("BUTTONBIBLIO"); // id del bottone
                    buttonBiblio.setTooltip("More Info"); // tooltip del bottone
                    buttonBiblio.addClickHandler(new ClickHandler() {
                        @Override
                        public void onClick(ClickEvent event) { // funzione click
                            Tab tTabMoreinfo = new Tab(" Biblio notes of " + result.getId().toUpperCase()); // creazione della tab
                            tTabMoreinfo.setCanClose(true); // la tab si pu chiudere
                            String name = result.getId();
                            Pan.TabMoreInfo(tTabMoreinfo, Pan, name); // richiama la tab


                        }
                    });

                    IButton buttonImage = new IButton("Add Image"); // creato bottone
                    // buttonGallery.setWidth("100%");
                    buttonImage.setID("BUTTONIMAGE"); // id del bottone
                    buttonImage.setTooltip("Add new image"); // tooltip del bottone
                    buttonImage.addClickHandler(new ClickHandler() {
                        @Override
                        public void onClick(ClickEvent event) { // funzione click
                            /*
                             * 
                             * COPIA LA TAB DI UPLOAD DI FE
                             * 
                             */
                            Tab tTabMoreinfo = new Tab(" More info of " + result.getId().toUpperCase()); // creazione della tab
                            tTabMoreinfo.setCanClose(true); // la tab si pu chiudere
                            String name = result.getId();
                            Pan.TabMoreInfo(tTabMoreinfo, Pan, name); // richiama la tab


                        }
                    });
                    VLayout vert = new VLayout();
                    vert.addMember(labelNewObject);
                    vert.addMember(buttonGallery);
                    vert.addMember(buttonBiblio);
                    myList1.addRecordClickHandler(new RecordClickHandler() {
                        @Override
                        public void onRecordClick(RecordClickEvent event) {
                            String attName = null;
                            String type = event.getRecord().getAttribute("partClusterName");
                            // String s = myList1.getDragData().toString();
                            // String s = event.getRecord();
                            String title = type;
                            Tab tTab = new Tab(title, " hello ");
                            tTab.setCanClose(true);
                            if (type.equalsIgnoreCase("Right Ascension")) {
                                attName = "ra";
                            }
                            if (type.equalsIgnoreCase("Declination")) {
                                attName = "dec";
                            }
                            if (type.equalsIgnoreCase("Galactic Longitude")) {
                                attName = "lii";
                            }
                            if (type.equalsIgnoreCase("Galactic Latitude")) {
                                attName = "bii";
                            }
                            if (type.equalsIgnoreCase("Foreground Reddening")) {
                                attName = "reddening";
                            }
                            if (type.equalsIgnoreCase("V Magnitude Level of the Horizontal Branch (or RR Lyraes)")) {
                                attName = "hb_vmag";
                            }
                            if (type.equalsIgnoreCase("Metallicity: Fe/H [Sun]")) {
                                attName = "metallicity";
                            }
                            if (type.equalsIgnoreCase("Half-mass relaxation time at present (in years)")) {
                                attName = "trh";
                            }
                            if (type.equalsIgnoreCase("Isotropic Velocity Distribution")) {
                                attName = "iso";
                            }
                            if (type.equalsIgnoreCase("Distance form the Sun [kpc]")) {
                                attName = "helio_distance";
                            }
                            if (type.equalsIgnoreCase("Distance from Galactic center [kpc]")) {
                                attName = "galcen_distance";
                            }
                            if (type.equalsIgnoreCase("Distance component X (toward gal center) [kpc]")) {
                                attName = "x_distance";
                            }
                            if (type.equalsIgnoreCase("Distance component Y (in direction of gal rotation) [kpc]")) {
                                attName = "y_distance";
                            }
                            if (type.equalsIgnoreCase("Distance comp. Z (toward north gal pole) [kpc]")) {
                                attName = "z_distance";
                            }
                            if (type.equalsIgnoreCase("Apparent visual distance modulus")) {
                                attName = "app_distance_mod";
                            }
                            if (type.equalsIgnoreCase("Integrated V magnitude of the cluster")) {
                                attName = "vmag";
                            }
                            if (type.equalsIgnoreCase("Absolute visual magnitude (cluster luminosity)")) {
                                attName = "abs_vmag";
                            }
                            if (type.equalsIgnoreCase("integrated color indices U-B (uncorrected for reddening)")) {
                                attName = "ub_color";
                            }
                            if (type.equalsIgnoreCase("integrated color indices B-V (uncorrected for reddening)")) {
                                attName = "bv_color";
                            }
                            if (type.equalsIgnoreCase("integrated color indices V-R (uncorrected for reddening)")) {
                                attName = "vr_color";
                            }
                            if (type.equalsIgnoreCase("integrated color indices V-I (uncorrected for reddening)")) {
                                attName = "vi_color";
                            }
                            if (type.equalsIgnoreCase("specific frequency of RR Lyrae variables")) {
                                attName = "rrlyr_frequency";
                            }
                            if (type.equalsIgnoreCase("horizontal-branch ratio")) {
                                attName = "hb_ratio";
                            }
                            if (type.equalsIgnoreCase("Dickens horizontal-branch morphological type")) {
                                attName = "hb_morph_type";
                            }
                            if (type.equalsIgnoreCase("integrated spectral type")) {
                                attName = "spect_type";
                            }
                            if (type.equalsIgnoreCase("heliocentric radial velocity [km/s]")) {
                                attName = "radial_velocity";
                            }
                            if (type.equalsIgnoreCase("observational (internal) uncertainty in radial velocity")) {
                                attName = "radial_velocity_error";
                            }
                            if (type.equalsIgnoreCase("radial velocity relative to solar local standard of rest")) {
                                attName = "lsr_radial_velocity";
                            }
                            if (type.equalsIgnoreCase("central concentration (c=log(r_t/r_c)")) {
                                attName = "central_concentration";
                            }
                            if (type.equalsIgnoreCase("ellipticity (projected axial ratio)")) {
                                attName = "ellipticity";
                            }
                            if (type.equalsIgnoreCase("core radius in arcmin")) {
                                attName = "core_radius";
                            }
                            if (type.equalsIgnoreCase("half-mass radius in arcmin")) {
                                attName = "halfmass_radius";
                            }
                            if (type.equalsIgnoreCase("core radius in arcmin")) {
                                attName = "core_radius";
                            }
                            if (type.equalsIgnoreCase("half-mass radius in arcmin")) {
                                attName = "halfmass_radius";
                            }
                            if (type.equalsIgnoreCase("core radius in arcmin")) {
                                attName = "core_radius";
                            }
                            if (type.equalsIgnoreCase("half-mass radius in arcmin")) {
                                attName = "halfmass_radius";
                            }
                            if (type.equalsIgnoreCase("tidal radius in arcmin")) {
                                attName = "tidal_radius";
                            }
                            if (type.equalsIgnoreCase("logarithm of core relaxation time [years]")) {
                                attName = "log_core_relaxtime";
                            }
                            if (type.equalsIgnoreCase("logarithm of relaxation time at the half-mass radius in years")) {
                                attName = "log_hmr_relaxtime";
                            }
                            if (type.equalsIgnoreCase("central surface brightness, V mag per square arcsec")) {
                                attName = "cent_surf_brightness";
                            }
                            if (type.equalsIgnoreCase("logarithm of central luminosity density (sol lum per cubic parsec)")) {
                                attName = "log_lum_density";
                            }


                            Pan.TabHystory(tTab, attName, result.getId().toUpperCase(), Pan);


                            //SC.warn("ciao" + s);
                        }
                    });
                    addMember(vert);
                    addMember(myList1);
                    // addMember(myList2);

                    //addMember(rightImg);
                    // addMember(leftImg);
                    //addMember(myList2);
                    // addMember(arrowImg);
//                show();
//                PartClusterData.getInstance().emptyRec();
//                PartClusterDataBiblio.getInstance().emptyRec();


                } else {
                    SC.warn("Search Result: ERROR");
                }
            }

            @Override
            public void onFailure(Throwable caught) {
                //  SC.warn("server side failure " + caught);
                SC.warn("OBJECT NOT FOUND");
                // SC.warn("OBJECT NOT FOUND" + caught.getMessage() + caught.getLocalizedMessage());
            }
        };
        getService().getGcluster(s, callback);



    }

    public void setS(String s) {
        this.s = s;
    }
}
