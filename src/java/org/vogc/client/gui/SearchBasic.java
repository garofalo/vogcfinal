package org.vogc.client.gui;

import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.ItemChangedEvent;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import java.util.ArrayList;
import org.vogc.client.gui.data.FormFieldSearch;

public class SearchBasic extends VLayout {

    public static final ArrayList<String> LISTARICERCA = new ArrayList<String>(5);
    int type;
    String typeParam;
    String ParamName;
    private final String obligatory = "obligatory";

    public SearchBasic(final TabPanel p) {
        setID("SEARCH");
        setBackgroundImage("background.jpg");
        setWidth(90);
        setHeight("10%"); // altezza di tutto il panello pannello
        /**
         * ************************NAME SEARCH*****************************
         */
        final DynamicForm ObjectNameSearchForm = new DynamicForm();
        ObjectNameSearchForm.setID("nameform");
        ObjectNameSearchForm.setIsGroup(true);
        ObjectNameSearchForm.setNumCols(8);
        ObjectNameSearchForm.setGroupTitle("Object Name");
        ObjectNameSearchForm.setWidth100();
        ObjectNameSearchForm.setHeight("100%");

        final TextItem nameField = new TextItem("val", "Value");// campo dove inserire il nome
        nameField.setRequired(true);
        nameField.setTooltip(obligatory);
        ButtonItem ButtonSearchbyName = new ButtonItem(); //pulsante

        ButtonSearchbyName.setTitle("Search");
        ButtonSearchbyName.setTooltip("Name object search");
        ButtonSearchbyName.setRowSpan(0);

        final SelectItem typesearch = new SelectItem();
        typesearch.setRequired(true);
        typesearch.setTitle("Type*");
        typesearch.setTooltip(obligatory);
        typesearch.setValueMap("Cluster", "Pulsar", "Star");
        typesearch.setDefaultValue("_______");
        typesearch.setWidth(80);

        typesearch.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent event) {
                String ds = (String) event.getValue();
                if (ds.equalsIgnoreCase("Cluster")) {
                    type = 1;
                } else if (ds.equalsIgnoreCase("Pulsar")) {
                    type = 2;
                } else if (ds.equalsIgnoreCase("Star")) {
                    type = 3;
                }
            }
        });

        ObjectNameSearchForm.setFields(nameField, typesearch, ButtonSearchbyName);

        ButtonSearchbyName.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ObjectNameSearchForm.validate();
//                ObjectNameSearchForm.saveData();
                // show tab for result

                String title = ObjectNameSearchForm.getValueAsString("val").replaceAll(" ", "");
                Tab tTab = new Tab(title, " hello ");
                tTab.setCanClose(true);
                p.TabNameSearch(tTab, title, type, p);

            }
        });

        /**
         * *********************PARAMETER SEARCH***************************
         */
        // form  per parametro
        final DynamicForm objectParameterForm = new DynamicForm();
        objectParameterForm.setIsGroup(true);
        objectParameterForm.setGroupTitle("Object Parameter");
        objectParameterForm.setNumCols(6);
        objectParameterForm.setDataSource(FormFieldSearch.getInstance());
        objectParameterForm.setAutoFocus(false);
        objectParameterForm.setWidth("100%");

        TextItem valueField = new TextItem("valp", "Value"); //campo dove inserire il parametro
        valueField.setRequired(true);
        valueField.setTooltip("parameter value");

        final SelectItem parameterItem = new SelectItem();
        parameterItem.setRequired(true);
        parameterItem.setTitle("Parameter *");
        parameterItem.setTooltip(obligatory);
        parameterItem.setValueMap("Gal longitude", "Gal latitude", "X", "Y", "Z", "ellipticity");
        parameterItem.setDefaultValue("_______");
        parameterItem.setWidth(100);

        parameterItem.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent event) {
                String ds3 = (String) event.getValue();
                if (ds3.equalsIgnoreCase("Gal longitude")) {
                    ParamName = "3";
                } else if (ds3.equalsIgnoreCase("Gal latitude")) {
                    ParamName = "4";
                } else if (ds3.equalsIgnoreCase("X")) {
                    ParamName = "12";
                } else if (ds3.equalsIgnoreCase("Y")) {
                    ParamName = "13";
                } else if (ds3.equalsIgnoreCase("Z")) {
                    ParamName = "14";
                } else if (ds3.equalsIgnoreCase("ellipticity")) {
                    ParamName = "30";
                }
            }
        });

        final SelectItem measureItem = new SelectItem();
        measureItem.setRequired(true);
        measureItem.setTitle("Operator *");
        measureItem.setTooltip(obligatory);
        measureItem.setValueMap("= equals", "< less than", "> more than");
        measureItem.setDefaultValue("_______");
        measureItem.setWidth(100);

        measureItem.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent event) {
                String ds2 = (String) event.getValue();
                if (ds2.equalsIgnoreCase("= equals")) {
                    typeParam = "uguale";
                } else if (ds2.equalsIgnoreCase("< less than")) {
                    typeParam = "minore";
                } else if (ds2.equalsIgnoreCase("> more than")) {
                    typeParam = "maggiore";

                }
            }
        });

        ButtonItem buttonParameter = new ButtonItem(); // bottone
        buttonParameter.setTitle("Search");
        buttonParameter.setTooltip("Search by parameter");
        buttonParameter.setRowSpan(0);
        objectParameterForm.setFields(parameterItem, measureItem, valueField, buttonParameter);
        buttonParameter.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                objectParameterForm.validate();
                objectParameterForm.saveData();
                // grid.fetchData(form.getValuesAsCriteria());

                Tab tTab2 = new Tab("Parameter search");
                tTab2.setCanClose(true);
                // p.TabParameterSearch(tTab2, p);
                p.TabParameterSearch(tTab2, p, ParamName, objectParameterForm.getValueAsString("valp").replaceAll(",", "."), typeParam);

            }
        });

        objectParameterForm.addItemChangedHandler(new ItemChangedHandler() {
            @Override
            public void onItemChanged(ItemChangedEvent event) {
                //  tileGrid.fetchData(Categoryform.getValuesAsCriteria());
            }
        });

        // Complessa per categoria
        DynamicForm objectCategoryForm = new DynamicForm();
        objectCategoryForm.setID("categoryFormID");
        objectCategoryForm.setIsGroup(true);
        objectCategoryForm.setGroupTitle("Object Category");
        objectCategoryForm.setDataSource(FormFieldSearch.getInstance());
        objectCategoryForm.setWidth(50);
        //categoryForm.setAutoFocus(false);

        //TextItem CategoryField = new TextItem("Value");//campo dove inserire il valore
        SelectItem categoryItem = new SelectItem("Category");// tipi di categorie
        categoryItem.setTooltip("Select Category");
        categoryItem.setOperator(OperatorId.EQUALS);
        categoryItem.setAllowEmptyValue(true);
        categoryItem.setWidth(90);

        ButtonItem buttonCategory = new ButtonItem();//bottone
        buttonCategory.setTitle("Search");
        buttonCategory.setRowSpan(0);
        buttonCategory.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                // grid.fetchData(form.getValuesAsCriteria());
                Tab tTab = new Tab("Category search");
                tTab.setCanClose(true);
                p.TabCategorySearch(tTab, p);
                SC.say("Sorry! Feature available in next release");
            }
        });
        objectCategoryForm.setFields(categoryItem, buttonCategory);

        objectCategoryForm.addItemChangedHandler(new ItemChangedHandler() {
            @Override
            public void onItemChanged(ItemChangedEvent event) {
                //  tileGrid.fetchData(Categoryform.getValuesAsCriteria());
            }
        });

        HLayout orizz = new HLayout();

        addMember(orizz);
        orizz.addMember(ObjectNameSearchForm);
        orizz.addMember(objectCategoryForm);

        addMember(objectParameterForm);
    }
}
