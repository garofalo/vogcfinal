package org.vogc.client.gui;

import com.smartgwt.client.types.Side;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

/**
 *
 * @author EM
 */
public class TabPanel extends VLayout {

    private TabSet toptabset = null;

    public TabPanel() {

        toptabset = new TabSet();
        toptabset.setID("TABSET");
        toptabset.setTabBarPosition(Side.TOP);
        toptabset.setTabBarAlign(Side.LEFT);


        setHeight100();
        setWidth100();
        addMember(toptabset);
//        show();
    }

    // welcome TAB
    void Start() {

        Tab tStart = new Tab("MAIN");
        WelcomeTABpanel startTab = new WelcomeTABpanel();
        tStart.setPane(startTab);
        toptabset.addTab(tStart);

    }

    // insert object TAB
    void TabInsertNewObject(Tab t1) {

        InsertObject NewObject = new InsertObject();
        // InsertNewObjectParameters NewObject = new InsertNewObjectParameters();
        t1.setPane(NewObject); // add class to tAB
        toptabset.addTab(t1); // add tab to main tab
        toptabset.selectTab(t1); // show tab
        //  toptabset.removeTab(t1);


    }

    // insert bibilio note tab
    void TabBiblio(Tab t2) {
        NewBiblioSelection biblio = new NewBiblioSelection();
        t2.setPane(biblio); // classe che continete i vari dati che si trovano nella main tab
        toptabset.addTab(t2);
        toptabset.selectTab(t2);
    }

    // insert new image
    void TabInsertNewImage(Tab t3) {

        NewImagePanel video = new NewImagePanel();
        t3.setPane(video);
        toptabset.addTab(t3);
        toptabset.selectTab(t3);
    }

    // create new plot
    void TabNewPlot(Tab t4) {


        toptabset.addTab(t4);
        toptabset.selectTab(t4);

    }

    // user list
    void TabUserList(Tab t5, TabPanel pane) {

        ShowUserListNEW user = new ShowUserListNEW(pane);
        t5.setPane(user);
        toptabset.addTab(t5);
        toptabset.selectTab(t5);

    }

    // show only the result from tag search
    void TabTagSearch(Tab t6, TabPanel p, String s) {
        //PanelSearchResultPROVVISORIO result = new PanelSearchResultPROVVISORIO(p);

        //ShowResultTags result = new ShowResultTags(p, s);
        ShowResultPartialSearch result = new ShowResultPartialSearch(s, p);

        t6.getCanClose();

        t6.setPane(result);
        toptabset.addTab(t6);
        toptabset.selectTab(t6);

//        toptabset.
    }

    // show the COMPLETE info of object after a name search
    void TabNameSearch(Tab t6, String s, int type, TabPanel p) {
        //PanelSearchResultPROVVISORIO result = new PanelSearchResultPROVVISORIO(p);
        //ShowResultTags result = new ShowResultTags(p,s);
        // pannelloINFOoggetto pan3 = new pannelloINFOoggetto(s);
        //pannelloINFOoggettoCluster pan3 =new pannelloINFOoggettoCluster(s);
        switch (type) {

            case 1: {
                InfoClusterPanel pan3 = new InfoClusterPanel(p, s);
                t6.setPane(pan3);
                toptabset.addTab(t6);
                toptabset.selectTab(t6);
                break;
            }
            case 2: {
                InfoPulsarPanel pan3 = new InfoPulsarPanel(p, s);
                t6.setPane(pan3);
                toptabset.addTab(t6);
                toptabset.selectTab(t6);
                break;
            }
            case 3: {
                InfoStarPanel pan3 = new InfoStarPanel(p, s);
                t6.setPane(pan3);
                toptabset.addTab(t6);
                toptabset.selectTab(t6);
                break;
            }
            default:
                SC.warn("Alert", "Wrong type to search");
        }


    }

    void TabParameterSearch(Tab t7, TabPanel p, String parameter, String Value, String operator) {
        // PanelSearchResultPROVVISORIO result = new PanelSearchResultPROVVISORIO(p);
        //PanelParameterResult result = new PanelParameterResult(parameter,Value,operator,p);
        ShowResultParameter result = new ShowResultParameter(parameter, Value, operator, p);
        t7.setPane(result);
        toptabset.addTab(t7);
        toptabset.selectTab(t7);
    }

    void TabCategorySearch(Tab t8, TabPanel p) {
        PanelSearchResultPROVVISORIO result = new PanelSearchResultPROVVISORIO(p);
        t8.setPane(result);
        toptabset.addTab(t8);
        toptabset.selectTab(t8);
    }

    void TabAdvancedSearch(Tab t9, TabPanel p) {
        // TabPanel p = new TabPanel();
        AdvancedSearchPan adv = new AdvancedSearchPan(p);
        t9.setPane(adv);
        toptabset.addTab(t9);
        toptabset.selectTab(t9);
    }

    // show the complete list of object
    void TabCompleteSearch(Tab t10, TabPanel p) {

        CompleteListObjectPanel comp = new CompleteListObjectPanel(p);
        t10.setPane(comp);
        toptabset.addTab(t10);
        toptabset.selectTab(t10);
    }

    void TabAbout(Tab t11) {
        //  AbilitaPanel abl = new AbilitaPanel();
        AboutPanel abl = new AboutPanel();
        t11.setPane(abl);
        toptabset.addTab(t11);
        toptabset.selectTab(t11);

    }

    void TabObject(Tab t12) {
        PannelloOggettoPROVVISORIO obj = new PannelloOggettoPROVVISORIO();
        // ObjectPanel obj = new ObjectPanel();
        t12.setPane(obj);
        toptabset.addTab(t12);
        toptabset.selectTab(t12);
    }

    // add new attibute
    void TabNewAttribute(Tab t13) {

        NewAttributePanel nAtt = new NewAttributePanel();
        t13.setPane(nAtt);
        toptabset.addTab(t13);
        toptabset.selectTab(t13);

    }

    void TabNote(Tab t14) {
        NewNote note = new NewNote();
        t14.setPane(note); // classe che continete i vari dati che si trovano nella main tab
        toptabset.addTab(t14);
        toptabset.selectTab(t14);
    }
// cancellazione

    void TabDeleteObj(Tab t15) {
        DeleteObject deleteobj = new DeleteObject();
        t15.setPane(deleteobj);
        toptabset.addTab(t15);
        toptabset.selectTab(t15);
    }

    void TabDeleteAtt(Tab t15bis) {
        DeleteAttribute deleteAtt = new DeleteAttribute();
        t15bis.setPane(deleteAtt);
        toptabset.addTab(t15bis);
        toptabset.selectTab(t15bis);
    }

    void TabUpdate(Tab t16) {
        UpdateObject update = new UpdateObject();
        t16.setPane(update);
        toptabset.addTab(t16);
        toptabset.selectTab(t16);
    }
    //  usato solo per fare prove

    void TabUserProfile(Tab t17, TabPanel p, String name) {
        //UpdateObject update = new UpdateObject();
        // t16.setPane(update);
        UserProfilePanel profile = new UserProfilePanel(name, p);

        t17.setPane(profile);
        toptabset.addTab(t17);
        toptabset.selectTab(t17);

    }

    void TabHystory(Tab t20, String type, String obj, TabPanel p) {
        //UpdateObject update = new UpdateObject();
        // t16.setPane(update);
        HistoryPanel history = new HistoryPanel(type, obj);
        t20.setPane(history);
        toptabset.addTab(t20);
        toptabset.selectTab(t20);

    }

    void TabGallery(Tab t21, TabPanel p) {
        //UpdateObject update = new UpdateObject();
        // t16.setPane(update);


        toptabset.addTab(t21);
        toptabset.selectTab(t21);

    }

    void TabMoreInfo(Tab t22, TabPanel p, String s) {
        //UpdateObject update = new UpdateObject();
        // t16.setPane(update);

        BiblioPanel bibpan = new BiblioPanel(s);
        t22.setPane(bibpan);
        toptabset.addTab(t22);
        toptabset.selectTab(t22);

    }

    void TabAuthor(Tab t23, TabPanel p) {
        //UpdateObject update = new UpdateObject();
        // t16.setPane(update);


        toptabset.addTab(t23);
        toptabset.selectTab(t23);

    }
}
