package org.vogc.client.gui;

/**
 *
 * @author EM
 */
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.gui.data.HeaderMenuRecord;

// pulsanti in alto a destra
public class HeaderLink extends HLayout {

    public HeaderLink() {
        int lenght = HeaderMenuRecord.lenght();
        VLayout[] menuList = new VLayout[lenght];

        for (int i = 0; i < lenght; i++) {

            menuList[i] = new MenuForm(i);
            addMember(menuList[i]);
        }

    }
}
