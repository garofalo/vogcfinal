package org.vogc.client.gui;

/**
 *
 * @author EM
 */
import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author EM
 */
public class WelcomeFacebook extends VLayout {

    public WelcomeFacebook(int membersMargin) {
        super(membersMargin);
    }

    public WelcomeFacebook(JavaScriptObject jsObj) {
        super(jsObj);
    }

    public WelcomeFacebook() {


        Label label = new Label();
        label.setContents("<marquee direction='up' scrollAmount=1 height='100%' width='100%'>RecentPreprints </marquee>");
        //   setBorder("2px solid pink");
        HTMLPane htmlPane = new HTMLPane();

        htmlPane.setShowEdges(true);
        htmlPane.setStyleName("exampleTextBlock");
        String contents = "<table><iframe src='http://www.facebook.com/plugins/likebox.php?href"
                + "=http%3A%2F%2Fwww.facebook.com%2Fpages%2Fgclusters%2F123871424321591&amp;width"
                + "=370&amp;colorscheme=light&amp;show_faces=true&amp;stream=true&amp;header=false"
                + "&amp;height=556' scrolling='no' frameborder='0' style='border:none; overflow:hidden; "
                + "width:370px; height:556px;' allowTransparency='true'></iframe></table>";
        htmlPane.setContents(contents);
        addMember(htmlPane);

//        show();
    }
}