package org.vogc.client.gui;

import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author Ettore
 */
public class NewBiblioSelection extends VLayout {

    NewBiblio biblio = new NewBiblio();
    NewAuthorPanel author = new NewAuthorPanel();
    ShowAuthors auth = new ShowAuthors();

    public NewBiblioSelection() {
        setBackgroundImage("background.jpg");

        Label label = new Label();
        label.setHeight(5);
        label.setShowEdges(true);
        label.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>New Biblio Note</FONT></p></font>");

        IButton newAuthors = new IButton("New Authors");
        newAuthors.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                addMember(author);
                author.draw();

            }
        });

        IButton newBiblio = new IButton("New Biblio notes");
        newBiblio.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                final HLayout orizz2 = new HLayout();
                addMember(orizz2);
                orizz2.addMember(biblio);
                orizz2.addMember(auth);
//                biblio.draw();
//                auth.draw();

            }
        });




        addMember(label);
        HLayout orizz = new HLayout();
        addMember(orizz);
        orizz.addMember(newAuthors);
        orizz.addMember(newBiblio);




    }
}
