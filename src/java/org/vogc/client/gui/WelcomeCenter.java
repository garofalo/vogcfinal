package org.vogc.client.gui;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.events.IconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.IconClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author EM
 */
public class WelcomeCenter extends VLayout {

//    public welcomeCenter(int membersMargin) {
//        super(membersMargin);
//    }
//
//    public welcomeCenter(JavaScriptObject jsObj) {
//        super(jsObj);
//    }
    private static final String HELPTEXT = "<br><b>WELCOME TO THE VOGCLUSTERS QUICK GUIDE</b> <br>  "
            + "<br><b>Main options are:</b> <br>  "
            + "<br><b>OBJECT NAME :</b>Insert the object name into the value field, select the object type and then press the search button.<br>  "
            + "."
            + "<br><br><b>PARAMETER NAME :</b>Insert the parameter name, select the operator, then insert the value and click on the search button.<br> "
            + "."
            + "<br><br><b>SHOW ALL :</b>Click to have an overview about all current objects in the database.<br> "
            + "."
            + "<br><br><b>UPDATE OBJECT :</b>After clicking it, it will appear a new tab, where you can select object type, name, value and attribute.<br> "
            + ".";

    public WelcomeCenter() {




        Label title = new Label();
        title.setWidth("100%");
        title.setHeight(25);
        title.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'><b>----------------Welcome ----------------</FONT></p></font></b>");
        // setBorder("2px solid purple");

        Utility a = new Utility();
        String userId = a.getUserId();

        Label title2 = new Label();
        title2.setWidth("100%");
        title2.setHeight(25);
        title2.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'><b>" + userId + "</FONT></p></font></b>");
        // setBorder("2px solid purple");
        setBackgroundImage("sfondo.jpg");


        final DynamicForm form = new DynamicForm();
        form.setWidth(400);

        FormItemIcon icon = new FormItemIcon();
        icon.setSrc("help.png");

        final StaticTextItem quickGuide = new StaticTextItem();
        quickGuide.setAlign(Alignment.CENTER);
        quickGuide.setName("_");
        quickGuide.setDefaultValue("<font color='#708090'><p align='center'><FONT SIZE='3'><b>           QUICK GUIDE </FONT></p></font></b>");
        quickGuide.setIcons(icon);
        quickGuide.addIconClickHandler(new IconClickHandler() {
            public void onIconClick(IconClickEvent event) {
                SC.say("Quick Guide", HELPTEXT);
            }
        });

        form.setFields(quickGuide);




        addMember(title);
        addMember(title2);
        addMember(form);

//        show();
    }
}