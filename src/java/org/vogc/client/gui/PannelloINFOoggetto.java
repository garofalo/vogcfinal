package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
//import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
//import com.google.gwt.user.client.ui.TextBox;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import java.util.Iterator;
import org.vogc.client.GetVObjectService;
import org.vogc.client.GetVObjectServiceAsync;
import org.vogc.client.datatype.AdditionalInfoAttribute;
import org.vogc.client.datatype.GClusterInfo;

/**
 *
 * @author EM
 */
class PannelloINFOoggetto extends VLayout {

    // Listen for the button clicks
    public static GetVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(GetVObjectService.class);
    }
    public String s = null;
    public TabPanel Pan;
//    private Label lblServerReply = new Label();
//    private TextBox txtUserInput = new TextBox();
//    private Button btnSend = new Button("SHOW CLUSTER");
    public Record[] records;
    private Grid myGrid = new Grid(100, 1);
    private Grid myGrid2 = new Grid(100, 1);
    ArrayList<String> lista = new ArrayList<String>(10);

    PannelloINFOoggetto(final String s) {

        final AsyncCallback<GClusterInfo> callback = new AsyncCallback<GClusterInfo>() {
            @Override
            public void onSuccess(GClusterInfo result) {
                Iterator nrow = result.getGcAtt().iterator();
                int row = 0, i = 0;
                while (nrow.hasNext()) {
                    AdditionalInfoAttribute att = (AdditionalInfoAttribute) nrow.next();


                    myGrid.setText(row, 0,  att.getAttPrimaryInfo().getDescription());
                    myGrid2.setText(row, 0,  att.getAttPrimaryInfo().getValue());
                    // lista.add(att.getAttPrimaryInfo().getDescription());

                    row++;
                    // rec [row] = att.getAttPrimaryInfo().getDescription();
                    lista.add(att.getAttPrimaryInfo().getDescription().toString());

                }
                //lblServerReply.setText("OK " + result.getName() + result.getType());
                Label label = new Label();
                label.setHeight(50);
                label.setPadding(10);
                label.setAlign(Alignment.CENTER);
                label.setValign(VerticalAlignment.CENTER);

                label.setShowEdges(true);
                label.setContents("OBJECT NAME = " + s);
                myGrid.setWidth("100%");
                myGrid.setBorderWidth(1);
                myGrid.resizeRows(row);
                myGrid2.setWidth("50%");
                myGrid2.setBorderWidth(1);
                myGrid2.resizeRows(row);
                addMember(label);
                HLayout orizz = new HLayout();
                addMember(orizz);
                orizz.addMember(myGrid);
                orizz.addMember(myGrid2);
//                show();

            }

            @Override
            public void onFailure(Throwable caught) {
                SC.warn("No information found for this object");
            }
        };
        getService().getGcluster(s, callback);






    }
}
