package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tab.Tab;
import java.util.ArrayList;
import org.vogc.client.GetVObjectService;
import org.vogc.client.GetVObjectServiceAsync;
import org.vogc.client.datatype.VObject;
import org.vogc.client.gui.data.PartParameterData;

/**
 *
 * @author EM
 */
public class ShowResultParameter extends HLayout {

    public static ArrayList<String> listaRicerca = new ArrayList<String>(10);

    public static GetVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(GetVObjectService.class);

    }
    public String s = null;
    public TabPanel Pan;
    final AsyncCallback<ArrayList<VObject>> callback2 = new AsyncCallback<ArrayList<VObject>>() {
        public void onSuccess(ArrayList<VObject> result) {
            int row = result.size();
            if (result.get(row - 1).getReport() == null) {


                Record[] data = PartParameterData.getInstance().getRecordsObj(result);


                final PartsListGrid myList1 = new PartsListGrid();

                myList1.setCanDragRecordsOut(true);
                myList1.setCanReorderFields(true);
                myList1.setDragDataAction(DragDataAction.MOVE);
                // myList1.setFields("name");
                myList1.setData(data);


                final PartsListGrid myList2 = new PartsListGrid();
                myList2.setCanDragRecordsOut(true);
                myList2.setCanAcceptDroppedRecords(true);
                myList2.setCanReorderRecords(true);

                myList1.addRecordClickHandler(new RecordClickHandler() {
                    public void onRecordClick(RecordClickEvent event) {

                        String s = event.getRecord().getAttribute("partName");
                        // String s = myList1.getDragData().toString();
                        // String s = event.getRecord();
                        String title = s;
                        Tab tTab = new Tab(title, " hello ");
                        tTab.setCanClose(true);

                        Pan.TabNameSearch(tTab, s, 1, Pan);



                    }
                });


            } else {
                SC.warn("Search Result: ERROR");
            }
        }

        public void onFailure(Throwable caught) {
            //  SC.warn("server side failure " + caught);
            SC.warn("OBJECT NOT FOUND");
            // SC.warn("OBJECT NOT FOUND" + caught.getMessage() + caught.getLocalizedMessage());
        }
    };

    public ShowResultParameter(String parameter, String Value, String operator, final TabPanel p) {

        Pan = p;
        // toptabset.animateHide(AnimationEffect.SLIDE);
        setBackgroundImage("background.jpg");
        getService().getGclusterByParameters(parameter, Value, operator, callback2);



    }

    public void setS(String s) {
        this.s = s;
    }
}
