package org.vogc.client.gui;

import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author EM
 */
public class WelcomeTABpanel extends VLayout {

    public WelcomeTABpanel() {
        setBackgroundImage("background.jpg");
        VLayout VerticalLayoutTab = new VLayout();

        HLayout OrizzontalLayoutTab = new HLayout();
        //Label Title = new Label();
        Label title = new Label();
        title.setWidth("100%");
        title.setHeight(25);
        title.setContents("<font color='#708090'><p align='center'><FONT SIZE='3'><b>----------------Welcome ----------------</FONT></p></font></b>");
        // setBorder("2px solid purple");


        Img tImg1 = new Img("sidebar.jpg");
        tImg1.setWidth("100%");
        tImg1.setHeight(48);
        WelcomeLink link = new WelcomeLink();// feed rss

        // MostVisitedPagePanel mostVisited = new MostVisitedPagePanel();

        WelcomeCenter center = new WelcomeCenter(); //pannello centrale

        WelcomeFacebook facebook = new WelcomeFacebook();// classe per facebook

        //   VerticalLayoutTab.addMember(title); //titolo
        OrizzontalLayoutTab.addMember(facebook); //riquadro notizie
        //OrizzontalLayoutTab.addMember(mostVisited);
        VerticalLayoutTab.addMember(tImg1); // separatore ( si può eliminare)
        OrizzontalLayoutTab.addMember(center);//riquadro links
        OrizzontalLayoutTab.addMember(link);// riquadro preprints
        VerticalLayoutTab.addMember(OrizzontalLayoutTab);
        addMember(VerticalLayoutTab);
       
    }
}
