package org.vogc.client.gui;

import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.TopOperatorAppearance;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.FilterBuilder;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import org.vogc.client.gui.data.FormFieldAdvancedSearch;
/* @author Ettore
 */

// ricerca avanzata
public class AdvancedSearchPan extends VLayout {

    public AdvancedSearchPan(final TabPanel p) {
        setBackgroundImage("background.jpg");
        //final TabPanel tab = null;
        DataSource worldDS = FormFieldAdvancedSearch.getInstance();

        final FilterBuilder matchfilter = new FilterBuilder();
        matchfilter.setDataSource(worldDS);
        matchfilter.setTopOperatorAppearance(TopOperatorAppearance.RADIO);

        final ListGrid firstGrid = new ListGrid();
        firstGrid.setWidth("50%");
        firstGrid.setHeight("50%");
        firstGrid.setDataSource(worldDS);
        firstGrid.setAutoFetchData(true);

        ListGridField nameField = new ListGridField("cobjName", "Name");
        ListGridField continentField = new ListGridField("long", "Gal. Longitude");
        ListGridField memberG8Field = new ListGridField("glat", "Gal. Latitude");
        memberG8Field.setCanEdit(false);

        ListGridField populationField = new ListGridField("R_sun", "R_sun");
        populationField.setType(ListGridFieldType.INTEGER);
        populationField.setCellFormatter(new CellFormatter() {
            @Override
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
                String ret = null;
                if (value != null) {

                    try {
                        NumberFormat nf = NumberFormat.getFormat("0,000");
                        ret = nf.format(((Number) value).longValue());
                    } catch (Exception e) {
                        ret = value.toString();
                    }
                }
                return ret;
            }
        });
        ListGridField independenceField = new ListGridField("independence", "R_gc");
        firstGrid.setFields(nameField, continentField, memberG8Field, populationField, independenceField);

        IButton filterButton = new IButton("Filter");
        filterButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                firstGrid.filterData(matchfilter.getCriteria());

            }
        });

        firstGrid.addSelectionChangedHandler(new SelectionChangedHandler() {
            @Override
            public void onSelectionChanged(SelectionEvent event) {
                //selectedCountriesGrid.setData(countryGrid.getSelection());
                String title = "COMING SOON ";
                Tab tTab2 = new Tab(title, " hello ");
                tTab2.setCanClose(true);
                p.TabObject(tTab2);
            }
        });

        IButton goButton = new IButton("GO");
        goButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                String title = "COMING SOON";
                Tab tTab2 = new Tab(title, " hello ");
                tTab2.setCanClose(true);
                p.TabObject(tTab2);

            }
        });


        addMember(matchfilter);
        addMember(filterButton);
        addMember(firstGrid);
        addMember(goButton);

    }
}
