package org.vogc.client.gui;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SubmitItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.datatype.Author;

/**
 *
 * @author Luca
 */
public class NewImagePanel extends VLayout {

    private DynamicForm descriptionForm = new DynamicForm();
    private SubmitItem submitButton = new SubmitItem();
    private Label labelNewObject = new Label();

    public NewImagePanel() {
        setBackgroundImage("background.jpg");
        final DynamicForm newImage = new DynamicForm();
        labelNewObject.setHeight(10);
        labelNewObject.setShowEdges(true);
        labelNewObject.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>Insert New Image</FONT></p></font>");
        addMember(labelNewObject);
//        final TextItem idAuthors = new TextItem("idA", "id Authors");


        final TextItem objectName = new TextItem();
        objectName.setName("objimg");
        objectName.setTitle("Object Name");
        objectName.setRequired(true);


        final TextItem description = new TextItem();
        description.setName("description");
        description.setTitle("Just a little descriprion");
        description.setRequired(true);

        final TextItem scale = new TextItem();
        scale.setName("scale");
        scale.setTitle("The scale of the image");
        scale.setRequired(true);

        final TextItem dimension = new TextItem();
        dimension.setName("diomension");
        dimension.setTitle("Dimension of the image");
        dimension.setRequired(true);


        submitButton = new SubmitItem();
        submitButton.setName("send");
        submitButton.setTitle("Upload");
        submitButton.setAlign(Alignment.CENTER);
        submitButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                if (event.getForm().validate(false)) {

                    //

                    submitButton.disable();
                }
            }
        });

        descriptionForm.setFields(description, submitButton);

        final VLayout uriLayer = new VLayout();
        uriLayer.setMargin(20);
        uriLayer.addMember(descriptionForm);
        uriLayer.hide();


        VerticalPanel panel = new VerticalPanel();

        // Create a FileUpload widget.
        final FileUpload upload = new FileUpload();
        upload.setName("uploadFormElement");

        panel.add(upload);

        Button subButton = new Button("Upload File", new com.google.gwt.event.dom.client.ClickHandler() {
            @Override
            public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
                if (!upload.getFilename().isEmpty()) {
                    //                    String uri = objectName.getDataPath();
                    Author a1 = new Author();
                    //Author a2 = new Author();
                    a1.setId(Integer.parseInt(newImage.getValueAsString("idA")));


                }
            }
        });
        panel.add(subButton);

        // Add an event handler to the form.


        final VLayout hdLayer = new VLayout();

        hdLayer.addMember(panel);

        hdLayer.hide();

        SelectItem selectSource = new SelectItem();
        selectSource.setName("Datasource");
        selectSource.setTitle("Upload from:");
        String[] map = new String[2];
        map[0] = "URI";
        map[1] = "Hard Disk";
        selectSource.setValueMap(map);
        descriptionForm = new DynamicForm();



        selectSource.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent event) {
                String ds = (String) event.getValue();

                if (ds.equalsIgnoreCase("URI")) {

                    hdLayer.hide();
                    uriLayer.show();

                } else {
                    uriLayer.hide();
                    hdLayer.show();

                }

            }
        });

        final DynamicForm sourceForm = new DynamicForm();
        sourceForm.setValue("Datasource", "Source");
        sourceForm.setItems(selectSource, objectName, description, scale, dimension);


        addMember(sourceForm);

        HLayout down = new HLayout();

        down.addMember(uriLayer);
        down.addMember(hdLayer);
        addMember(down);
    }
}
