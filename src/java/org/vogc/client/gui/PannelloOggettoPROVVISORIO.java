package org.vogc.client.gui;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.gui.data.TempSearchObject;
import org.vogc.client.gui.data.ValoriOggetto;

/**
 *
 * @author EM
 */
public class PannelloOggettoPROVVISORIO extends VLayout {

    public PannelloOggettoPROVVISORIO() {



        Label label = new Label();
        label.setHeight(10);
        label.setWidth100();
        label.setContents("PANEL PROVVISORIO");
        // addMember(label);

        final DataSource dataSource = ValoriOggetto.getInstance();

        final DynamicForm form = new DynamicForm();
        form.setIsGroup(true);
        form.setGroupTitle("Update");
        form.setNumCols(4);
        form.setDataSource(dataSource);



        final ListGrid listGrid = new ListGrid();
        listGrid.setWidth("50%");
        listGrid.setHeight(100);
        listGrid.setDataSource(dataSource);
        listGrid.setAutoFetchData(true);
        TempSearchObject tmp = new TempSearchObject();   // creazione del record di appoggio

        listGrid.setData(tmp.getRec());

        listGrid.addRecordClickHandler(new RecordClickHandler() {
            @Override
            public void onRecordClick(RecordClickEvent event) {
                form.reset();
                form.editSelectedData(listGrid);
            }
        });

        addMember(listGrid);
        // addMember(form);

        IButton button = new IButton("Save");
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                form.saveData();
            }
        });
        addMember(button);
//
//        show();
    }
}
