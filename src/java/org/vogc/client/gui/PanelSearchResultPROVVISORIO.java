package org.vogc.client.gui;

import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

/**
 *
 * @author EM
 */

// pannnello provvisorio di PROVA per la visualizzazione dei risultati
public class PanelSearchResultPROVVISORIO extends VLayout {

   // ListGrid resultGrid;

    public PanelSearchResultPROVVISORIO(final TabPanel p) {

        ListGrid resultGrid = new ListGrid();
        
        resultGrid.setWidth100();
        resultGrid.setHeight(100);
//        resultGrid.setAutoFetchData(true);
        resultGrid.setShowAllRecords(true);
        resultGrid.setWrapCells(true);
        resultGrid.setFixedRecordHeights(false);

        final ListGridField nameField = new ListGridField("NAME");
        resultGrid.setFields(new ListGridField[]{nameField});
        

        IButton goButton = new IButton("GO");
        goButton.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
                String title = "COMING SOON";
                Tab tTab2 = new Tab(title, " hello ");
                tTab2.setCanClose(true);
                p.TabObject(tTab2);

            }
        });
        addMember(resultGrid);
        addMember(goButton);
    }
}



