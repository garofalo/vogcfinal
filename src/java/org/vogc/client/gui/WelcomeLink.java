package org.vogc.client.gui;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author EM
 */
public class WelcomeLink extends VLayout {

    public WelcomeLink(int membersMargin) {
        super(membersMargin);
    }

    public WelcomeLink(JavaScriptObject jsObj) {
        super(jsObj);
    }

    public WelcomeLink() {


        Label label = new Label();
        label.setContents("<marquee direction='up' scrollAmount=1 height='100%' width='100%'>RecentPreprints </marquee>");
        //   setBorder("2px solid pink");
        HTMLPane htmlPane = new HTMLPane();

        htmlPane.setShowEdges(true);
        htmlPane.setStyleName("exampleTextBlock");
//         String contents =  "<br /><fieldset><legend>Incoming Events</legend><br />[+] <a href='http://dame.dsf.unina.it/astroinformatics2011.html' target='_blank'>Astroinformatics 2011<br /> September 25-29, 2011, Sorrento Italy</a><p></p>"
//                 + "[+] <a href='http://voi.iucaa.ernet.in:9090/InterOp_2011/framework/mainTemplate.jsp' target='_blank'>IVOA Interop Meeting<br /> October 17-21, 2011, Pune, India</a><p></p>[+] <a href='http://www.eso.org/sci/meetings/2011/adass2011.html' target='_blank'>ADASS XXI Conference<br /> November 06-10, 2011, Paris, France</a><p></p>[+] <a href='http://www.rightscale.com/conference/index.php?mkt_tok=3RkMMJWWfF9wsRoksqrMZKXonjHpfsX57%2B8rXLHr08Yy0EZ5VunJEUWy2oUFTdQhcOuuEwcWGog80gVfEvSGf4FM%2Fw%3D%3D' target='_blank'>Real Cloud Experience Conference<br /> November 08-09, 2011, Santa Clara, CA USA</a><p></p>[+] <a href='http://agenda.infn.it/conferenceDisplay.py?confId=3325' target='_blank'>2nd ICFDT Conference<br /> November 28-30, 2011, National Laboratories, Frascati Italy</a></fieldset>";
//         htmlPane.setContents(contents);

        String contents = "<iframe width='100%' height='100%' style='border:none;' src='http://output13.rssinclude.com/output?type=iframe&amp;id=342379&amp;hash=9d5f640b8d96fd06f24d7c1f1d61c87e'></iframe>";
        htmlPane.setContents(contents);

        HTMLPane htmlPane2 = new HTMLPane();

        htmlPane2.setShowEdges(true);
        htmlPane2.setStyleName("exampleTextBlock");
//         String contents =  "<br /><fieldset><legend>Incoming Events</legend><br />[+] <a href='http://dame.dsf.unina.it/astroinformatics2011.html' target='_blank'>Astroinformatics 2011<br /> September 25-29, 2011, Sorrento Italy</a><p></p>"
//                 + "[+] <a href='http://voi.iucaa.ernet.in:9090/InterOp_2011/framework/mainTemplate.jsp' target='_blank'>IVOA Interop Meeting<br /> October 17-21, 2011, Pune, India</a><p></p>[+] <a href='http://www.eso.org/sci/meetings/2011/adass2011.html' target='_blank'>ADASS XXI Conference<br /> November 06-10, 2011, Paris, France</a><p></p>[+] <a href='http://www.rightscale.com/conference/index.php?mkt_tok=3RkMMJWWfF9wsRoksqrMZKXonjHpfsX57%2B8rXLHr08Yy0EZ5VunJEUWy2oUFTdQhcOuuEwcWGog80gVfEvSGf4FM%2Fw%3D%3D' target='_blank'>Real Cloud Experience Conference<br /> November 08-09, 2011, Santa Clara, CA USA</a><p></p>[+] <a href='http://agenda.infn.it/conferenceDisplay.py?confId=3325' target='_blank'>2nd ICFDT Conference<br /> November 28-30, 2011, National Laboratories, Frascati Italy</a></fieldset>";
//         htmlPane.setContents(contents);

        String contents2 = "<iframe width='100%' height='100%' style='border:none;' src='http://output22.rssinclude.com/output?type=iframe&amp;id=340006&amp;hash=38101e4bbc820ab33a0d463905e0afce'></iframe>";

        htmlPane2.setContents(contents2);
        addMember(htmlPane);
        addMember(htmlPane2);

//        show();
    }
}