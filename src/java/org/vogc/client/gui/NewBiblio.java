package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.IntegerItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import org.vogc.client.NewBiblioNoteService;
import org.vogc.client.NewBiblioNoteServiceAsync;
import org.vogc.client.datatype.Author;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.Paper;
import org.vogc.client.datatype.User;

/**
 *
 * @author EM
 */
public class NewBiblio extends VLayout {

    public static NewBiblioNoteServiceAsync getBibloService() {

        return GWT.create(NewBiblioNoteService.class);
    }

    public NewBiblio() {
        setBackgroundImage("background.jpg");

        final DynamicForm dataBiblio = new DynamicForm();

        final TextItem idObjectName = new TextItem("scientificName", "Object Name *");
        idObjectName.setRequired(true);

        final IntegerItem idPaper = new IntegerItem();
        idPaper.setRequired(true);
        idPaper.setName("idpaper");
        idPaper.setTitle("journal *");

        final IntegerItem numberAuthors = new IntegerItem();
        numberAuthors.setName("numberAuthors");
        numberAuthors.setTitle("Number of authors");

        final TextItem idAuthors = new TextItem("idA", "id Authors");

        final TextItem title = new TextItem("title", "Title");

        final TextItem uri = new TextItem("urifield", "Source/URI *");
        uri.setRequired(true);

        final IntegerItem year = new IntegerItem();
        year.setName("yearnumber");
        year.setTitle("Year");

        final TextAreaItem messageItem = new TextAreaItem();
        messageItem.setShowTitle(true);
        messageItem.setName("descriptionvalue");
        messageItem.setTitle("Description");
        messageItem.setColSpan(2);
        messageItem.setHeight(150);
        messageItem.setWidth(200);
        messageItem.setLength(5000);

        // dataBiblio.setFields(title, messageItem, uri, year, idObjectName, idPaper, numberAuthors, idAuthors);
        dataBiblio.setFields(title, messageItem, uri, year, idObjectName, idPaper, numberAuthors, idAuthors);

        //dataBiblio.setFields(idObjectName, title, uri, year);
        final AsyncCallback<ErrorReport> biblioCallback = new AsyncCallback<ErrorReport>() {
            public void onFailure(Throwable caught) {
                // throw new UnsupportedOperationException("Not supported yet.");
                // SC.say("FAILED");
                SC.warn("server side failure " + caught);
            }

            public void onSuccess(ErrorReport result) {
                // throw new UnsupportedOperationException("Not supported yet.");
                // SC.say("OK");

                //SC.warn("OK:" + result.getCode() + "    " + result.getMessage() + "  " + result.getAdditionalInfo());
                SC.say("SUCCESS");
            }
        };
        IButton shortMessageButton = new IButton("New Biblio");
        shortMessageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (dataBiblio.validate(true)) {
                    dataBiblio.saveData();

                    BiblioReference br = new BiblioReference();

                    ArrayList<Author> authors = new ArrayList<Author>(2);
                    Paper p = new Paper();

                    int paper = Integer.parseInt(dataBiblio.getValueAsString("idpaper"));

                    p.setId(paper);
                    //p.setId(idPaper.getAttributeAsInt("idpaper"));
                    Author a1 = new Author();
                    //Author a2 = new Author();
                    a1.setId(Integer.parseInt(dataBiblio.getValueAsString("idA")));
                    //a2.setId(21);
                    authors.add(a1);
                    //authors.add(a2);
                    User u = new User();

                    br.setUser(u);
                    Utility a = new Utility();
                    u.setUserId(a.getUserId()); // user dell'utente che accede a vogcluster

                    br.setTitle(dataBiblio.getValueAsString("title"));

                    br.setDescription(dataBiblio.getValueAsString("descriptionvalue")); // messaggio

                    br.setUri(dataBiblio.getValueAsString("urifield"));

                    br.setYear(dataBiblio.getValueAsString("yearnumber"));

                    br.setPaper(p);

                    br.setObjectId(dataBiblio.getValueAsString("scientificName").replaceAll(" ", "")); //J0514-4002

                    br.setAuthors(authors);

                    br.setType(1);

                    getBibloService().saveBiblioRef(br, biblioCallback);
                }
            }
        });

        addMember(dataBiblio);
        addMember(shortMessageButton);

    }
}
