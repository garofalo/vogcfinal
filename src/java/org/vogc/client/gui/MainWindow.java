package org.vogc.client.gui;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.HeaderControls;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;

/**
 *
 * @author EM
 */
public class MainWindow extends Window {

    public MainWindow(String usernametop) {
        setID("MainWindow");
        setTitle("VOGCLUSTERS APPLICATION -User: " + usernametop);
        // Window w = new Window();
        setShowMinimizeButton(false);
        setCanDrag(false);
        setCanDragReposition(false);

//        setShowCloseButton(true);  BLOCCARE FINESTRA
//        setCanDragReposition(false);
//        setCanDragResize(false);
//        setShowShadow(false);

        DynamicForm sysOut = new DynamicForm();
        sysOut.setWidth(75);
        sysOut.setNumCols(1);
        sysOut.setLayoutAlign(Alignment.CENTER);
        StaticTextItem logout = new StaticTextItem();
        logout.setTitle("Logout");
        //logout.setDefaultValue(usernametop);
        FormItemIcon icon = new FormItemIcon();
        icon.setSrc("woo/user_close_32.png");
        logout.setIcons(icon);
        icon.addFormItemClickHandler(new FormItemClickHandler() {
            @Override
            public void onFormItemClick(FormItemIconClickEvent event) {
                com.google.gwt.user.client.Window.Location.reload();

            }
        });

        sysOut.setItems(logout);
        setHeaderControls(HeaderControls.HEADER_LABEL, sysOut);



        setWidth100();
        setHeight100();

        addItem(MainPanel.getInstance());

    }
}
