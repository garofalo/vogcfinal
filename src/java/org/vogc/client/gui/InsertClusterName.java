package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.GetVObjectService;
import org.vogc.client.GetVObjectServiceAsync;
import org.vogc.client.datatype.GClusterInfo;

/**
 *
 * @author Ettore
 */
public class InsertClusterName extends VLayout {
    // Listen for the button clicks

    public static GetVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.
        return GWT.create(GetVObjectService.class);
    }
    public String s;

    public InsertClusterName() {

        final TextItem nameItem = new TextItem();

        nameItem.setName("CLUSTERNAME");
        nameItem.setTitle("Cluster");
        s = nameItem.getAttributeAsString("CLUSTERNAME");


        //nameItem.disable();


        final ButtonItem buttonItem = new ButtonItem();
        buttonItem.setName("proceed");
        buttonItem.setTitle("Proceed");


        final DynamicForm controls = new DynamicForm();
        controls.setIsGroup(true);
        controls.setGroupTitle("enter the name of the new CLUSTER");
        controls.setNumCols(2);
        controls.setFields(nameItem, buttonItem);
        final AsyncCallback<GClusterInfo> callback = new AsyncCallback<GClusterInfo>() {
            public void onSuccess(GClusterInfo result) {

                SC.warn("CLUSTER ALREADY EXISTS INTO DB TRY WITH ANOTHER NAME");

            }

            public void onFailure(Throwable caught) {
                InsertClusterStandAlone cluster = new InsertClusterStandAlone();
                addMember(cluster);

            }
        };

        buttonItem.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

                getService().getGcluster(controls.getValueAsString("CLUSTERNAME"), callback);
                buttonItem.setDisabled(true);
                controls.hide();
            }
        });
        addMember(controls);
    }
}
