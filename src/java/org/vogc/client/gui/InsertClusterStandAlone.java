package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import org.vogc.client.NewVObjectService;
import org.vogc.client.NewVObjectServiceAsync;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.VObject;
import org.vogc.client.datatype.VObjectAttribute;

/**
 * Example class using the NewVObjectService service.
 *
 * @author Sabrina
 */
public class InsertClusterStandAlone extends VLayout {

    public static NewVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(NewVObjectService.class);
    }
    private final String empty = "EMPTY";
//    private TextBox txtUserInput = new TextBox();
    // private Button btnSend = new Button("Send to server");
    private final Button btnSend2 = new Button("New VOBject");
//    private Grid myGrid = new Grid(50, 50);
    public int type = 0;

    public InsertClusterStandAlone() {

        Label label = new Label();
        label.setHeight(20);
        label.setWidth(100);
        label.setContents("<b>NEW CLUSTER</b>");

        final DynamicForm insertobj = new DynamicForm();
        insertobj.setIsGroup(true);
        insertobj.setGroupTitle("All fields with * are required");
        insertobj.setNumCols(8);

        final TextItem idObjectName = new TextItem("objectId", "Object Name *");
        idObjectName.setRequired(true);

        final TextItem valueName = new TextItem("valname", "Alias Name ");
        valueName.setDefaultValue(empty);

        final TextItem ra = new TextItem("ravalue", "Ra *");
        ra.setMask("[0-2][0-3]? [0-5][0-9]? [0-5][0-9][.] ##");
        ra.setPrompt(" example [0..23] [0..59] [0..59].xx");
        ra.setRequired(true);

        final TextItem dec = new TextItem("decvalue", "Dec *");
        dec.setMask("[+--][0-8][0-9]? [0-5][0-9]? [0-5][0-9][.] #");
        dec.setPrompt("the sign is mandatory example [+/- 0..89] [0..59] [0..59].x");
        dec.setRequired(true);

        final TextItem lii = new TextItem("liivalue", "Lii ");
        lii.setDefaultValue(empty);

        final TextItem bii = new TextItem("biivalue", "Bii ");
        bii.setDefaultValue(empty);

        final TextItem reddening = new TextItem("reddeningvalue", "Reddening ");
        reddening.setDefaultValue(empty);

        final TextItem hb_vmag = new TextItem("hb_vmagvalue", "Hb_vmag ");
        hb_vmag.setDefaultValue(empty);

        final TextItem metallicity = new TextItem("metallicityvalue", "Metallicity ");
        metallicity.setDefaultValue(empty);

        final TextItem trh = new TextItem("trhvalue", "Trh ");
        trh.setDefaultValue(empty);

        final TextItem iso = new TextItem("isovalue", "Iso ");
        iso.setDefaultValue(empty);

        final TextItem helio_distance = new TextItem("helio_distancevalue", "Helio_distance ");
        helio_distance.setDefaultValue(empty);

        final TextItem galcen_distance = new TextItem("galcen_distancevalue", "Galcen_distance ");
        galcen_distance.setDefaultValue(empty);

        final TextItem x_distance = new TextItem("x_distancevalue", "X_distance ");
        x_distance.setDefaultValue(empty);

        final TextItem y_distance = new TextItem("y_distancevalue", "Y_distance ");
        y_distance.setDefaultValue(empty);

        final TextItem z_distance = new TextItem("z_distancevalue", "Z_distance ");
        z_distance.setDefaultValue(empty);

        final TextItem app_distance_mod = new TextItem("app_distance_modvalue", "App_distance_mod ");
        app_distance_mod.setDefaultValue(empty);

        final TextItem vmag = new TextItem("vmagvalue", "Vmag ");
        vmag.setDefaultValue(empty);

        final TextItem abs_vmag = new TextItem("abs_vmagvalue", "Abs_vmag ");
        abs_vmag.setDefaultValue(empty);

        final TextItem ub_color = new TextItem("ub_colorvalue", "Ub_color ");
        ub_color.setDefaultValue(empty);

        final TextItem bv_color = new TextItem("bv_colorvalue", "Bv_color ");
        bv_color.setDefaultValue(empty);

        final TextItem vr_color = new TextItem("vr_colorvalue", "Vr_color ");
        vr_color.setDefaultValue(empty);

        final TextItem vi_color = new TextItem("vi_colorvalue", "Vi_color ");
        vi_color.setDefaultValue(empty);

        final TextItem rrlyr_frequency = new TextItem("rrlyr_frequencyvalue", "Rrlyr_frequency ");
        rrlyr_frequency.setDefaultValue(empty);

        final TextItem hb_ratio = new TextItem("hb_ratiovalue", "Hb_ratio ");
        hb_ratio.setDefaultValue(empty);

        final TextItem hb_morph_type = new TextItem("hb_morph_typevalue", "Hb_morph_type ");
        hb_morph_type.setDefaultValue(empty);

        final TextItem spect_type = new TextItem("spect_typevalue", "Spect_type ");
        spect_type.setDefaultValue(empty);

        final TextItem radial_velocity = new TextItem("radial_velocityvalue", "Radial_velocity ");
        radial_velocity.setDefaultValue(empty);

        final TextItem radial_velocity_error = new TextItem("radial_velocity_errorvalue", "Radial_velocity_error ");
        radial_velocity_error.setDefaultValue(empty);

        final TextItem lsr_radial_velocity = new TextItem("lsr_radial_velocityvalue", "Lsr_radial_velocity ");
        lsr_radial_velocity.setDefaultValue(empty);

        final TextItem central_concentration = new TextItem("central_concentrationvalue", "Central_concentration");
        central_concentration.setDefaultValue(empty);

        final TextItem ellipticity = new TextItem("ellipticityvalue", "Ellipticity");
        ellipticity.setDefaultValue(empty);

        final TextItem core_radius = new TextItem("core_radiusvalue", "Core_radius");
        core_radius.setDefaultValue(empty);

        final TextItem halfmass_radius = new TextItem("halfmass_radiusvalue", "Halfmass_radius");
        halfmass_radius.setDefaultValue(empty);

        final TextItem tidal_radius = new TextItem("tidal_radiusvalue", "Tidal_radius");
        tidal_radius.setDefaultValue(empty);

        final TextItem log_core_relaxtime = new TextItem("log_core_relaxtimevalue", "Log_core_relaxtime");
        log_core_relaxtime.setDefaultValue(empty);

        final TextItem log_hmr_relaxtime = new TextItem("log_hmr_relaxtimevalue", "Log_hmr_relaxtime");
        log_hmr_relaxtime.setDefaultValue(empty);

        final TextItem cent_surf_brightness = new TextItem("cent_surf_brightnessvalue", "Cent_surf_brightness");
        cent_surf_brightness.setDefaultValue(empty);

        final TextItem log_lum_density = new TextItem("log_lum_densityvalue", "Log_lum_density");
        log_lum_density.setDefaultValue(empty);

        insertobj.setFields(idObjectName, valueName, ra, dec, lii, bii, reddening, hb_vmag, metallicity, trh, iso, helio_distance, galcen_distance, x_distance, y_distance, z_distance, app_distance_mod, vmag, abs_vmag, ub_color, bv_color, vr_color, vi_color, rrlyr_frequency, hb_ratio, hb_morph_type, spect_type, radial_velocity, radial_velocity_error, lsr_radial_velocity, central_concentration, ellipticity, core_radius, halfmass_radius, tidal_radius, log_core_relaxtime, log_hmr_relaxtime, cent_surf_brightness, log_lum_density);

        addMember(label);

        addMember(insertobj);

        addMember(btnSend2);

        // final AsyncCallback<ErrorReport> callback3 = new AsyncCallback<ErrorReport>() {
//            public void onSuccess(ErrorReport result) {
//                GWT.log("FERMATI sono la callback 3",null);
//                SC.warn("OK sono la callback 3" + result.getMessage());
//            }
//            public void onFailure(Throwable caught) {
//                SC.warn("Communication failed");
//            }
//        };
        final AsyncCallback<ErrorReport> callback2 = new AsyncCallback<ErrorReport>() {
            public void onSuccess(ErrorReport result) {
                GWT.log("FERMATI sono la callback2", null);
                //  SC.warn("OK sono l callback2" + result.getMessage());
                SC.say("SUCCESS");

            }

            public void onFailure(Throwable caught) {
                SC.warn("Communication failed");
            }
        };

        btnSend2.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (insertobj.validate(true)) {
                    insertobj.saveData();
                    VObject x = new VObject();

                    x.setObjType(1);
                    x.setId(insertobj.getValueAsString("objectId").replaceAll(" ", ""));
                    x.setName(insertobj.getValueAsString("valname"));
                    Utility a = new Utility();
                    x.setSourceId(a.getUserId());
                    x.setType(0);
                    ArrayList<VObjectAttribute> attList = new ArrayList<VObjectAttribute>(40);
                    VObjectAttribute first = new VObjectAttribute();
                    VObjectAttribute second = new VObjectAttribute();
                    VObjectAttribute tre = new VObjectAttribute();
                    VObjectAttribute quattro = new VObjectAttribute();
                    VObjectAttribute cinque = new VObjectAttribute();
                    VObjectAttribute sei = new VObjectAttribute();
                    VObjectAttribute sette = new VObjectAttribute();
                    VObjectAttribute otto = new VObjectAttribute();
                    VObjectAttribute nove = new VObjectAttribute();
                    VObjectAttribute dieci = new VObjectAttribute();
                    VObjectAttribute undici = new VObjectAttribute();
                    VObjectAttribute dodici = new VObjectAttribute();
                    VObjectAttribute tredici = new VObjectAttribute();
                    VObjectAttribute quattordici = new VObjectAttribute();
                    VObjectAttribute quindici = new VObjectAttribute();
                    VObjectAttribute sedici = new VObjectAttribute();
                    VObjectAttribute diciassette = new VObjectAttribute();
                    VObjectAttribute diciotto = new VObjectAttribute();
                    VObjectAttribute diciannove = new VObjectAttribute();
                    VObjectAttribute venti = new VObjectAttribute();
                    VObjectAttribute ventuno = new VObjectAttribute();
                    VObjectAttribute ventidue = new VObjectAttribute();
                    VObjectAttribute ventitre = new VObjectAttribute();
                    VObjectAttribute ventiquattro = new VObjectAttribute();
                    VObjectAttribute venticinque = new VObjectAttribute();
                    VObjectAttribute ventisei = new VObjectAttribute();
                    VObjectAttribute ventisette = new VObjectAttribute();
                    VObjectAttribute ventotto = new VObjectAttribute();
                    VObjectAttribute ventinove = new VObjectAttribute();
                    VObjectAttribute trenta = new VObjectAttribute();
                    VObjectAttribute trentuno = new VObjectAttribute();
                    VObjectAttribute trentadue = new VObjectAttribute();
                    VObjectAttribute trentatre = new VObjectAttribute();
                    VObjectAttribute trentaquattro = new VObjectAttribute();
                    VObjectAttribute trentacinque = new VObjectAttribute();
                    VObjectAttribute trentasei = new VObjectAttribute();
                    VObjectAttribute trentasette = new VObjectAttribute();

                    first.setIsNew(0);
                    first.setName("ra");
//                    String prova;
//                    prova = insertobj.getValueAsString("ravalue");
                    first.setValue(insertobj.getValueAsString("ravalue"));
                    second.setIsNew(0);
                    second.setName("dec");
                    second.setValue(insertobj.getValueAsString("decvalue"));
                    tre.setIsNew(0);
                    tre.setName("lii");
                    tre.setValue(insertobj.getValueAsString("liivalue"));
                    quattro.setIsNew(0);
                    quattro.setName("bii");
                    quattro.setValue(insertobj.getValueAsString("biivalue"));
                    cinque.setIsNew(0);
                    cinque.setName("reddening");
                    cinque.setValue(insertobj.getValueAsString("reddeningvalue"));
                    sei.setIsNew(0);
                    sei.setName("hb_vmag");
                    sei.setValue(insertobj.getValueAsString("hb_vmagvalue"));
                    sette.setIsNew(0);
                    sette.setName("metallicity");
                    sette.setValue(insertobj.getValueAsString("metallicityvalue"));
                    otto.setIsNew(0);
                    otto.setName("trh");
                    otto.setValue(insertobj.getValueAsString("trhvalue"));
                    nove.setIsNew(0);
                    nove.setName("iso");
                    nove.setValue(insertobj.getValueAsString("isovalue"));
                    dieci.setIsNew(0);
                    dieci.setName("helio_distance");
                    dieci.setValue(insertobj.getValueAsString("helio_distancevalue"));
                    undici.setIsNew(0);
                    undici.setName("galcen_distance");
                    undici.setValue(insertobj.getValueAsString("galcen_distancevalue"));
                    dodici.setIsNew(0);
                    dodici.setName("x_distance");
                    dodici.setValue(insertobj.getValueAsString("x_distancevalue"));
                    tredici.setIsNew(0);
                    tredici.setName("y_distance");
                    tredici.setValue(insertobj.getValueAsString("y_distancevalue"));
                    quattordici.setIsNew(0);
                    quattordici.setName("z_distance");
                    quattordici.setValue(insertobj.getValueAsString("z_distancevalue"));
                    quindici.setIsNew(0);
                    quindici.setName("app_distance_mod");
                    quindici.setValue(insertobj.getValueAsString("app_distance_modvalue"));
                    sedici.setIsNew(0);
                    sedici.setName("vmag");
                    sedici.setValue(insertobj.getValueAsString("vmagvalue"));
                    diciassette.setIsNew(0);
                    diciassette.setName("abs_vmag");
                    diciassette.setValue(insertobj.getValueAsString("abs_vmagvalue"));
                    diciotto.setIsNew(0);
                    diciotto.setName("ub_color");
                    diciotto.setValue(insertobj.getValueAsString("ub_colorvalue"));
                    diciannove.setIsNew(0);
                    diciannove.setName("bv_color");
                    diciannove.setValue(insertobj.getValueAsString("bv_colorvalue"));
                    venti.setIsNew(0);
                    venti.setName("vr_color");
                    venti.setValue(insertobj.getValueAsString("vr_colorvalue"));
                    ventuno.setIsNew(0);
                    ventuno.setName("vi_color");
                    ventuno.setValue(insertobj.getValueAsString("vi_colorvalue"));
                    ventidue.setIsNew(0);
                    ventidue.setName("rrlyr_frequency");
                    ventidue.setValue(insertobj.getValueAsString("rrlyr_frequencyvalue"));
                    ventitre.setIsNew(0);
                    ventitre.setName("hb_ratio");
                    ventitre.setValue(insertobj.getValueAsString("hb_ratiovalue"));
                    ventiquattro.setIsNew(0);
                    ventiquattro.setName("hb_morph_type");
                    ventiquattro.setValue(insertobj.getValueAsString("hb_morph_typevalue"));
                    venticinque.setIsNew(0);
                    venticinque.setName("spect_type");
                    venticinque.setValue(insertobj.getValueAsString("spect_typevalue"));
                    ventisei.setIsNew(0);
                    ventisei.setName("radial_velocity");
                    ventisei.setValue(insertobj.getValueAsString("radial_velocityvalue"));
                    ventisette.setIsNew(0);
                    ventisette.setName("radial_velocity_error");
                    ventisette.setValue(insertobj.getValueAsString("radial_velocity_errorvalue"));
                    ventotto.setIsNew(0);
                    ventotto.setName("lsr_radial_velocity");
                    ventotto.setValue(insertobj.getValueAsString("lsr_radial_velocityvalue"));
                    ventinove.setIsNew(0);
                    ventinove.setName("central_concentration");
                    ventinove.setValue(insertobj.getValueAsString("central_concentrationvalue"));
                    trenta.setIsNew(0);
                    trenta.setName("ellipticity");
                    trenta.setValue(insertobj.getValueAsString("ellipticityvalue"));
                    trentuno.setIsNew(0);
                    trentuno.setName("core_radius");
                    trentuno.setValue(insertobj.getValueAsString("core_radiusvalue"));
                    trentadue.setIsNew(0);
                    trentadue.setName("halfmass_radius");
                    trentadue.setValue(insertobj.getValueAsString("halfmass_radiusvalue"));
                    trentatre.setIsNew(0);
                    trentatre.setName("tidal_radius");
                    trentatre.setValue(insertobj.getValueAsString("tidal_radiusvalue"));
                    trentaquattro.setIsNew(0);
                    trentaquattro.setName("log_core_relaxtime");
                    trentaquattro.setValue(insertobj.getValueAsString("log_core_relaxtimevalue"));
                    trentacinque.setIsNew(0);
                    trentacinque.setName("log_hmr_relaxtime");
                    trentacinque.setValue(insertobj.getValueAsString("log_hmr_relaxtimevalue"));
                    trentasei.setIsNew(0);
                    trentasei.setName("cent_surf_brightness");
                    trentasei.setValue(insertobj.getValueAsString("cent_surf_brightnessvalue"));
                    trentasette.setIsNew(0);
                    trentasette.setName("log_lum_density");
                    trentasette.setValue(insertobj.getValueAsString("log_lum_densityvalue"));
                    attList.add(first);
                    attList.add(second);
                    attList.add(tre);
                    attList.add(quattro);
                    attList.add(cinque);
                    attList.add(sei);
                    attList.add(sette);
                    attList.add(otto);
                    attList.add(nove);
                    attList.add(dieci);
                    attList.add(undici);
                    attList.add(dodici);
                    attList.add(tredici);
                    attList.add(quattordici);
                    attList.add(quindici);
                    attList.add(sedici);
                    attList.add(diciassette);
                    attList.add(diciotto);
                    attList.add(diciannove);
                    attList.add(venti);
                    attList.add(ventuno);
                    attList.add(ventidue);
                    attList.add(ventitre);
                    attList.add(ventiquattro);
                    attList.add(venticinque);
                    attList.add(ventisei);
                    attList.add(ventisette);
                    attList.add(ventotto);
                    attList.add(ventinove);
                    attList.add(trenta);
                    attList.add(trentuno);
                    attList.add(trentadue);
                    attList.add(trentatre);
                    attList.add(trentaquattro);
                    attList.add(trentacinque);
                    attList.add(trentasei);
                    attList.add(trentasette);
                    x.setAttribute(attList);
                    // riempi oggetto
                    getService().saveNewVObject(x, null, callback2);
                }
            }
        });
    }
}
