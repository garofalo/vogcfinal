package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ItemHoverEvent;
import com.smartgwt.client.widgets.form.fields.events.ItemHoverHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.UpdateService;
import org.vogc.client.UpdateServiceAsync;
import org.vogc.client.datatype.ErrorReport;

/**
 *
 * @author EM
 */
public class UpdateObject extends VLayout {

    public static UpdateServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(UpdateService.class);
    }
    private final String maskCCC = "CCCCCCCCCCCCC";
    private final Button buttonValue = new Button("Update value");
    int type;
    int objId;
    String att;

    public UpdateObject() {
        setBackgroundImage("background.jpg");
        /**
         * ********************************CANCELLA
         * VALORE***********************
         */
        final DynamicForm insertobj = new DynamicForm();
        insertobj.setIsGroup(true);
        insertobj.setGroupTitle("All fields with * are required");
        insertobj.setNumCols(8);
        Label label = new Label();
        label.setHeight(5);
        label.setShowEdges(true);
        label.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>Update Value Attribute Object</FONT></p></font>");

        final ComboBoxItem selectObject = new ComboBoxItem();
        selectObject.setRequired(true);
        selectObject.setTitle("Object Type*");
        selectObject.setValueMap("Cluster", "Horizontal Branch Star", "Pulsar");
        selectObject.setDefaultValue("         ");
        selectObject.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent event) {
                String ds = (String) event.getValue();
                if (ds.equalsIgnoreCase("Cluster")) {
                    type = 1;
                } else if (ds.equalsIgnoreCase("Pulsar")) {
                    type = 2;
                } else if (ds.equalsIgnoreCase("Horizontal Branch Star")) {
                    type = 3;
                }
            }
        });
        selectObject.addItemHoverHandler(new ItemHoverHandler() {
            @Override
            public void onItemHover(ItemHoverEvent event) {
                String prompt = "Select Object Type";
//                if (!selectObject.isDisabled()) {
//                }
                selectObject.setPrompt(prompt);
            }
        });

        final TextItem idObject = new TextItem("idname", "Object Name *");
        idObject.setRequired(true);

//        final TextItem attName = new TextItem("attribute", " attribute NAME *");
        final TextItem valueName = new TextItem("valname", "Value*");
        valueName.setRequired(true);

        final ComboBoxItem attributeName = new ComboBoxItem();
        attributeName.setRequired(true);
        attributeName.setTitle("Attribute Name *");
        attributeName.setValueMap("ra", "dec", "lii", "bii", "reddening", "hb_vmag", "metallicity", "trh", "iso", "helio_distance", "galcen_distance", "x_distance", "y_distance", "z_distance", "app_distance_mod", "vmag", "abs_vmag", "ub_color", "bv_color", "vr_color", "vi_color", "rrlyr_frequency", "hb_ratio", "hb_morph_type", "spect_type", "radial_velocity", "radial_velocity_error", "lsr_radial_velocity", "central_concentration", "ellipticity", "core_radius", "halfmass_radius", "tidal_radius", "log_core_relaxtime", "log_hmr_relaxtime", "cent_surf_brightness", "log_lum_density", "offset", "period");
        attributeName.setDefaultValue("         ");

        attributeName.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent event) { // scelta attributo
                String ds2 = (String) event.getValue();
                if (ds2.equalsIgnoreCase("ra")) {
                    objId = 1;
                    att = "ra";
                    valueName.setMask("[0-2][0-3]? [0-5][0-9]? [0-5][0-9][.] ##");
                } else if (ds2.equalsIgnoreCase("dec")) {
                    objId = 2;
                    att = "dec";
                    valueName.setMask("[+--][0-8][0-9]? [0-5][0-9]? [0-5][0-9][.] #");
                } else if (ds2.equalsIgnoreCase("lii")) {
                    objId = 3;
                    att = "lii";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("bii")) {
                    objId = 4;
                    att = "bii";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("reddening")) {
                    objId = 5;
                    att = "reddening";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("hb_vmag")) {
                    objId = 6;
                    att = "hb_vmag";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("metallicity")) {
                    objId = 7;
                    att = "metallicity";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("trh")) {
                    objId = 8;
                    att = "trh";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("iso")) {
                    objId = 9;
                    att = "iso";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("helio_distance")) {
                    objId = 10;
                    att = "helio_distance";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("galcen_distance")) {
                    objId = 11;
                    att = "galcen_distance";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("x_distance")) {
                    objId = 12;
                    att = "x_distance";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("y_distance")) {
                    objId = 13;
                    att = "y_distance";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("z_distance")) {
                    objId = 14;
                    att = "z_distance";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("app_distance_mod")) {
                    objId = 15;
                    att = "app_distance_mod";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("vmag")) {
                    objId = 16;
                    att = "vmag";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("abs_vmag")) {
                    objId = 17;
                    att = "abs_vmag";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("ub_color")) {
                    objId = 18;
                    att = "ub_color";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("bv_color")) {
                    objId = 19;
                    att = "bv_color";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("vr_color")) {
                    objId = 20;
                    att = "vr_color";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("vi_color")) {
                    objId = 21;
                    att = "vi_color";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("rrlyr_frequency")) {
                    objId = 22;
                    att = "rrlyr_frequency";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("hb_ratio")) {
                    objId = 23;
                    att = "hb_ratio";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("hb_morph_type")) {
                    objId = 24;
                    att = "hb_morph_type";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("spect_type")) {
                    objId = 25;
                    att = "spect_type";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("radial_velocity")) {
                    objId = 26;
                    att = "radial_velocity";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("radial_velocity_error")) {
                    objId = 27;
                    att = "radial_velocity_error";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("lsr_radial_velocity")) {
                    objId = 28;
                    att = "lsr_radial_velocity";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("central_concentration")) {
                    objId = 29;
                    att = "central_concentration";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("ellipticity")) {
                    objId = 30;
                    att = "ellipticity";
                    valueName.setMask(maskCCC);
                } else if (ds2.equalsIgnoreCase("core_radius")) {
                    objId = 31;
                    att = "core_radius";
                    valueName.setMask(maskCCC);

                } else if (ds2.equalsIgnoreCase("halfmass_radius")) {
                    objId = 32;
                    att = "halfmass_radius";
                    valueName.setMask(maskCCC);

                } else if (ds2.equalsIgnoreCase("tidal_radius")) {
                    objId = 33;
                    att = "tidal_radius";
                    valueName.setMask(maskCCC);

                } else if (ds2.equalsIgnoreCase("log_core_relaxtime")) {
                    objId = 34;
                    att = "log_core_relaxtime";
                    valueName.setMask(maskCCC);

                } else if (ds2.equalsIgnoreCase("log_hmr_relaxtime")) {
                    objId = 35;
                    att = "log_hmr_relaxtime";
                    valueName.setMask(maskCCC);

                } else if (ds2.equalsIgnoreCase("cent_surf_brightness")) {
                    objId = 36;
                    att = "cent_surf_brightness";
                    valueName.setMask(maskCCC);

                } else if (ds2.equalsIgnoreCase("log_lum_density")) {
                    objId = 37;
                    att = "log_lum_density";
                    valueName.setMask(maskCCC);

                } else if (ds2.equalsIgnoreCase("offset")) {
                    objId = 1;
                    att = "offset";
                    valueName.setMask("CCCCCCCCCC");
                } else if (ds2.equalsIgnoreCase("period")) {
                    objId = 2;
                    att = "period";
                    valueName.setMask("CCCCCCCCCC");
                }

            }
        });

        attributeName.addItemHoverHandler(new ItemHoverHandler() {
            @Override
            public void onItemHover(ItemHoverEvent event) {
                String prompt = "Select Attribute to update";
//                if (!selectObject.isDisabled()) {
//                }
                attributeName.setPrompt(prompt);
            }
        });

        insertobj.setFields(selectObject, idObject, attributeName, valueName);

        //insertobj.setFields(selectObject, idObject, valueName, attName);
        final AsyncCallback<ErrorReport> callback = new AsyncCallback<ErrorReport>() {
            @Override
            public void onSuccess(ErrorReport result) {
                if (result.getCode().equals("vogcSRV013")) {
                    SC.say("SUCCESS");
                } else {
                    if (result.getCode().equals("vogcSRV0006")) {

                        SC.warn("You aren't the author of this object you can't modify the value");
                    } else {
                        {
                            SC.warn("Object Not Exist");
                        }
                    }
                }
            }

            @Override
            public void onFailure(Throwable caught) {
                SC.say("ERRORE");
            }
        };

        buttonValue.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (insertobj.validate(true)) {
                    // int type = Integer.parseInt(insertobj.getValueAsString("objectType"));
                    //int id = 5; // per ora è fisso tanto per il cluster non è usato poi dovrà essere eliminato
                    // getService().updateAttributeValue("sabrina.checola@gmail.com", type, id, insertobj.getValueAsString("valname"), insertobj.getValueAsString("idname"), insertobj.getValueAsString("attribute"), callback);
                    Utility a = new Utility();
                    getService().updateAttributeValue(a.getUserId(), type, objId, insertobj.getValueAsString("valname").replaceAll(" ", ""), insertobj.getValueAsString("idname").replaceAll(" ", ""), att, callback);
                }
            }
        });

        addMember(label);
        addMember(insertobj);
        addMember(buttonValue);
        show();
    }
}
