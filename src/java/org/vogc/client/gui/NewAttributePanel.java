package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.NewAttributeService;
import org.vogc.client.NewAttributeServiceAsync;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.VObjectAttribute;
//import org.vogc.client.gui.data.FormFieldNewAttribute;

/**
 *
 * @author EM
 */
public class NewAttributePanel extends VLayout {

    public static NewAttributeServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(NewAttributeService.class);
    }
    public int intero;

    public NewAttributePanel() {
        Label label = new Label();
        label.setHeight(5);
        label.setShowEdges(true);
        label.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>New Attribute Value</FONT></p></font>");
        setBackgroundImage("background.jpg");

        // final DataSource dataSource = FormFieldNewAttribute.getInstance();

        //final FormFieldNewAttribute form2 = FormFieldNewAttribute.getInstance();

        final DynamicForm form = new DynamicForm();
        form.setIsGroup(true);
        form.setGroupTitle("All fields with * are required");
        form.setNumCols(8);
        final TextItem objectidField = new TextItem("objectId", "Object Name *");
        objectidField.setRequired(true);

        final TextItem attributeNameField = new TextItem("OptionalNameDescription", " Attribute Name * ");
        attributeNameField.setRequired(true);

        final TextItem attributeDeskeField = new TextItem("attributeDesk", "Attribute Description");
        attributeDeskeField.setTooltip("a brief description of the attribute");

        final TextItem attributeUCDField = new TextItem("UCD", "UCD(Uniform Content Description)");
        attributeUCDField.setDefaultValue("null");

        final TextItem attributeDatatypeField = new TextItem("AttributeDatatype", "Attribute Datatype *");
        attributeDatatypeField.setDefaultValue("double");

        final TextItem attributevalueField = new TextItem("AttributeValue", "Attribute Value *");
        attributevalueField.setRequired(true);

        //final TextItem attributeTypeField = new TextItem("attribute Type", "Attribute Type");

        final ComboBoxItem unitsField = new ComboBoxItem();

        unitsField.setRequired(true);
        unitsField.setTitle("Category *");
        unitsField.setName("Categorytype");

        unitsField.setValueMap("Positional", "Photometric", "Dynamic");
        // unitsField.getAttribute("Categorytype");

        // unitsField.setDefaultValue("       ");
        final AsyncCallback<ErrorReport> callback = new AsyncCallback<ErrorReport>() {
            @Override
            public void onSuccess(ErrorReport result) {
                // lblServerReply.setText(result.getMessage());


                // SC.warn("OK:" + result.getCode() + "    " + result.getMessage() + "  " + result.getAdditionalInfo());
                SC.say("SUCCESS");
            }

            @Override
            public void onFailure(Throwable caught) {
                //  lblServerReply.setText("Communication failed");
                SC.warn("server side failure " + caught);
            }
        };
        unitsField.addChangedHandler(new ChangedHandler() {
            public void onChanged(ChangedEvent event) {
                String ds = (String) event.getValue();

                if (ds.equalsIgnoreCase("Positional")) {
                    //unitsField.setDisabled(Boolean.TRUE);

                    intero = 0;


                } else {
                    if (ds.equalsIgnoreCase("Photometric")) {

                        //unitsField.setDisabled(Boolean.TRUE);
                        intero = 1;

                    } else {
                        if (ds.equalsIgnoreCase("Dynamic")) {

                            //  unitsField.setDisabled(Boolean.TRUE);
                            intero = 2;


                        }

                    }
                }
            }
        });
//        unitsField.addItemHoverHandler(new ItemHoverHandler() {
//
//            public void onItemHover(ItemHoverEvent event) {
//                String prompt = "Status can only be changed by the bug's owner";
//                if (!unitsField.isDisabled()) {
//                    //  prompt = getHoverText((String) severityLevel.getValue());
//                }
//                unitsField.setPrompt(prompt);
//            }
//        });




        form.setFields(objectidField, attributeNameField, attributeDeskeField, attributeUCDField, attributevalueField);

        //form.setDataSource(form2);




        IButton button = new IButton("Apply");
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (form.validate(true)) {
                    GWT.log("FERMATI", null);
                    //  form.saveData();
                    final VObjectAttribute att = new VObjectAttribute();

                    att.setName(form.getValueAsString("OptionalNameDescription").replaceAll(" ", ""));
                    att.setDescription(form.getValueAsString("attributeDesk").replaceAll(" ", ""));
                    att.setUcd(form.getValueAsString("UCD").replaceAll(" ", ""));
                    att.setDatatype(form.getValueAsString("AttributeDatatype"));
                    att.setValue(form.getValueAsString("AttributeValue"));
                    att.setType(0);
                    String objectId = form.getValueAsString("objectId").replaceAll(" ", "");
                    Utility a = new Utility();
                    String userId = a.getUserId();


                    getService().newAttribute(att, userId, objectId, callback);

                }
            }
        });
        addMember(label);
        addMember(form);
        addMember(button);



    }
}
