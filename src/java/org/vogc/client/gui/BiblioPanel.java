package org.vogc.client.gui;

/**
 *
 * @author Ettore
 */
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.Iterator;
import org.vogc.client.GetVObjectService;
import org.vogc.client.GetVObjectServiceAsync;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.GClusterInfo;

public class BiblioPanel extends VLayout {

    public static GetVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(GetVObjectService.class);
    }
    private Grid myGridbiblio = new Grid(100, 4);
    private String s;

    public BiblioPanel(String str) {
        s = str;
        //addMember(myGrid);

        // Create an asynchronous callback to handle the result.


        final AsyncCallback<GClusterInfo> callback = new AsyncCallback<GClusterInfo>() {
            public void onSuccess(GClusterInfo result) {

                myGridbiblio.setText(0, 0, "Title");
                myGridbiblio.setText(0, 1, "year");
                myGridbiblio.setText(0, 2, "Date");
                myGridbiblio.setText(0, 3, "Description");
                Iterator nrow = result.getGcAtt().iterator();
                int row = 1, i = 0;
                while (nrow.hasNext()) {
                    BiblioReference att = (BiblioReference) nrow.next();

                    myGridbiblio.setBorderWidth(1);
                    myGridbiblio.setText(row, 0,  att.getTitle() + ", ");
                    myGridbiblio.setText(row, 1,  att.getYear() + ", ");
                    myGridbiblio.setText(row, 2,  att.getDate() + ", ");
                    myGridbiblio.setText(row, 3, att.getDescription() + ", ");
                    // lista.add(att.getAttPrimaryInfo().getDescription());

                    row++;
                    // rec [row] = att.getAttPrimaryInfo().getDescription();


                }
                //lblServerReply.setText("OK " + result.getName() + result.getType());

                myGridbiblio.resizeRows(row);
                Label labelNewObject = new Label();
                labelNewObject.setHeight(10);
                labelNewObject.setShowEdges(true);
                labelNewObject.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>" + " Biblio note of " + s + "</FONT></p></font>");

                addMember(labelNewObject);

                addMember(myGridbiblio);
                //myGrid2biblio.resizeRows(row);
            }

            public void onFailure(Throwable caught) {
                SC.warn("No information found for this object");
            }
        };
        setBackgroundImage("background.jpg");
        getService().getBiblioGcluster(s, callback);


        // orizz.addMember(myGrid2biblio);

//        show();
        // Listen for the button clicks
//        btnSend.addClickHandler(new ClickHandler(){
//            public void onClick(ClickEvent event) {
//                // Make remote call. Control flow will continue immediately and later
//                // 'callback' will be invoked when the RPC completes.
//
//            }
//        });

//        btnUsers.addClickHandler(new ClickHandler(){
//            public void onClick(ClickEvent event) {
//                // Make remote call. Control flow will continue immediately and later
//                // 'callback' will be invoked when the RPC completes.
//                getService().getUsersList(callback2);
//            }
//        });
    }
}
