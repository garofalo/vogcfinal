package org.vogc.client.gui;

import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ItemHoverEvent;
import com.smartgwt.client.widgets.form.fields.events.ItemHoverHandler;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Example class using the NewVObjectService service.
 *
 * @author Sabrina
 */
public class InsertObject extends VLayout {

    private Label labelNewObject = new Label();
//    private TextBox txtUserInput = new TextBox();
    // private Button btnSend = new Button("Send to server");
//    private Button btnSend2 = new Button("New VOBject");
//    private Grid myGrid = new Grid(50, 50);
    public int type = 0;

    public InsertObject() {

        setBackgroundImage("background.jpg");
        labelNewObject.setHeight(10);
        labelNewObject.setShowEdges(true);
        labelNewObject.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>New Object</FONT></p></font>");

        final DynamicForm insertobj = new DynamicForm();





        final ComboBoxItem severityLevel = new ComboBoxItem();
        severityLevel.setTitle("Object Type");
        severityLevel.setValueMap("Cluster", "Horizontal Branch Star", "Pulsar");
        severityLevel.setDefaultValue("         ");



        severityLevel.addChangedHandler(new ChangedHandler() {
            public void onChanged(ChangedEvent event) {
                String ds = (String) event.getValue();
                if (ds.equalsIgnoreCase("Cluster")) {

                    severityLevel.setDisabled(Boolean.TRUE);
                    type = 1;
                    InsertClusterName Cluster = new InsertClusterName();
                    addMember(Cluster);

                } else {
                    if (ds.equalsIgnoreCase("Pulsar")) {

                        severityLevel.setDisabled(Boolean.TRUE);
                        type = 2;
                        InsertClusterNameforPulsar pulsar = new InsertClusterNameforPulsar();
                        addMember(pulsar);

                    } else {
                        if (ds.equalsIgnoreCase("Horizontal Branch Star")) {

                            severityLevel.setDisabled(Boolean.TRUE);
                            type = 3;
                            InsertClusterNameforStar star = new InsertClusterNameforStar();

                            addMember(star);


                        }
                    }
                }
            }
        });
        severityLevel.addItemHoverHandler(new ItemHoverHandler() {
            public void onItemHover(ItemHoverEvent event) {
                String prompt = "Select Object Type";
//                if (!severityLevel.isDisabled()) {
//                }
                severityLevel.setPrompt(prompt);
            }
        });

        insertobj.setFields(severityLevel);
        addMember(labelNewObject);
        addMember(insertobj);
    }
}
