
package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
//import com.google.gwt.user.client.ui.Grid;
//import com.google.gwt.user.client.ui.Label;
//import com.google.gwt.user.client.ui.TextBox;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ItemHoverEvent;
import com.smartgwt.client.widgets.form.fields.events.ItemHoverHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import org.vogc.client.NewVObjectService;
import org.vogc.client.NewVObjectServiceAsync;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.VObject;
import org.vogc.client.datatype.VObjectAttribute;

/**
 * Example class using the NewVObjectService service.
 *
 * @author Sabrina
 */
public class PROVAinserimentoOggetto extends VLayout {

    public static NewVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(NewVObjectService.class);
    }

//    private Label lblServerReply = new Label();
//    private TextBox txtUserInput = new TextBox();
    // private Button btnSend = new Button("Send to server");
    private Button btnSend2 = new Button("New VOBject");
//    private Grid myGrid = new Grid(50, 50);
    public int type = 0;

    public PROVAinserimentoOggetto() {


        final DynamicForm insertobj = new DynamicForm();

        final TextItem idObjectName = new TextItem("objectId", "objectId *");
        idObjectName.setRequired(true);


        final TextItem gclusterIdName = new TextItem("gclusterId", "gclusterId id*");
        //idObjectName.setRequired(true);



        final ComboBoxItem severityLevel = new ComboBoxItem();
        severityLevel.setTitle("Object Type");
        severityLevel.setValueMap("Cluster", "Horizontal Branch Star", "Pulsar");
        severityLevel.setDefaultValue("         ");

        final TextItem valueName = new TextItem("valname", "Alias Name *");
        idObjectName.setRequired(true);

        severityLevel.addChangedHandler(new ChangedHandler() {

            public void onChanged(ChangedEvent event) {
                String ds = (String) event.getValue();
                if (ds.equalsIgnoreCase("Cluster")) {

                    severityLevel.setDisabled(Boolean.TRUE);
                    type = 1;

                } else {
                    if (ds.equalsIgnoreCase("Pulsar")) {

                        severityLevel.setDisabled(Boolean.TRUE);
                        type = 2;

                    } else {
                        if (ds.equalsIgnoreCase("Horizontal Branch Star")) {

                            severityLevel.setDisabled(Boolean.TRUE);
                            type = 3;


                        }
                    }
                }
            }
        });
        severityLevel.addItemHoverHandler(new ItemHoverHandler() {

            public void onItemHover(ItemHoverEvent event) {
                String prompt = "Status can only be changed by the bug's owner";
//                if (!severityLevel.isDisabled()) {
//                }
                severityLevel.setPrompt(prompt);
            }
        });

        insertobj.setFields(severityLevel, idObjectName, gclusterIdName,valueName);



        addMember(insertobj);


        addMember(btnSend2);


       // final AsyncCallback<ErrorReport> callback3 = new AsyncCallback<ErrorReport>() {

//            public void onSuccess(ErrorReport result) {
//                GWT.log("FERMATI sono la callback 3",null);
//                SC.warn("OK sono la callback 3" + result.getMessage());
//            }

//            public void onFailure(Throwable caught) {
//                SC.warn("Communication failed");
//            }
//        };
        final AsyncCallback<ErrorReport> callback2 = new AsyncCallback<ErrorReport>() {

            public void onSuccess(ErrorReport result) {
                GWT.log("FERMATI sono la callback2",null);
              //  SC.warn("OK sono l callback2" + result.getMessage());
                SC.warn("SUCCESS" );


            }

            public void onFailure(Throwable caught) {
                SC.warn("Communication failed");
            }
        };



        btnSend2.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
                // Make remote call. Control flow will continue immediately and later
                // 'callback' will be invoked when the RPC completes.
               
                VObject x = new VObject();
               // VObject pulsar = new VObject();
//        if ( type ==1 )
//        {
                x.setObjType(type);
                x.setId(insertobj.getValueAsString("objectId"));
                x.setClusterId(insertobj.getValueAsString("gclusterId"));
                x.setName(insertobj.getValueAsString("valname"));
                x.setSourceId("HARRIS2010");
                x.setType(0);
                ArrayList<VObjectAttribute> attList = new ArrayList<VObjectAttribute>(4);
                VObjectAttribute first = new VObjectAttribute();
                VObjectAttribute second = new VObjectAttribute();
                
                first.setIsNew(0);
                first.setName("ra");
                first.setValue("0.44");
                second.setIsNew(0);
                second.setName("dec");
                second.setValue("2.79");
                attList.add(first);
                attList.add(second);
                x.setAttribute(attList);
                // riempi oggetto
                getService().saveNewVObject(x, null, callback2);
                //getService().saveNewPulsar(null, x, callback2); // Aggiunto da Luca
        //}
//              if (type == 2)
//              {
//                x.setObjType(1);
//                x.setId(insertobj.getValueAsString("objectId"));
//                x.setClusterId(insertobj.getValueAsString("gclusterId"));
//                x.setName(insertobj.getValueAsString("valname"));
//                x.setSourceId("sabrina.checola@gmail.com");
//                x.setType(0);
//                List attList2 = new ArrayList();
//                VObjectAttribute fifth = new VObjectAttribute();
//                VObjectAttribute sixth = new VObjectAttribute();
//                fifth.setIsNew(0);
//                fifth.setName("ra");
//                fifth.setValue("0.44");
//                sixth.setIsNew(0);
//                sixth.setName("dec");
//                sixth.setValue("2.79");
//                attList2.add(fifth);
//                attList2.add(sixth);
//                x.setAttribute(attList2);
//
//                pulsar.setObjType(type);
//                pulsar.setId(insertobj.getValueAsString("objectId"));
//                pulsar.setClusterId(insertobj.getValueAsString("gclusterId"));
//                pulsar.setName(insertobj.getValueAsString("valname"));
//
//                pulsar.setSourceId("sabrina.checola@gmail.com");
//                pulsar.setType(0);
//                List attList3 = new ArrayList();
//                VObjectAttribute third = new VObjectAttribute();
//                VObjectAttribute fourth = new VObjectAttribute();
//                third.setIsNew(0);
//                third.setName("ra");
//                third.setValue("0.44");
//
//                fourth.setIsNew(0);
//                fourth.setName("dec");
//                fourth.setValue("2.79");
//                attList3.add(third);
//                attList3.add(fourth);
//                pulsar.setAttribute(attList3);
//                getService().saveNewVObject(x, pulsar, callback2);
//              }
//                VObject x2 = new VObject();
//                x2.setObjType(type);
//                //x2.setId(insertobj.getValueAsString("objname"));
//                x2.setClusterId(insertobj.getValueAsString("objname"));
//                x2.setName("NUOVO_OGGETTO_NAME");
//                x2.setSourceId("sabrina.checola@gmail.com");
//                x2.setType(1);
//                List attList2 = new ArrayList();
//                VObjectAttribute first2 = new VObjectAttribute();
//                VObjectAttribute second2 = new VObjectAttribute();
//                first2.setIsNew(0);
//                first2.setName("offset");
//                first2.setValue("0.44");
//                second2.setIsNew(0);
//                second2.setName("period");
//                second2.setValue("2.79");
//                attList2.add(first2);
//                attList2.add(second2);
//                x2.setAttribute(attList2);
//
//                getService().saveNewVObject(x2, callback3);

            }
        });
    }
}
