package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import org.vogc.client.AuthorService;
import org.vogc.client.AuthorServiceAsync;
import org.vogc.client.datatype.Author;
import org.vogc.client.gui.data.PartAuthorData;

/**
 *
 * @author Ettore
 */
public class ShowAuthors extends VLayout {

    public static AuthorServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(AuthorService.class);
    }
//    private Grid myGrid = new Grid(100, 3);
    final AsyncCallback<ArrayList<Author>> callback2 = new AsyncCallback<ArrayList<Author>>() {
        public void onFailure(Throwable caught) {
            SC.say("Communication failed");
        }

        public void onSuccess(ArrayList<Author> result) {


            Record[] data = PartAuthorData.getInstance().getRecords(result);

            final PartsAuthorListGrid myAuthorList = new PartsAuthorListGrid();
            myAuthorList.setHeight("100%");
            myAuthorList.setCanReorderRecords(true);
            myAuthorList.setShowAllRecords(true);

            //myList1.setFields(nameField,valueField);
            myAuthorList.setData(data);
            setBackgroundImage("background.jpg");

            addMember(myAuthorList);
        }
    };

    public ShowAuthors() {
        getService().ShowlistAuthors(callback2);
    }
}
