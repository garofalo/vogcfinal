package org.vogc.client.gui;

import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author EM
 */
public class MainPanel extends VLayout {

    private static MainPanel istanza = null;

    public static MainPanel getInstance() {

        if (istanza == null) {
            istanza = new MainPanel();

        }
        return istanza;
    }

    private MainPanel() {



        setWidth100(); // max width
        setHeight100();//max height

        // LAYOUT

        HLayout layerTop = new HLayout(2);// top layer
        layerTop.setHeight("102px");
        layerTop.setID("TOP");


        HLayout layerButton = new HLayout(2); //button layer
        layerButton.setHeight("5%");
        layerButton.setWidth("100%");
        layerButton.setBorder("1px solid grey");

        HLayout layerSearch = new HLayout(2);// search layer
        layerSearch.setHeight("5%");
        layerSearch.setID("TOP2");

        HLayout down = new HLayout(2); //tab layer
        down.setID("DOWN");
        down.setBorder("1px solid grey");


        // class

        TabPanel tab = new TabPanel();  // tab class

//        TopHeader imageAndWelcome = new TopHeader(); // image & link class
//        imageAndWelcome.setAlign(VerticalAlignment.TOP);
//        imageAndWelcome.setID("IMAGE");

        HLayout imageAndWelcome = new TopHeader();
        imageAndWelcome.setWidth100();
        imageAndWelcome.setHeight("102px");
        imageAndWelcome.setAlign(VerticalAlignment.TOP);


        ButtonPanel button = new ButtonPanel(tab); // button class

        SearchBasic search = new SearchBasic(tab); // normal search class
        search.setWidth("40%");
        

        SearchGeneral SearchAdv = new SearchGeneral(tab); //advanced search class
        SearchAdv.setWidth("30%");

        SearchSpecial spec = new SearchSpecial(tab);
        spec.setWidth("30%");





        layerTop.addMember(imageAndWelcome); // top layer with add image and link
        layerButton.addMember(button);   //layer with add button
        layerSearch.addMember(search);   //layer with add standard search on left
        layerSearch.addMember(SearchAdv); //layer with add advanced search on right
        layerSearch.addMember(spec);
        down.addMember(tab);           //layer with add of tab
        addMember(layerTop);// add layer top on VLAYOUT
        addMember(layerButton); //add button layer on VLAYOUT
        addMember(layerSearch); //add search layer on VLAYOUT
        addMember(down);// add Down layer to VLAYOUT
        tab.Start();// welcome tab call at start of web application
       
    }
}
