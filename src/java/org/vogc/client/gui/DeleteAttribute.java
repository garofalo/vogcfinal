package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.DeleteService;
import org.vogc.client.DeleteServiceAsync;
import org.vogc.client.datatype.ErrorReport;

/**
 *
 * @author EM
 */
public class DeleteAttribute extends HLayout {

    public static DeleteServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(DeleteService.class);
    }
    Label labelValue = new Label();
    Label labelAttribute = new Label();
    Label labelObject = new Label();
    private Label labelAttibute = new Label();
    private Button buttonAttribute = new Button("Delete attribute");
//    private Button buttonObject = new Button("Delete Object");
    String type;

    public DeleteAttribute() {

        setBackgroundImage("background.jpg");
        /**
         * *****************CANCELLA ATTRIBUTO**********************
         */
        final DynamicForm insertobj2 = new DynamicForm();

        labelAttibute.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>Delete Attibute</FONT></p></font>");
        labelAttibute.setShowEdges(true);
        labelAttibute.setHeight(5);
        final TextItem idObjectName2 = new TextItem("objectId2", "Object Name *");
        idObjectName2.setRequired(true);

        final TextItem valueName2 = new TextItem("attname2", "Attribute Name *");
        idObjectName2.setRequired(true);

        insertobj2.setFields(idObjectName2, valueName2);




        VLayout v1 = new VLayout();
        v1.addMember(labelAttibute);
        v1.addMember(insertobj2);
        v1.addMember(buttonAttribute);


        addMember(v1);

//        show();


        final AsyncCallback<ErrorReport> callback = new AsyncCallback<ErrorReport>() {
            public void onSuccess(ErrorReport result) {
                if (result.getCode().equals("vogcSRV0007")) {
                    SC.say("SUCCESS");
                } else if (result.getCode().equals("vogcSRV0006")) {

                    SC.warn("You aren't the author of this object you can't modify the value");
                } else {
                    SC.warn("Object Not Exist");
                }

            }

            public void onFailure(Throwable caught) {
                SC.say("ERRORE");
            }
        };


        buttonAttribute.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

//                int intval = Integer.parseInt(insertobj.getValueAsString("attname")) ;
//                getService().deleteAttributeValue("HARRIS2010",insertobj.getValueAsString("objectId"), intval, callback);
                Utility a = new Utility();
                getService().deleteAttribute(a.getUserId(), insertobj2.getValueAsString("objectId2"), insertobj2.getValueAsString("attname2"), callback);
            }
        });


    }
}
