package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import org.vogc.client.NewVObjectService;
import org.vogc.client.NewVObjectServiceAsync;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.VObject;
import org.vogc.client.datatype.VObjectAttribute;

/**
 * Example class using the NewVObjectService service.
 *
 * @author Sabrina
 */
public class InsertPulsar extends VLayout {

    public static NewVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(NewVObjectService.class);
    }
//    private Label lblServerReply = new Label();
//    private TextBox txtUserInput = new TextBox();
    // private Button btnSend = new Button("Send to server");
    private Button btnSend2 = new Button("New VOBject");
//    private Grid myGrid = new Grid(50, 50);
    public int type = 0;

    public InsertPulsar(final String s) {

        setBackgroundImage("background.jpg");
        final DynamicForm insertobj = new DynamicForm();

        final TextItem idObjectName = new TextItem("objectId", "Object Name *");
        idObjectName.setRequired(true);


        final TextItem offset = new TextItem("offsetvalue", "Offset *");
        offset.setRequired(true);
        final TextItem period = new TextItem("periodvalue", "Period *");
        period.setRequired(true);


        insertobj.setFields(idObjectName, offset, period);



        addMember(insertobj);


        addMember(btnSend2);



        final AsyncCallback<ErrorReport> callback2 = new AsyncCallback<ErrorReport>() {
            public void onSuccess(ErrorReport result) {
                GWT.log("FERMATI sono la callback2", null);
                // SC.warn("OK sono l callback2" + result.getMessage());
                SC.say("SUCCESS");


            }

            public void onFailure(Throwable caught) {
                SC.warn("Communication failed");
            }
        };



        btnSend2.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (insertobj.validate(true)) {
                    insertobj.saveData();
                    VObject x = new VObject();

                    x.setObjType(2);
                    x.setId(insertobj.getValueAsString("objectId").replaceAll(" ", ""));
                    x.setClusterId(s);

                    x.setName("test");
                    Utility a = new Utility();
                    x.setSourceId(a.getUserId());
                    x.setType(0);
                    ArrayList<VObjectAttribute> attList = new ArrayList<VObjectAttribute>(2);
                    VObjectAttribute first = new VObjectAttribute();
                    VObjectAttribute second = new VObjectAttribute();

                    first.setIsNew(0);
                    first.setName("Offset");
                    first.setValue(insertobj.getValueAsString("offsetvalue"));
                    second.setIsNew(0);
                    second.setName("Period");
                    second.setValue(insertobj.getValueAsString("periodvalue"));
                    attList.add(first);
                    attList.add(second);
                    x.setAttribute(attList);
                    // riempi oggetto
                    getService().saveNewVObject(x, null, callback2);
                }
            }
        });
    }
}
