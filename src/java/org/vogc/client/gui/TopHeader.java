package org.vogc.client.gui;

import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author EM
 */
public class TopHeader extends HLayout {

    // HeaderLink mf;
    private HeaderLink menuForm;

    public TopHeader() {

        Img logo = new Img("logo.png");
        logo.setWidth("330px");
        logo.setHeight("100px");

        VLayout leftLayout = new VLayout(15);
        leftLayout.setWidth("200px");
        leftLayout.addMember(logo);
        leftLayout.setBackgroundColor("#708090");

        addMember(leftLayout);

        menuForm = new HeaderLink();
        VLayout rightLayout = new VLayout(25);
        rightLayout.addMember(menuForm);
        rightLayout.setBackgroundColor("#708090");

        addMember(rightLayout);
    }
}