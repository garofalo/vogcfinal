package org.vogc.client.gui;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

/**
 *
 * @author Luca
 */
public class SearchImagebyObjectId extends VLayout {

    public SearchImagebyObjectId(final TabPanel p) {
        setBackgroundImage("background.jpg");
        setHeight("10%"); // altezza di tutto il panello pannello
        final DynamicForm imagesearchform = new DynamicForm();
        imagesearchform.setID("Biblioform");
        imagesearchform.setIsGroup(true);
        imagesearchform.setNumCols(8);
        imagesearchform.setGroupTitle("search Image by Object name ");
        imagesearchform.setWidth100();


        final TextItem authorField = new TextItem("valueA", "Object name");// campo dove inserire il nome

        ButtonItem ButtonByName = new ButtonItem();
        ButtonByName.setTitle("Biblio by Author");
        ButtonByName.setTooltip("Biblio search");
        ButtonByName.setRowSpan(0);
        // ButtonParameter3.setAlign(Alignment.LEFT);



        imagesearchform.setFields(authorField, ButtonByName);




        ButtonByName.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

                // grid.fetchData(form.getValuesAsCriteria());
                Tab tTab = new Tab("Object search");
                tTab.setCanClose(true);
                p.TabCategorySearch(tTab, p);


            }
        });



        VLayout verticals = new VLayout();

        verticals.addMember(imagesearchform);

        addMember(verticals);

    }
}