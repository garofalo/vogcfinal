package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import java.util.ArrayList;
import org.vogc.client.UserInfoService;
import org.vogc.client.UserInfoServiceAsync;
import org.vogc.client.datatype.User;
import org.vogc.client.gui.data.PartUserData;

/**
 *
 * @author Ettore
 */
public class ShowUserListNEW extends VLayout {

    public static UserInfoServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(UserInfoService.class);

    }
    TabPanel pane;
    final AsyncCallback<ArrayList<User>> callback2 = new AsyncCallback<ArrayList<User>>() {
        @Override
        public void onFailure(Throwable caught) {
            SC.say("Communication failed");
        }

        @Override
        public void onSuccess(ArrayList<User> result) {
//            User obj = null;
//            Iterator nrow = result.iterator();
//            int row = 0;
//            while (nrow.hasNext()) {
//                obj = (User) nrow.next();
//
//            }

            Record[] data = PartUserData.getInstance().getRecords(result);

            final PartsUserListGrid myUserList = new PartsUserListGrid();

            myUserList.setCanReorderRecords(true);
//                ListGridField nameField = new ListGridField("partClusterName", "NAME");
//                ListGridField valueField = new ListGridField("partClusterSrc", "VALUE");
//                myList1.setCanDragRecordsOut(true);
//                myList1.setCanReorderFields(true);
//                myList1.setDragDataAction(DragDataAction.MOVE);
            myUserList.setShowAllRecords(true);

            //myList1.setFields(nameField,valueField);
            myUserList.setData(data);
            setBackgroundImage("background.jpg");
            Label label = new Label();
            label.setHeight(5);
            label.setShowEdges(true);
            label.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>User List (click on the focused user to send him an e-mail)</FONT></p></font>");

            myUserList.addRecordClickHandler(new RecordClickHandler() {
                @Override
                public void onRecordClick(RecordClickEvent event) {
                    String userName = event.getRecord().getAttribute("partUserName");
//                    String url = "mailto:" + s;
//                    // + DataSession.getInstance().getSsid() + "&userMail="
//                    // + DataSession.getInstance().getEmail() + "&fileUri="
//
//                    Window.open(url, url, url);
                    String title = "New Attibute";
                    Tab tTab13 = new Tab(title, " hello ");
                    tTab13.setCanClose(true);

//                    p.TabUpdate(tTab13);
                    pane.TabUserProfile(tTab13, pane, userName);
                }
            });


            addMember(label);
            addMember(myUserList);
            // addMember(myList2);

            //addMember(rightImg);
            // addMember(leftImg);
            //addMember(myList2);
            // addMember(arrowImg);
//            show();
//            PartUserData.getInstance().emptyRec();
        }
    };

    public ShowUserListNEW(TabPanel p) {
        pane = p;
//          public ShowUserListNEW(){
        getService().getUsersList(callback2);

    }
}
