package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.Iterator;
import java.util.ArrayList;
import org.vogc.client.GetVObjectService;
import org.vogc.client.GetVObjectServiceAsync;
import org.vogc.client.datatype.AdditionalInfoAttribute;

/**
 *
 * @author Ettore
 */
public class HistoryPanel extends VLayout {

    public static GetVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(GetVObjectService.class);
    }
//    private Label lblServerReply = new Label();
//    private TextBox txtUserInput = new TextBox();
//    private Button btnSend = new Button("GET USER INFO");
//    private Button btnUsers = new Button("GET USERS LIST");
    private Grid myGrid = new Grid(100, 3);

    public HistoryPanel(String Type, String Obj) {

        final AsyncCallback<ArrayList<AdditionalInfoAttribute>> callback2 = new AsyncCallback<ArrayList<AdditionalInfoAttribute>>() {
            public void onFailure(Throwable caught) {
                SC.say("Communication failed");
            }

            public void onSuccess(ArrayList<AdditionalInfoAttribute> result) {
                myGrid.setText(0, 0, "Value");
                myGrid.setText(0, 1, "Author");
                myGrid.setText(0, 2, "Date");
                myGrid.setBorderWidth(1);


                Iterator nrow = result.iterator();
                int row = 1;

                while (nrow.hasNext()) {
                    AdditionalInfoAttribute usr = (AdditionalInfoAttribute) nrow.next();
                    myGrid.setText(row, 0,  usr.getIsFirst() + ", ");
                    myGrid.setText(row, 1, usr.getUserName() + ", ");
                    myGrid.setText(row, 2,  usr.getDate() + ", ");
                    row++;
                }
                myGrid.resizeRows(row);
            }
        };

        getService().getParameterHistory(Type, Obj, callback2);

        Label labelNewObject = new Label();
        labelNewObject.setHeight(10);
        labelNewObject.setShowEdges(true);
        labelNewObject.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>" + Type + " of " + Obj + "</FONT></p></font>");
        setBackgroundImage("background.jpg");
        addMember(labelNewObject);
        addMember(myGrid);

//        show();
        // Listen for the button clicks
//        btnSend.addClickHandler(new ClickHandler(){
//            public void onClick(ClickEvent event) {
//                // Make remote call. Control flow will continue immediately and later
//                // 'callback' will be invoked when the RPC completes.
//
//            }
//        });

//        btnUsers.addClickHandler(new ClickHandler(){
//            public void onClick(ClickEvent event) {
//                // Make remote call. Control flow will continue immediately and later
//                // 'callback' will be invoked when the RPC completes.
//                getService().getUsersList(callback2);
//            }
//        });
    }
}
