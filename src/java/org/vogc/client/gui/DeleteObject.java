package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.DeleteService;
import org.vogc.client.DeleteServiceAsync;
import org.vogc.client.datatype.ErrorReport;

/**
 *
 * @author EM
 */
public class DeleteObject extends HLayout {

    public static DeleteServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(DeleteService.class);
    }
    Label labelValue = new Label();
    Label labelAttribute = new Label();
    Label labelObject = new Label();
//    private Label labelAttibute = new Label();
//    private Button buttonAttribute = new Button("Delete Attribute");
    private Button buttonObject = new Button("Delete Object");
    String type;

    public DeleteObject() {

        setBackgroundImage("background.jpg");


        /**
         * *******************CANCELLA OGGETTO************************
         */
        final DynamicForm insertobj3 = new DynamicForm();

        labelObject.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>Delete Object</FONT></p></font>");
        labelObject.setShowEdges(true);
        labelObject.setHeight(5);
        final TextItem idObjectName3 = new TextItem("objectId3", "Object Name *");
        idObjectName3.setRequired(true);

        final SelectItem typesearch = new SelectItem();
        typesearch.setRequired(true);
        typesearch.setTitle("Type*");
        typesearch.setValueMap("Cluster", "Pulsar", "Star");
        typesearch.setDefaultValue("_______");
        typesearch.setWidth(80);

        typesearch.addChangedHandler(new ChangedHandler() {
            public void onChanged(ChangedEvent event) {
                String ds = (String) event.getValue();
                if (ds.equalsIgnoreCase("Cluster")) {
                    type = "1";
                } else if (ds.equalsIgnoreCase("Pulsar")) {
                    type = "2";
                } else if (ds.equalsIgnoreCase("Star")) {
                    type = "3";
                }


            }
        });
        insertobj3.setFields(idObjectName3, typesearch);



        VLayout v2 = new VLayout();
        v2.addMember(labelObject);
        v2.addMember(insertobj3);
        v2.addMember(buttonObject);


        addMember(v2);
//        show();


        final AsyncCallback<ErrorReport> callback = new AsyncCallback<ErrorReport>() {
            public void onSuccess(ErrorReport result) {
                if (result.getCode().equals("vogcSRV013")) {
                    SC.say("SUCCESS");
                } else {
                    if (result.getCode().equals("vogcSRV0006")) {

                        SC.warn("You aren't the author of this object you can't modify the value");
                    } else {
                        {
                            SC.warn("Object Not Exist");
                        }
                    }
                }
            }

            public void onFailure(Throwable caught) {
                SC.say("ERRORE");
            }
        };




        buttonObject.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

//                int intval = Integer.parseInt(insertobj.getValueAsString("attname")) ;
//                getService().deleteAttributeValue("HARRIS2010",insertobj.getValueAsString("objectId"), intval, callback);
                Utility a = new Utility();
                getService().deleteObject(a.getUserId(), type, insertobj3.getValueAsString("objectId3").replaceAll(" ", ""), callback);
            }
        });
    }
}
