package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;
import org.vogc.client.NewBiblioNoteService;
import org.vogc.client.NewBiblioNoteServiceAsync;
import org.vogc.client.datatype.ErrorReport;

/**
 *
 * @author Ettore
 */
public class NewAuthorPanel extends VLayout {

    public static NewBiblioNoteServiceAsync getBibloService() {


        return GWT.create(NewBiblioNoteService.class);
    }

    public NewAuthorPanel() {

        final DynamicForm newAuthor = new DynamicForm();

        final TextItem idAuthors = new TextItem("idNewAuthors", "id Authors *");
        idAuthors.setRequired(true);

        final TextItem uriAuthors = new TextItem("uriAuth", "uri of authors");
        uriAuthors.setDefaultValue("null");

        newAuthor.setFields(idAuthors, uriAuthors);
        final AsyncCallback<ErrorReport> biblioCallback = new AsyncCallback<ErrorReport>() {
            public void onFailure(Throwable caught) {
                // throw new UnsupportedOperationException("Not supported yet.");
                // SC.say("FAILED");
                SC.warn("server side failure " + caught);
            }

            public void onSuccess(ErrorReport result) {
                // throw new UnsupportedOperationException("Not supported yet.");
                // SC.say("OK");

                //SC.warn("OK:" + result.getCode() + "    " + result.getMessage() + "  " + result.getAdditionalInfo());
                SC.say("SUCCESS");
            }
        };

        IButton saveAuthors = new IButton("new authors");
        saveAuthors.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (newAuthor.validate(true)) {
                    newAuthor.saveData();

                    String name = newAuthor.getValueAsString("idNewAuthors").replaceAll(" ", "");
                    String uri = newAuthor.getValueAsString("uriAuth").replaceAll(" ", "");

                    getBibloService().saveNewAuthor(name, uri, biblioCallback);
                }



            }
        });

        addMember(newAuthor);
        addMember(saveAuthors);
    }
}
