package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import java.util.Iterator;
import org.vogc.client.UserInfoService;
import org.vogc.client.UserInfoServiceAsync;
import org.vogc.client.datatype.User;

/**
 *
 * @author EM
 */
public class ShowUserList extends VLayout {

    public static UserInfoServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(UserInfoService.class);
    }
    private Label lblServerReply = new Label();
//    private TextBox txtUserInput = new TextBox();
//    private Button btnSend = new Button("GET USER INFO");
//    private Button btnUsers = new Button("GET USERS LIST");
    private Grid myGrid = new Grid(100, 1);

    public ShowUserList() {
        addMember(new Label("User List: "));
        setBackgroundImage("background.jpg");

        //addMember(myGrid);

        // Create an asynchronous callback to handle the result.
        final AsyncCallback<User> callback = new AsyncCallback<User>() {
            public void onSuccess(User result) {
                if (result.getReport() == null) {
                    lblServerReply.setText("Name = " + result.getName() + " Surname= " + result.getSurname() + " Motivations= " + result.getMotivations());
                } else {
                    lblServerReply.setText("Error: "+ result.getReport().getMessage());
                }
            }

            public void onFailure(Throwable caught) {
                lblServerReply.setText("Communication failed");
            }
        };

        final AsyncCallback<ArrayList<User>> callback2 = new AsyncCallback<ArrayList<User>>() {
            public void onFailure(Throwable caught) {
                lblServerReply.setText("Communication failed");
            }

            public void onSuccess(ArrayList<User> result) {
                myGrid.setBorderWidth(1);
                myGrid.resizeColumns(1);

                Iterator nrow = result.iterator();
                int row = 0;
                lblServerReply.setText("Users List: \n");
                while (nrow.hasNext()) {
                    User usr = (User) nrow.next();
                    myGrid.setText(row, 0, usr.getUserId() + ", " + usr.getName() + ", " + usr.getSurname());
                    row++;
                }
                myGrid.resizeRows(row);
            }
        };
        getService().getUserInfo("*", callback);
        getService().getUsersList(callback2);

        addMember(myGrid);
        show();
        // Listen for the button clicks
//        btnSend.addClickHandler(new ClickHandler(){
//            public void onClick(ClickEvent event) {
//                // Make remote call. Control flow will continue immediately and later
//                // 'callback' will be invoked when the RPC completes.
//
//            }
//        });

//        btnUsers.addClickHandler(new ClickHandler(){
//            public void onClick(ClickEvent event) {
//                // Make remote call. Control flow will continue immediately and later
//                // 'callback' will be invoked when the RPC completes.
//                getService().getUsersList(callback2);
//            }
//        });
    }
}
