package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import java.util.ArrayList;
import org.vogc.client.GetVObjectService;
import org.vogc.client.GetVObjectServiceAsync;
import org.vogc.client.datatype.PulsarInfo;
import org.vogc.client.gui.data.PartPulsarData;

/**
 *
 * @author EM
 */
public class InfoPulsarPanel extends HLayout {

    public static ArrayList<String> listaRicerca = new ArrayList<String>(10);

    public static GetVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(GetVObjectService.class);
    }
    public String s = null;
    public TabPanel Pan;

    public InfoPulsarPanel(final TabPanel p, String s) {

        Pan = p;
        final AsyncCallback<PulsarInfo> callback = new AsyncCallback<PulsarInfo>() {
            public void onSuccess(PulsarInfo result) {

                if (result.getReport() == null) {



                    Record[] data = PartPulsarData.getInstance().getRecords(result.getPlsAtt());


                    final PartsClusterListGrid myList1 = new PartsClusterListGrid();


//                ListGridField nameField = new ListGridField("partClusterName", "NAME");
//                ListGridField valueField = new ListGridField("partClusterSrc", "VALUE");
//                myList1.setCanDragRecordsOut(true);
//                myList1.setCanReorderFields(true);
//                myList1.setDragDataAction(DragDataAction.MOVE);
                    myList1.setShowAllRecords(true);
                    myList1.setCanResizeFields(true);
                    //myList1.setFields(nameField,valueField);
                    myList1.setData(data);


                    final PartsClusterListGrid myList2 = new PartsClusterListGrid();
                    myList2.setCanDragRecordsOut(true);
                    myList2.setCanAcceptDroppedRecords(true);
                    myList2.setCanReorderRecords(true);

                    Label labelNewObject = new Label();
                    labelNewObject.setHeight(10);
                    labelNewObject.setShowEdges(true);
                    labelNewObject.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>" + result.getId().toUpperCase() + "</FONT></p></font>");

                    addMember(labelNewObject);


                    addMember(myList1);
                    //addMember(rightImg);
                    // addMember(leftImg);
                    //addMember(myList2);
                    // addMember(arrowImg);
//                show();
//                PartPulsarData.getInstance().emptyRec();


                } else {
                    SC.warn("Search Result: ERROR");
                }
            }

            public void onFailure(Throwable caught) {
                //  SC.warn("server side failure " + caught);
                SC.warn("OBJECT NOT FOUND");
                // SC.warn("OBJECT NOT FOUND" + caught.getMessage() + caught.getLocalizedMessage());
            }
        };

        getService().getPulsar(s, callback);
    }
}
