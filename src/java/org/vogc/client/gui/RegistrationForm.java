package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.HeaderItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;
import com.smartgwt.client.widgets.form.validator.MatchesFieldValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.layout.HLayout;
import org.vogc.client.LoginRegistrationService;
import org.vogc.client.LoginRegistrationServiceAsync;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.User;
//import org.vogc.client.LoginRegistrationService;
//import org.vogc.client.LoginRegistrationServiceAsync;

/**
 *
 * @author Mauro Garofalo
 */
public class RegistrationForm extends HLayout {

    private static RegistrationForm instance = null;

    public static RegistrationForm getInstance() {
        if (instance == null) {
            instance = new RegistrationForm();
        }
        return instance;
    }

    public static LoginRegistrationServiceAsync getRegistrationService() {
        return GWT.create(LoginRegistrationService.class);
    }
    private ButtonItem send;
    private User user;
    private DynamicForm regForm;

    private RegistrationForm() {
        setMargin(10);
        setWidth100();
        setHeight100();

        regForm = new DynamicForm();
        regForm.setID("_RegistrationForm");
        regForm.setWidth("400px");
        regForm.setHeight100();

        HeaderItem header = new HeaderItem();
        header.setDefaultValue("VOGCLUSTERS Registration Form");

        TextItem name = new TextItem("name", "Name");
        name.setRequired(true);

        TextItem surname = new TextItem("surname", "Family Name");
        surname.setRequired(true);

        RegExpValidator mailRegExp = new RegExpValidator();
        mailRegExp.setExpression("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
        mailRegExp.setErrorMessage("Use a valid email address");

        MatchesFieldValidator mailValidator = new MatchesFieldValidator();
        mailValidator.setOtherField("confirmMail");
        mailValidator.setErrorMessage("Email do not match");

        TextItem mail = new TextItem("userMail", "Email");
        mail.setRequired(true);
        mail.setValidators(mailRegExp, mailValidator);
        mail.setHint("Use a valid address");


        TextItem confirmEmail = new TextItem("confirmMail", "Confirm Email");
        confirmEmail.setRequired(true);

        TextItem country = new TextItem("country", "Country");
        country.setRequired(true);

        TextItem affiliation = new TextItem("affiliation", "Affiliation");
        affiliation.setRequired(true);

        MatchesFieldValidator validator = new MatchesFieldValidator();
        validator.setOtherField("password2");
        validator.setErrorMessage("Passwords do not match");
        LengthRangeValidator pwdLength = new LengthRangeValidator();
        pwdLength.setMin(6);
        pwdLength.setMax(16);
        pwdLength.setErrorMessage("Password length not allowed");


        PasswordItem password = new PasswordItem("password", "Password");
        password.setRequired(true);
        password.setValidators(validator, pwdLength);
        password.setHint("Allowed range [6,16] characters");

        PasswordItem confirmPwd = new PasswordItem("password2", "Confirm Password");
        confirmPwd.setRequired(true);
        final AsyncCallback<ErrorReport> callback = new AsyncCallback<ErrorReport>() {
            @Override
            public void onSuccess(ErrorReport result) {
                if (result.getCode().equals("vogcSRV0000")) {
                    final BooleanCallback ok = new BooleanCallback() {
                        @Override
                        public void execute(Boolean value) {
                            Scheduler.get().scheduleDeferred(new ScheduledCommand() {
                                @Override
                                public void execute() {
                                    RegistrationWindow.getInstance().hide();
                                    regForm.clearValues();
                                }
                            });
                        }
                    };

                    SC.say("Registration Submitted.<br />"
                            + "Please wait for confirmation email.", ok);


                } else {
                    SC.warn("USER NAME ALREDY EXIST");
                }
            }

            @Override
            public void onFailure(Throwable caught) {
                SC.warn("GENERAL COMMUNICATION FAILURE!<br /> INTERNAL CODE:" + caught.getMessage() + " RegForm");
            }
        };

        send = new ButtonItem();
        send.setTitle("Submit");
        send.setAlign(Alignment.CENTER);
        send.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (regForm.validate()) {
                    user = new User();
                    user.setName(regForm.getValueAsString("name"));
                    user.setSurname(regForm.getValueAsString("surname"));
                    user.setUserMail(regForm.getValueAsString("userMail"));
                    user.setCountry(regForm.getValueAsString("country"));
                    user.setAffiliation(regForm.getValueAsString("affiliation"));
                    user.setPassword(regForm.getValueAsString("password"));

                    getRegistrationService().registration(user, callback);


                } else {
                    SC.warn("Error on field");
                }

            }
        });
        regForm.setFields(header, name, surname, mail, confirmEmail, country, affiliation, password, confirmPwd, send);

        // regForm.setFields(header, name, surname, mail, confirmEmail, country, affiliation, password ,send);

        addMember(regForm);
    }
}
