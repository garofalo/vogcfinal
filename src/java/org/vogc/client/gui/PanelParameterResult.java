package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import java.util.Iterator;
import org.vogc.client.GetVObjectService;
import org.vogc.client.GetVObjectServiceAsync;
import org.vogc.client.datatype.VObject;

/**
 *
 * @author Ettore
 */
public class PanelParameterResult extends VLayout {

    public static GetVObjectServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(GetVObjectService.class);
    }
    private Label lblServerReply = new Label();
    private Grid myGrid = new Grid(200, 1);

    public PanelParameterResult(String parameter, String Value, String operator) {


        setBackgroundImage("background.jpg");

        final AsyncCallback<ArrayList<VObject>> callback2 = new AsyncCallback<ArrayList<VObject>>() {
            public void onSuccess(ArrayList<VObject> result) {
                myGrid.setBorderWidth(1);
                myGrid.resizeColumns(1);

                lblServerReply.setText("OK" + result.size());
                Iterator nrow = result.iterator();
                int row = 0;
                while (nrow.hasNext()) {
                    VObject obj = (VObject) nrow.next();

                    myGrid.setText(row, 0, " " + obj.getId());
                    row++;
                }
                myGrid.resizeRows(row);
            }

            public void onFailure(Throwable caught) {
                lblServerReply.setText("Communication failed");

            }
        };



        getService().getGclusterByParameters(parameter, Value, operator, callback2);


        addMember(myGrid);
//        show();
    }
}
