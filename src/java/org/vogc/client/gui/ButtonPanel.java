package org.vogc.client.gui;

/**
 *
 * @author EM
 */
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class ButtonPanel extends HLayout {

    //final TabPanel p = new TabPanel();
    public ButtonPanel(final TabPanel p) {
        setBackgroundImage("background.jpg");
        final String hello = " hello ";

        HLayout buttons = new HLayout();

        IButton buttonNewObject = new IButton("New Object"); // creato bottone
        buttonNewObject.setWidth100();
        buttonNewObject.setID("BUTTONNEWOBJECT"); // id del bottone
        buttonNewObject.setTooltip("insert new object"); // tooltip del bottone
        buttonNewObject.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) { // funzione click
                String title = "New Object"; // titolo
                Tab tTab1 = new Tab(title, hello); // creazione della tab
                tTab1.setCanClose(true); // la tab si pu chiudere
                p.TabInsertNewObject(tTab1); // richiama la tab

            }
        });

        IButton buttonNewBiblio = new IButton("New Biblio");
        buttonNewBiblio.setWidth100();
        buttonNewBiblio.setTooltip("insert new Biblio note");
        buttonNewBiblio.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String title = "New biblio";
                Tab tTab2 = new Tab(title, hello);
                tTab2.setCanClose(true);
                p.TabBiblio(tTab2);

            }
        });



        IButton buttonUserList = new IButton("User List");
        buttonUserList.setWidth100();
        buttonUserList.setTooltip("user list");
        buttonUserList.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                // Utilita a = new Utilita();
                // String userId = a.getUserId();
                //   if (userId.equals("admin"))
                //  {
                Tab tTab7 = new Tab("User List");
                tTab7.setCanClose(true);
                p.TabUserList(tTab7, p);

                //   }
                //   else
                // SC.say("You are logged as guest and you can't see userlist. Please login as admin");
            }
        });


        IButton buttonAbout = new IButton("About");
        buttonAbout.setWidth100();
        buttonAbout.setTooltip("About");
        buttonAbout.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                Tab tTab8 = new Tab("About");
                tTab8.setCanClose(true);
                p.TabAbout(tTab8);


            }
        });

        IButton buttonNewNote = new IButton("New Note");
        buttonNewNote.setWidth100();
        buttonNewNote.setTooltip("insert new  note");
        buttonNewNote.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String title = "New Note";
                Tab tTab9 = new Tab(title, hello);
                tTab9.setCanClose(true);
                p.TabNote(tTab9);


            }
        });

        IButton buttonNewAttribute = new IButton("New Attribute");
        buttonNewAttribute.setWidth100();
        buttonNewAttribute.setTooltip("insert new Attribute");
        buttonNewAttribute.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String title = "New Attibute";
                Tab tTab10 = new Tab(title, hello);
                tTab10.setCanClose(true);
                p.TabNewAttribute(tTab10);

            }
        });


        IButton buttonDeleteObject = new IButton("Delete Object");
        buttonDeleteObject.setWidth100();
        buttonDeleteObject.setTooltip("delete object");
        buttonDeleteObject.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String title = "Delete Object";
                Tab tTab11 = new Tab(title, hello);
                tTab11.setCanClose(true);
                p.TabDeleteObj(tTab11);

            }
        });
        IButton buttonDeleteAttribute = new IButton("Delete Attribute");
        buttonDeleteAttribute.setWidth100();
        buttonDeleteAttribute.setTooltip("delete attribute");
        buttonDeleteAttribute.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String title = "Delete Attribute";
                Tab tTab11bis = new Tab(title, hello);
                tTab11bis.setCanClose(true);
                p.TabDeleteAtt(tTab11bis);

            }
        });


        IButton buttonUpdateObject = new IButton("Update Object");
        buttonUpdateObject.setWidth100();
        buttonUpdateObject.setTooltip("update object");
        buttonUpdateObject.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String title = "Update Object";
                Tab tTab12 = new Tab(title, hello);
                tTab12.setCanClose(true);
                p.TabUpdate(tTab12);

            }
        });

        IButton buttonUserField = new IButton("Show my Doc");
        buttonUserField.setWidth100();
        buttonUserField.setTooltip("Show my Doc");
        buttonUserField.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String title = "My Documents";
                Tab tTab13 = new Tab(title, hello);
                tTab13.setCanClose(true);
                //p.TabUpdate(tTab13);
                Utility a = new Utility();

                p.TabUserProfile(tTab13, p, a.getUserId());

            }
        });



        //PlusOneButton.setStyleName("exampleTextBlock");
        String contents = " ";
        HTMLFlow PlusOneButton = new HTMLFlow(contents);
        PlusOneButton.setContents(contents);




        buttons.addMember(buttonNewObject);
        buttons.addMember(buttonUpdateObject);
        buttons.addMember(buttonDeleteObject);
        buttons.addMember(buttonDeleteAttribute);
        buttons.addMember(buttonNewAttribute);
        buttons.addMember(buttonNewBiblio);
        buttons.addMember(buttonNewNote);
        buttons.addMember(buttonUserList);
        buttons.addMember(buttonUserField);
        buttons.addMember(buttonAbout);
        // buttons.addMember(PlusOneButton);

        setHeight100();
        setWidth100();
        addMember(buttons);

    }
}
