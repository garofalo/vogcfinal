package org.vogc.client.gui;

/**
 *
 * @author EM
 */
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.Window;

/**
 *
 * @author Mauro Garofalo
 */
public class RegistrationWindow extends Window {

    private static RegistrationWindow instance = null;

    public static RegistrationWindow getInstance() {
        if (instance == null) {
            instance = new RegistrationWindow();
        }
        return instance;
    }

    public RegistrationWindow() {
        setIsModal(true);
        setDismissOnEscape(true);
        setModalMaskOpacity(50);
        setTitle("VOGCLUSTERS Registration Window");
        setCanDragResize(false);
        setSize("500", "400");
        setLeft("200px");
        setTop("100px");
        setAlign(Alignment.CENTER);
        setAlign(VerticalAlignment.CENTER);
        addItem(RegistrationForm.getInstance());
    }
}
