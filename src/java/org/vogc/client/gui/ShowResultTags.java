package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tab.Tab;
import java.util.ArrayList;
import org.vogc.client.SearchService;
import org.vogc.client.SearchServiceAsync;
import org.vogc.client.datatype.SearchResult;
import org.vogc.client.gui.data.PartData;

/**
 *
 * @author EM
 */
public class ShowResultTags extends HLayout {

    public static ArrayList<String> listaRicerca = new ArrayList<String>(10);

    public static SearchServiceAsync doSearch() {

        return GWT.create(SearchService.class);



    }
    public String s = null;
    public TabPanel Pan;
    final AsyncCallback<SearchResult> searchCallback = new AsyncCallback<SearchResult>() {
        @Override
        public void onSuccess(SearchResult result) {

            if (result.getReport() == null) {
                Record[] data = PartData.getInstance().getRecords(result.getTags());

                // Record[] data = PartData.getRec();
                //SC.say("data lenght" + data.length);
                final PartsListGrid myList1 = new PartsListGrid();
                // myList1.setEmptyMessage("No Records");
                //  ListGridField employeeIdField = new ListGridField("TAG");

//                ListGridField nameField = new ListGridField("Name");
                myList1.setCanDragRecordsOut(true);
                myList1.setCanReorderFields(true);
                myList1.setDragDataAction(DragDataAction.MOVE);
                // myList1.setFields("name");
                myList1.setData(data);


                final PartsListGrid myList2 = new PartsListGrid();
                myList2.setCanDragRecordsOut(true);
                myList2.setCanAcceptDroppedRecords(true);
                myList2.setCanReorderRecords(true);

//                TransferImgButton rightImg = new TransferImgButton(TransferImgButton.RIGHT);
//                rightImg.addClickHandler(new ClickHandler() {
//
//                    public void onClick(ClickEvent event) {
//                        //   myList2.transferSelectedData(myList1);
//
//                        Tab tTab2 = new Tab("OBJECT");
//                        tTab2.setCanClose(true);
//                        Pan.TabObject(tTab2);
////                        Record[] data2 = myList1.getDragData();
////                        TempSearchObject tmp = new TempSearchObject();
////                        tmp.setRec(data2);
//
//
//
//                    }
//                });
//
//
//                TransferImgButton leftImg = new TransferImgButton(TransferImgButton.LEFT);
//                leftImg.addClickHandler(new ClickHandler() {
//
//                    public void onClick(ClickEvent event) {
//                        myList1.transferSelectedData(myList2);
//                    }
//                });

                //hStack.addMember(myList1);
                myList1.addRecordClickHandler(new RecordClickHandler() {
                    @Override
                    public void onRecordClick(RecordClickEvent event) {

                        String s = event.getRecord().getAttribute("partName");
                        // String s = myList1.getDragData().toString();
                        // String s = event.getRecord();
                        String title = s;
                        Tab tTab = new Tab(title, " hello ");
                        tTab.setCanClose(true);

                        Pan.TabNameSearch(tTab, s, 1, Pan);


                        //SC.warn("ciao" + s);
                    }
                });



                addMember(myList1);


            } else {
                SC.warn("Search Result: ERROR");
            }
        }

        @Override
        public void onFailure(Throwable caught) {
            //  SC.warn("server side failure " + caught);
            SC.warn("OBJECT NOT FOUND");
            // SC.warn("OBJECT NOT FOUND" + caught.getMessage() + caught.getLocalizedMessage());
        }
    };

    public ShowResultTags(final TabPanel p, String s) {

        Pan = p;
        // toptabset.animateHide(AnimationEffect.SLIDE);

        doSearch().search(s, searchCallback);



    }

    public void setS(String s) {
        this.s = s;
    }
}
