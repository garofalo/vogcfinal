package org.vogc.client.gui;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import org.vogc.client.gui.data.FormFieldSearch;

/**
 *
 * @author EM
 */
public class SearchGeneral extends VLayout {

    //final TabPanel p = new TabPanel();
    public SearchGeneral(final TabPanel p) {
        setBackgroundImage("background.jpg");
        VLayout principale = new VLayout();


        // pulsanti per ricerca avanzata e per mostrare tutto il DB
        DynamicForm advancedForm = new DynamicForm();
        advancedForm.setIsGroup(true);
        advancedForm.setGroupTitle("Advanced Search");
        advancedForm.setNumCols(6);
        advancedForm.setDataSource(FormFieldSearch.getInstance());
        advancedForm.setAutoFocus(false);
        advancedForm.setWidth100();



        ButtonItem buttonAdvanced = new ButtonItem();//bottone avanzato

        buttonAdvanced.setTitle("Advanced");
        buttonAdvanced.setTooltip("click here for advanced search");
        buttonAdvanced.setRowSpan(0);
        buttonAdvanced.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String title = "Advanced";
                Tab tTab = new Tab(title, " hello ");
                tTab.setCanClose(true);
                p.TabAdvancedSearch(tTab, p);
            }
        });

        ButtonItem buttonAllDatabase = new ButtonItem();// bottone per visualizzare tutto il database
        buttonAllDatabase.setTitle("Show all");
        buttonAllDatabase.setTooltip("click here show all Database");
        buttonAllDatabase.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                String title = "Complete";
                Tab tTab = new Tab(title, " hello ");
                tTab.setCanClose(true);
                p.TabCompleteSearch(tTab, p);

            }
        });
        advancedForm.setFields(buttonAdvanced, buttonAllDatabase);

        /**
         * ************************TAG SEARCH*****************************
         */
        final DynamicForm tagSearchForm = new DynamicForm();
        tagSearchForm.setID("tagform");
        tagSearchForm.setIsGroup(true);
        tagSearchForm.setGroupTitle("Generic/Partial Chars ");
        tagSearchForm.setWidth100();
        tagSearchForm.setHeight("50%");

        final TextItem nametagField = new TextItem("val", "Value");// campo dove inserire il nome
        nametagField.setTooltip("insert chars");
        ButtonItem ButtonParameter4 = new ButtonItem(); //pulsante

        ButtonParameter4.setTitle("Search");
        ButtonParameter4.setTooltip("Search for chars ");
        ButtonParameter4.setRowSpan(0);



        tagSearchForm.setFields(nametagField, ButtonParameter4);




        ButtonParameter4.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                //nameSearchForm.validate();
                tagSearchForm.saveData();

                String s = tagSearchForm.getValueAsString("val");
                // doSearch().search(s, searchCallback);
                String title = "search tag";
                Tab tTab = new Tab(title, " hello ");
                tTab.setCanClose(true);
                p.TabTagSearch(tTab, p, s);




            }
        });
        principale.addMember(tagSearchForm);
        principale.addMember(advancedForm);

        addMember(principale);

    }
}