package org.vogc.client.gui;

import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

/**
 *
 * @author EM
 */
public class SearchSpecial extends VLayout {

    public SearchSpecial(final TabPanel p) {
        setBackgroundImage("background.jpg");
        setHeight("10%"); // altezza di tutto il panello pannello
        /**
         * ************************BIBLIO NOTE BY AUTHOR SEARCH*****************************
         */
        final DynamicForm authorNameSearchForm = new DynamicForm();
        authorNameSearchForm.setID("Biblioform");
        authorNameSearchForm.setIsGroup(true);
        authorNameSearchForm.setNumCols(8);
        authorNameSearchForm.setGroupTitle("search biblio Notes by Author ");
        authorNameSearchForm.setWidth100();


        final TextItem authorField = new TextItem("valueA", "Author");// campo dove inserire il nome
        ButtonItem ButtonBiblioByAuthor = new ButtonItem(); //pulsante

        ButtonBiblioByAuthor.setTitle("Biblio by Author");
        ButtonBiblioByAuthor.setTooltip("Biblio search");
        ButtonBiblioByAuthor.setRowSpan(0);
        // ButtonParameter3.setAlign(Alignment.LEFT);



        authorNameSearchForm.setFields(authorField, ButtonBiblioByAuthor);




        ButtonBiblioByAuthor.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

                // grid.fetchData(form.getValuesAsCriteria());
                Tab tTab = new Tab("Biblio search");
                tTab.setCanClose(true);
                p.TabCategorySearch(tTab, p);
                SC.say("Sorry! Feature available in next release");

            }
        });

        /**
         * ************************BIBLIO NOTE BY Object SEARCH*****************************
         */
        final DynamicForm biblioobjectSearchForm = new DynamicForm();
        biblioobjectSearchForm.setID("BiblioformObject");
        biblioobjectSearchForm.setIsGroup(true);
        biblioobjectSearchForm.setNumCols(8);
        biblioobjectSearchForm.setGroupTitle("search biblio Notes by Object");
        biblioobjectSearchForm.setWidth100();


        final TextItem objectField = new TextItem("valueO", "Object");// campo dove inserire il nome
        ButtonItem ButtonBiblioByObject = new ButtonItem(); //pulsante

        ButtonBiblioByObject.setTitle("Biblio by Object");
        ButtonBiblioByObject.setTooltip("Biblio search by Object");
        ButtonBiblioByObject.setRowSpan(0);
        // ButtonParameter3.setAlign(Alignment.LEFT);



        biblioobjectSearchForm.setFields(objectField, ButtonBiblioByObject);




        ButtonBiblioByObject.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                // grid.fetchData(form.getValuesAsCriteria());
                Tab tTab = new Tab("Biblio search");
                tTab.setCanClose(true);
                p.TabCategorySearch(tTab, p);
                SC.say("Sorry! Feature available in next release");


            }
        });

        VLayout verticals = new VLayout();

        verticals.addMember(authorNameSearchForm);
        verticals.addMember(biblioobjectSearchForm);
        addMember(verticals);
        show();

    }
}
///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.vogc.client.gui;
//
//import com.smartgwt.client.types.OperatorId;
//import com.smartgwt.client.util.SC;
//import com.smartgwt.client.widgets.form.fields.ButtonItem;
//import com.smartgwt.client.widgets.form.fields.TextItem;
//import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
//import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
//import com.smartgwt.client.widgets.form.DynamicForm;
//import com.smartgwt.client.widgets.form.events.ItemChangedEvent;
//import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
//import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
//import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
//import com.smartgwt.client.widgets.form.fields.SelectItem;
//import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
//import com.smartgwt.client.widgets.form.fields.events.ItemHoverEvent;
//import com.smartgwt.client.widgets.form.fields.events.ItemHoverHandler;
//import com.smartgwt.client.widgets.layout.HLayout;
//import com.smartgwt.client.widgets.layout.VLayout;
//import com.smartgwt.client.widgets.tab.Tab;
//import java.util.ArrayList;
//import java.util.ArrayList;
//import org.vogc.client.gui.data.FormFieldSearch;
//
///**
// *
// * @author EM
// */
//public class SearchSpecial extends VLayout {
//
//    public SearchSpecial(final TabPanel p) {
//
//        setHeight("10%"); // altezza di tutto il panello pannello
//        /**************************NAME SEARCH******************************/
//        final DynamicForm authorNameSearchForm = new DynamicForm();
//        authorNameSearchForm.setID("Biblioform");
//        authorNameSearchForm.setIsGroup(true);
//        authorNameSearchForm.setNumCols(8);
//        authorNameSearchForm.setGroupTitle("Biblio Notes by Author ");
//        authorNameSearchForm.setWidth100();
//        authorNameSearchForm.setHeight("100%");
//
//
//        final TextItem authorField = new TextItem("value", "AUTHOR");// campo dove inserire il nome
//        ButtonItem ButtonBiblioByAuthor = new ButtonItem(); //pulsante
//
//        ButtonBiblioByAuthor.setTitle("BiblioSearch");
//        ButtonBiblioByAuthor.setTooltip("Biblio search");
//        ButtonBiblioByAuthor.setRowSpan(0);
//        // ButtonParameter3.setAlign(Alignment.LEFT);
//
//
//
//        authorNameSearchForm.setFields(authorField, ButtonBiblioByAuthor);
//
//
//
//
//        ButtonBiblioByAuthor.addClickHandler(new ClickHandler() {
//
//            public void onClick(ClickEvent event) {
////                authorNameSearchForm.validate();
////                authorNameSearchForm.saveData();
////
////                String s = null;
//
////                s = authorNameSearchForm.getValueAsString("val");
////
////
////                String title = s;
////                Tab tTab = new Tab(title, " hello ");
////                tTab.setCanClose(true);
//
//            }
//        });
//
//
//        HLayout orizz = new HLayout();
//        orizz.setHeight("20%");
//        addMember(orizz);
//        orizz.addMember(authorNameSearchForm);
//        show();
//
//    }
//}
