package org.vogc.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import org.vogc.client.UserInfoService;
import org.vogc.client.UserInfoServiceAsync;
import org.vogc.client.datatype.User;
import org.vogc.client.gui.data.PartUserData;

/**
 *
 * @author Luca
 */
public class UserProfilePanel extends HLayout {

    public static UserInfoServiceAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(UserInfoService.class);


    }
    private String sourceId;
    private TabPanel pane;
    final AsyncCallback<ArrayList<User>> callback2 = new AsyncCallback<ArrayList<User>>() {
        @Override
        public void onFailure(Throwable caught) {
            SC.say("Communication failed");
        }

        @Override
        public void onSuccess(ArrayList<User> result) {
           
            Record[] data = PartUserData.getInstance().getRecords(result);

            final PartsUserListGrid myUserList = new PartsUserListGrid();

            myUserList.setCanReorderRecords(true);
//                ListGridField nameField = new ListGridField("partClusterName", "NAME");
//                ListGridField valueField = new ListGridField("partClusterSrc", "VALUE");
//                myList1.setCanDragRecordsOut(true);
//                myList1.setCanReorderFields(true);
//                myList1.setDragDataAction(DragDataAction.MOVE);
            myUserList.setShowAllRecords(true);

            //myList1.setFields(nameField,valueField);
            myUserList.setData(data);
            setBackgroundImage("background.jpg");
            Label label = new Label();
            label.setHeight(5);
            label.setShowEdges(true);
            label.setContents("<font color='#708090'><p align='center'><FONT SIZE='5'>User List (click on the focused user to send him an e-mail)</FONT></p></font>");



            VLayout cluster = new VLayout();
            cluster.setBackgroundColor("black");
            cluster.addMember(new ShowResultTags(pane, sourceId));
            VLayout star = new VLayout();
            VLayout pulsar = new VLayout();
            pulsar.setBackgroundColor("gray");
            VLayout bnotes = new VLayout();
            VLayout images = new VLayout();
            images.setBackgroundColor("yellow");
            addMember(label);
            addMember(cluster);
            addMember(star);
            addMember(pulsar);
            addMember(bnotes);
            addMember(images);

//            show();
//            PartUserData.getInstance().emptyRec();
        }
    };

    public UserProfilePanel(String userId, TabPanel p) {
        sourceId = userId;
        pane = p;

        getService().getUsersList(callback2);


    }
}
