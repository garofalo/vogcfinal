package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import org.vogc.client.datatype.AdditionalInfoAttribute;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.Note;

/**
 *
 * @author Sabrina
 */
@RemoteServiceRelativePath("updateservice")
public interface UpdateService extends RemoteService {

    public ErrorReport updateAttribute(AdditionalInfoAttribute attValues);

    public ErrorReport updateAttributeValue(String userId, int objectType, int objectHasAttId, String value, String name, String attribute);

    public ErrorReport updateBiblioRef(BiblioReference bibRef);

    public ErrorReport updateNote(Note note);
}
