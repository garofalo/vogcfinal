package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import org.vogc.client.datatype.AdditionalInfoAttribute;
import org.vogc.client.datatype.Catalogue;
import org.vogc.client.datatype.GClusterInfo;
import org.vogc.client.datatype.PulsarInfo;
import org.vogc.client.datatype.StarInfo;
import org.vogc.client.datatype.VObject;

/**
 *
 * @author Sabrina
 */
public interface GetVObjectServiceAsync {

    public void getGcluster(String objectId, AsyncCallback<GClusterInfo> asyncCallback);

    public void getClustersList(AsyncCallback<ArrayList<VObject>> asyncCallback);

    public void getPulsar(String objectId, AsyncCallback<PulsarInfo> asyncCallback);

    public void getStar(String objectId, AsyncCallback<StarInfo> asyncCallback);

    public void getCatalogInfo(String catalogId, AsyncCallback<Catalogue> asyncCallback);

    public void getParameterHistory(String attName, String vobjectId, AsyncCallback<ArrayList<AdditionalInfoAttribute>> asyncCallback);

    public void getGclusterByParameters(String parameter, String Value, String operator, AsyncCallback<ArrayList<VObject>> asyncCallback);

    public void getVobjectPartial(String value, AsyncCallback<ArrayList<VObject>> asyncCallback);

    public void getBiblioGcluster(String objectId, AsyncCallback<GClusterInfo> asyncCallback);
}
