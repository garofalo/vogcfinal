package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.ArrayList;
import org.vogc.client.datatype.AdditionalInfoAttribute;
import org.vogc.client.datatype.Catalogue;
import org.vogc.client.datatype.GClusterInfo;
import org.vogc.client.datatype.PulsarInfo;
import org.vogc.client.datatype.StarInfo;
import org.vogc.client.datatype.VObject;

/**
 *
 * @author Sabrina
 */
@RemoteServiceRelativePath("getvobjectservice")
public interface GetVObjectService extends RemoteService {

    public GClusterInfo getGcluster(String objectId);

    public PulsarInfo getPulsar(String objectId);

    public StarInfo getStar(String objectId);

    public Catalogue getCatalogInfo(String catalogId);

    public ArrayList<VObject> getClustersList();

    public ArrayList<AdditionalInfoAttribute> getParameterHistory(String attName, String vobjectId);

    public ArrayList<VObject> getGclusterByParameters(String parameter, String Value, String operator);

    public ArrayList<VObject> getVobjectPartial(String value);

    public GClusterInfo getBiblioGcluster(String objectId);
}
