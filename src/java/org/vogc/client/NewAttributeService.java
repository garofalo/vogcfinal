package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.VObjectAttribute;

/**
 *
 * @author Sabrina
 */
@RemoteServiceRelativePath("newattributeservice")
public interface NewAttributeService extends RemoteService {

    public ErrorReport newAttribute(VObjectAttribute att, String userId, String objectId);

    public ErrorReport newAttributeValue(String userId, String objectId, int attId, String attValue);
}
