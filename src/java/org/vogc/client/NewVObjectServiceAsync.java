package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.VObject;
import org.vogc.client.datatype.VObjectAttribute;

/**
 *
 * @author Sabrina
 */
public interface NewVObjectServiceAsync {

    public void saveNewVObject(VObject obj, VObject obj2, AsyncCallback<ErrorReport> asyncCallback);

    public void saveNewPulsar(VObject obj, VObject obj2, AsyncCallback<ErrorReport> asyncCallback);

    public void getAttributesList(int objType, AsyncCallback<ArrayList<VObjectAttribute>> asyncCallback);
}
