package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import org.vogc.client.datatype.ErrorReport;

/**
 *
 * @author Sabrina
 */
public interface UserAccessServiceAsync {

    public void authenticateUser(String userId, String password, AsyncCallback<ErrorReport> asyncCallback);
}
