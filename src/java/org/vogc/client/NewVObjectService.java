package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.ArrayList;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.VObject;
import org.vogc.client.datatype.VObjectAttribute;

/**
 *
 * @author Sabrina
 */
@RemoteServiceRelativePath("newvobjectservice")
public interface NewVObjectService extends RemoteService {

    public ArrayList<VObjectAttribute> getAttributesList(int objType);

    public ErrorReport saveNewVObject(VObject obj, VObject obj2);

    public ErrorReport saveNewPulsar(VObject obj, VObject obj2);
}
