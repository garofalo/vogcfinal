package org.vogc.client;

import com.google.gwt.core.client.EntryPoint;
import org.vogc.client.gui.NewLoginPanel;

/**
 * Main entry point.
 *
 * @author EM
 */
public class MainEntryPoint implements EntryPoint {

    /**
     * Creates a new instance of MainEntryPoint
     */
    public MainEntryPoint() {
    }

    /**
     * The entry point method, called automatically by loading a module that
     * declares an implementing class as an entry-point
     */
    @Override
    public void onModuleLoad() {
        new NewLoginPanel().draw();

    }
}
