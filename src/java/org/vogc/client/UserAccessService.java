package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import org.vogc.client.datatype.ErrorReport;

/**
 *
 * @author Sabrina
 */
@RemoteServiceRelativePath("UserAccessService")
public interface UserAccessService extends RemoteService {

    public ErrorReport authenticateUser(String userId, String password);
}
