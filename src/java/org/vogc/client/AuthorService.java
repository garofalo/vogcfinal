package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.ArrayList;
import org.vogc.client.datatype.Author;

/**
 *
 * @author Ettore
 */
@RemoteServiceRelativePath("authorservice")
public interface AuthorService extends RemoteService {

     ArrayList<Author> ShowlistAuthors();
}
