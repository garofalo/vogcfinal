package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import org.vogc.client.datatype.Author;

/**
 *
 * @author Ettore
 */
public interface AuthorServiceAsync {
    void ShowlistAuthors(AsyncCallback<ArrayList<Author>> asyncCallback);
}
