package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import org.vogc.client.datatype.SearchResult;

/**
 *
 * @author Sabrina Checola
 */
@RemoteServiceRelativePath("searchservice")
public interface SearchService extends RemoteService {

    public SearchResult search(String s);
}
