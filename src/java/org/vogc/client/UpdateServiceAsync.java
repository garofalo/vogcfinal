package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import org.vogc.client.datatype.AdditionalInfoAttribute;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.Note;

/**
 *
 * @author Sabrina
 */
public interface UpdateServiceAsync {

    public void updateAttribute(AdditionalInfoAttribute attValues, AsyncCallback<ErrorReport> asyncCallback);

    public void updateAttributeValue(String userId, int objectType, int objectHasAttId, String value, String name, String attribute, AsyncCallback<ErrorReport> asyncCallback);

    public void updateBiblioRef(BiblioReference bibRef, AsyncCallback<ErrorReport> asyncCallback);

    public void updateNote(Note note, AsyncCallback<ErrorReport> asyncCallback);
}
