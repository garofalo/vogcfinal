package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.VObjectAttribute;

/**
 *
 * @author Sabrina
 */
public interface NewAttributeServiceAsync {

    public void newAttribute(VObjectAttribute att, String userId, String objectId, AsyncCallback<ErrorReport> asyncCallback);

    public void newAttributeValue(String userId, String objectId, int attId, String attValue, AsyncCallback<ErrorReport> asyncCallback);
}
