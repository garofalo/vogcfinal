package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.ArrayList;
import org.vogc.client.datatype.User;

/**
 *
 * @author Sabrina
 */
@RemoteServiceRelativePath("userinfoservice")
public interface UserInfoService extends RemoteService {

    public User getUserInfo(String userId);

    public ArrayList<User> getUsersList();
}
