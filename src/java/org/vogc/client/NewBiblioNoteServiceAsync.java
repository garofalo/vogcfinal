package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import org.vogc.client.datatype.BiblioReference;
import org.vogc.client.datatype.ErrorReport;
import org.vogc.client.datatype.InfoForNotes;
import org.vogc.client.datatype.Note;

/**
 *
 * @author Sabrina
 */
public interface NewBiblioNoteServiceAsync {

    public void getBibblioNoteInfo(String s, AsyncCallback<InfoForNotes> asyncCallback);

    public void saveBiblioRef(BiblioReference br, AsyncCallback<ErrorReport> asyncCallback);

    public void saveNote(Note note, AsyncCallback<ErrorReport> asyncCallback);

    public void saveNoteNew(BiblioReference br, AsyncCallback<ErrorReport> asyncCallback);

    public void saveNewAuthor(String n, String u, AsyncCallback<ErrorReport> asyncCallback);
}
