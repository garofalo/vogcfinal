package org.vogc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import org.vogc.client.datatype.SearchResult;

/**
 *
 * @author Sabrina
 */
public interface SearchServiceAsync {

    public void search(String s, AsyncCallback<SearchResult> callback);
}
