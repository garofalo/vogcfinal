package org.vogc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import org.vogc.client.datatype.ErrorReport;

/**
 *
 * @author Sabrina
 */
@RemoteServiceRelativePath("deleteservice")
public interface DeleteService extends RemoteService {

    public ErrorReport deleteAttribute(String userId, String objectId, String attName);

    public ErrorReport deleteAttributeValue(String userId, String objectId, int vobjHasAttId);

    public ErrorReport deleteBiblioNote(String userId, int noteId);

    public ErrorReport deleteObject(String userId, String type, String objectId);
}
